using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetDropShadowProperties : MonoBehaviour
{
    public Shader shaderReference;
    public Texture dropShadowDepthTexture;
    public Transform cameraPosition;
    public Vector2 dropShadowTiling = new Vector2(1f, 1f);
    public Vector2 dropShadowOffset = new Vector2(0f, 0f);
    public Color shadowColor;
    private Material[] newToonShaderMaterials;

    void Awake()
    {
        // Seek all renderers and set the materials
        Material[] allMaterials = (Material[])Resources.FindObjectsOfTypeAll(typeof(Material));
        List<Material> wantedShaderMaterials = new List<Material>();
        Debug.Log("Total number of materials: " + allMaterials.Length);

        foreach (Material material in allMaterials)
        {
            if (material.shader.name == shaderReference.name)
            {
                material.SetTexture("_DropShadowDepthTexture", dropShadowDepthTexture);
                // material.EnableKeyword("BOOLEAN_76C42AF7CBDF452AAF8DE3EAF8F8664C_ON");     // HACK: To force enable later (NOTE: I think that in the reference text it needs to end with "_ON" in order to be an exposed keyword)
                if (material.IsKeywordEnabled("BOOLEAN_76C42AF7CBDF452AAF8DE3EAF8F8664C_ON"))
                {
                    wantedShaderMaterials.Add(material);
                }
            }
        }
        Debug.Log("Number of materials to set uniforms for: " + wantedShaderMaterials.Count);

        newToonShaderMaterials = wantedShaderMaterials.ToArray();
    }

    void Update()
    {
        // Add in the drop shadow props
        foreach (Material toonShaderMaterial in newToonShaderMaterials)
        {
            toonShaderMaterial.SetVector("_CameraPosition", cameraPosition.position);
            toonShaderMaterial.SetVector("_DropShadowTiling", dropShadowTiling);
            toonShaderMaterial.SetVector("_DropShadowOffset", dropShadowOffset);
            toonShaderMaterial.SetVector("_DropShadowColor", shadowColor);
        }
    }
}
