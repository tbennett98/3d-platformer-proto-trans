using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanAnimatorManager : MonoBehaviour
{
    public BaseMovement baseMovement;
    public Animator animator;

    private bool inputtedJump;

    void FixedUpdate()
    {
        //
        // Read off the states from baseMovement
        //
        if (inputtedJump && baseMovement.GetVelocity().y <= 0f)
        {
            inputtedJump = false;
        }
        if (!inputtedJump)
        {
            animator.SetBool("IsGrounded", baseMovement.isGrounded);
        }

        //
        // Check if moving
        //
        Vector3 movementXZ = baseMovement.GetVelocity();
        movementXZ.y = 0f;
        animator.SetBool("IsMoving", movementXZ.magnitude > 0.05f);

        
    }

    public void TriggerJumped(bool grounded)
    {
        inputtedJump = true;
        animator.SetBool("IsGrounded", false);

        if (!grounded)
            animator.SetTrigger("TriggerMidairJump");
    }

    public void SetLedgeGrabbing(bool isLedgeGrabbing)
    {
        animator.SetBool("IsLedgeGrab", isLedgeGrabbing);
    }

    public void SetWallClimbing(bool isWallClimbing)
    {
        animator.SetBool("IsWallClimbing", isWallClimbing);
    }

    public void SetWallClimbBlendValue(float blendValue)
    {
        animator.SetFloat("Wall-Climb Blend", blendValue);
    }
}
