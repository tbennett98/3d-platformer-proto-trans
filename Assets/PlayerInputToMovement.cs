using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputToMovement : MonoBehaviour
{
    public BaseMovement baseMovement;
    
    [Header("Movement Attributes")]
    public float acceleration = 10f;
    public float midairAcceleration = 5f;
    public float maxSpeed = 10f;
    public bool disableMovement = false;

    [Header("Model Facing Direction Attributes")]
    public Transform model;
    public float facingTargetAngle;
    public float turnSmoothTime = 0.1f;
    private float turnSmoothVelo;

    private Transform mainCamera;
    private Rigidbody body;

    void Awake()
    {
        mainCamera = Camera.main.transform;
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        ProcessFacingTargetAngle(false);
    }

    void FixedUpdate()
    {
        if (disableMovement) return;

        //
        // Input X and Z axis movement
        //
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 targetDirection =
            Quaternion.Euler(0f, mainCamera.eulerAngles.y, 0f)
            * Vector3.ClampMagnitude(new Vector3(horizontal, 0f, vertical), 1f);

        if (targetDirection.magnitude < 0.1f)
        {
            targetDirection = Vector3.zero;
        }
        else
        {
            facingTargetAngle = Mathf.Atan2(targetDirection.x, targetDirection.z) * Mathf.Rad2Deg;
        }

        targetDirection = Vector3.ClampMagnitude(targetDirection, 1f);        // NOTE: keyboard diagonal movement suppression

        Vector3 flatVelocity = baseMovement.GetVelocity();    flatVelocity.y = 0;
        flatVelocity = Vector3.MoveTowards(
            flatVelocity,
            targetDirection * maxSpeed,
            (baseMovement.isGrounded ? acceleration : midairAcceleration) * 10f * Time.deltaTime
        );

        baseMovement.SetVelocity(flatVelocity, true);
    }

    public void ProcessFacingTargetAngle(bool immediately = false)
    {
        if (immediately)
        {
            model.eulerAngles = new Vector3(model.eulerAngles.x, facingTargetAngle, model.eulerAngles.z);
            return;
        }

        //
        // Process target facing angle
        //
        float angle = Mathf.SmoothDampAngle(
            model.eulerAngles.y,
            facingTargetAngle,
            ref turnSmoothVelo,
            turnSmoothTime * 50f * Time.deltaTime
        );
        model.eulerAngles = new Vector3(model.eulerAngles.x, angle, model.eulerAngles.z);
    }
}
