- [x] There is one frame where the sword is in human's hip. When pressing attack btn, you'll hit yourself
- [x] Makes the monster's dive more natural.
- [ ] When slime climbs up a little then rolls down a wall, the climbing normal goes back to (0,1,0) on grounding contact, so then until slimbeball releases and touches the wall again, it can't climb
- [ ] Monster drill spin will follow downwards slope down if close enough
- [ ] ThirdPersonMovementRigidbody.cs not processing moving platform information after slime ball
    - Actually, this seems to be caused by fast falling movement. When player lands on moving platform after a lava superjump, the player does not reset velocity
- [x] By switching while in the middle of a lava superjump, you will trap the camera in the looking down position.

- [ ] Play around and try and get baked lighting/shadows in the toon shader