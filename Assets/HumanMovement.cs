using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanMovement : MonoBehaviour
{
    public BaseMovement baseMovement;
    public PlayerInputToMovement inputToMovement;
    public Transform model;

    [Header("Movement Props")]
    public float jumpHeight = 10f;
    public int numMidairJumps = 1;
    private int numMidairJumpsUsed;
    public float jumpCoyoteTime = 0.25f;
    public float jumpCoyoteTimeTimer;
    
    private bool inputJump = false;

    [Header("Wall Jump Movement Props")]
    public Vector2 wallJumpSpeedXY = new Vector2(15f, 13f);
    public float climbUpSpeed = 5f;
    private float climbUpCurrentSpeed;
    public float climbUpTime = 1.5f;
    private float climbUpCurrentTime;
    public float wallClimbGravityMin = 5f;
    public float wallClimbGravityMax = 20f;
    public float wallClimbSpace = 0.35f;    // NOTE: this value should be the same as the X on ledgeGrabAnchorOffset
    private Vector3 flatWallClimbNormal;

    [Header("Ledge grab props")]
    public float eyeLevelMin = 0.25f;       // NOTE: this value is also shared with the wall jump detection height 
    public float eyeLevelMax = 0.5f;
    public float searchOutDistance = 1.5f;
    public Vector2 ledgeGrabAnchorOffset;
    private float ledgeGrabFacingAngle;
    private Vector3 ledgeGrabAnchor;

    private HumanAnimatorManager animatorManager;
    private Rigidbody body;
    private Transform mainCamera;

    [Header("Movement States")]
    [SerializeField] private bool isWallClimbState = false;
    [SerializeField] private bool isLedgeGrabbing = false;

    #region Unity Events
    void Awake()
    {
        animatorManager = GetComponentInChildren<HumanAnimatorManager>();
        body = GetComponent<Rigidbody>();
        mainCamera = Camera.main.transform;
    }

    void Update()
    {
        PollJumpInput();
    }

    void FixedUpdate()
    {
        if (!baseMovement.isGrounded)
        {
            CheckForLedgeGrab();
        }

        if (isLedgeGrabbing)
        {
            ProcessLedgeGrab();
        }
        else if (isWallClimbState)
        {
            ProcessWallJump();
            ProcessWallClimbState();
        }
        else
        {
            ProcessJump();
            CheckForWallClimb();
        }
    }

    void OnDrawGizmos()
    {
        // if (Application.isPlaying) return;

        // Draw ledgegrab gizmos
        Gizmos.color = Color.blue;
        Vector3 lowerOrigin = transform.position + new Vector3(0f, baseMovement.capsuleCenterYOffset + eyeLevelMin);
        Gizmos.DrawLine(lowerOrigin, lowerOrigin + model.forward * searchOutDistance);
        Vector3 upperOrigin = transform.position + new Vector3(0f, baseMovement.capsuleCenterYOffset + eyeLevelMax);
        Gizmos.DrawLine(upperOrigin, upperOrigin + model.forward * searchOutDistance);
    }
    #endregion

    private void PollJumpInput()
    {
        //
        // Input Jump movement
        //
        if (Input.GetButtonDown("Jump"))
        {
            inputJump = true;
        }
    }

    private void ProcessJump()
    {
        if (baseMovement.isGrounded && baseMovement.groundSticking)
        {
            numMidairJumpsUsed = 0;
            jumpCoyoteTimeTimer = 0f;
        }
        else
        {
            jumpCoyoteTimeTimer += Time.deltaTime;
        }

        //
        // Process jump input
        //
        if (!inputJump) return;
        inputJump = false;

        bool consideredGrounded = baseMovement.isGrounded || jumpCoyoteTimeTimer <= jumpCoyoteTime;
        if (!consideredGrounded && numMidairJumpsUsed >= numMidairJumps)    return;
        if (!consideredGrounded)                                            numMidairJumpsUsed++;

        jumpCoyoteTimeTimer = jumpCoyoteTime + 1f;            // To prevent double time with coyote time

        Vector3 velocity = baseMovement.GetVelocity();
        velocity.y = jumpHeight + Mathf.Max(body.velocity.y, 0f);
        if (!consideredGrounded)
        {
            Vector3 targetAngle = GetCameraAdjustedTargetDirection();
            if (targetAngle.magnitude > 0.1f)
            {
                // NOTE: when you do a double jump, it's really good
                // to redirect the momentum in the direction you're pushing the joystick
                velocity = targetAngle.normalized * velocity.magnitude;
                velocity.y = jumpHeight;    // Don't inherit vertical velocity
            }
        }

        baseMovement.SetVelocity(velocity, false);
        StartCoroutine(TemporarilyDisableGroundSticking(0.2f));
        animatorManager.TriggerJumped(consideredGrounded);
    }

    private IEnumerator TemporarilyDisableGroundSticking(float seconds)
    {
        baseMovement.groundSticking = false;
        yield return new WaitForSeconds(seconds);
        baseMovement.groundSticking = true;
    }

    private void CheckForWallClimb()
    {
        if (baseMovement.isGrounded) return;
        if (baseMovement.GetVelocity().y >= 0f) return;

        Vector3 flatVelocity = baseMovement.GetVelocity();
        flatVelocity.y = 0f;
        if (flatVelocity.magnitude < 0.25f) return;

        //
        // Find wall based off facing direction
        //
        Vector3 direction =
            Quaternion.Euler(0f, inputToMovement.facingTargetAngle, 0f)
            * Vector3.forward;

        RaycastHit hitInfo;
        bool hit =
            Physics.Raycast(body.position + new Vector3(0f, baseMovement.capsuleCenterYOffset + eyeLevelMin, 0f), direction, out hitInfo, baseMovement.capsuleRadius + 1.5f, baseMovement.raycastLayers)
            && hitInfo.collider.tag.Contains("ground");
        if (!hit) return;

        //
        // Check if actually a wall
        //
        const float requiredSineValue80degrees = 0.98480775301f * 0.98480775301f;
        if (hitInfo.normal.x * hitInfo.normal.x + hitInfo.normal.z * hitInfo.normal.z < requiredSineValue80degrees) return;

        //
        // Get into the wall climb mode
        //
        flatWallClimbNormal = hitInfo.normal;
        flatWallClimbNormal.y = 0f;
        flatWallClimbNormal.Normalize();
        inputToMovement.facingTargetAngle = Mathf.Atan2(-flatWallClimbNormal.x, -flatWallClimbNormal.z) * Mathf.Rad2Deg;
        SetupWallClimbState(true);
    }

    private void ProcessWallJump()
    {
        // Process Jump
        if (!inputJump) return;
        inputJump = false;

        //
        // Build wall jump vector
        //
        Vector3 wallJumpVector = wallJumpSpeedXY.x * flatWallClimbNormal;
        wallJumpVector.y = wallJumpSpeedXY.y;
        baseMovement.SetVelocity(wallJumpVector);
        inputToMovement.facingTargetAngle = Mathf.Atan2(flatWallClimbNormal.x, flatWallClimbNormal.z) * Mathf.Rad2Deg;      // Flipped direction
        inputToMovement.ProcessFacingTargetAngle(true);
        SetupWallClimbState(false);
    }

    private void CheckForLedgeGrab()        // NOTE: this may turn into its own monobehavior if needed to be modularized.
    {
        if (baseMovement.GetVelocity().y > 0f && !isWallClimbState) return;

        //
        // Find wall based off facing direction at eye level
        //
        Vector3 direction =
            Quaternion.Euler(0f, inputToMovement.facingTargetAngle, 0f)
            * Vector3.forward;

        RaycastHit hitInfo;
        bool hit =
            Physics.Raycast(
                body.position + new Vector3(0f, baseMovement.capsuleCenterYOffset + eyeLevelMin, 0f),
                direction,
                out hitInfo,
                baseMovement.capsuleRadius + searchOutDistance,
                baseMovement.raycastLayers)
            && hitInfo.collider.tag.Contains("ground");
        Debug.DrawLine(
            body.position + new Vector3(0f, baseMovement.capsuleCenterYOffset + eyeLevelMin, 0f),
            body.position + new Vector3(0f, baseMovement.capsuleCenterYOffset + eyeLevelMin, 0f) + direction * (baseMovement.capsuleRadius + searchOutDistance),
            Color.magenta
        );
        if (!hit) return;

        //
        // Check if actually a wall
        //
        const float requiredSineValue80degrees = 0.98480775301f * 0.98480775301f;
        if (hitInfo.normal.x * hitInfo.normal.x + hitInfo.normal.z * hitInfo.normal.z < requiredSineValue80degrees) return;

        //
        // Check if this is a ledge
        // (probably... modular pieces of a level can be false positive ledges,
        // which is why we check the other things like ceilings and if the ledge is actually a wall later)
        //
        float heightDifference = Mathf.Abs(eyeLevelMax - eyeLevelMin);
        Vector3 wallFlatNormal = hitInfo.normal;    wallFlatNormal.y = 0f;  wallFlatNormal.Normalize();
        Vector3 checkLedgeOrigin =
            new Vector3(
                hitInfo.point.x,
                body.position.y + baseMovement.capsuleCenterYOffset + eyeLevelMax,
                hitInfo.point.z)
                - wallFlatNormal * 0.01f;
        RaycastHit ledgeHitInfo;
        bool confirmAsLedge =
            Physics.Raycast(checkLedgeOrigin, Vector3.down, out ledgeHitInfo, heightDifference + 0.1f, baseMovement.raycastLayers)
            && ledgeHitInfo.collider.tag.Contains("ground");
        if (!confirmAsLedge) return;

        //
        // Check if no ceiling
        //
        heightDifference = baseMovement.capsuleCenterYOffset + baseMovement.capsuleHeight / 2f - eyeLevelMax;
        if (heightDifference > 0f)
        {
            Ray checkIfCeilingExistsAbove = new Ray(
                body.position + new Vector3(0f, baseMovement.capsuleCenterYOffset + baseMovement.capsuleHeight / 2f, 0f),
                Vector3.up
            );
            RaycastHit ceilingHitInfo;
            bool hitCeiling =
                Physics.Raycast(checkIfCeilingExistsAbove, out ceilingHitInfo, heightDifference + 0.01f, baseMovement.raycastLayers)
                && ceilingHitInfo.collider.tag.Contains("ground");

            if (hitCeiling) return;
        }

        //
        // Check if ledge actually isn't a wall (min. required height = 0.05f)
        //
        const float minRequiredHeight = 0.05f;
        const float wallCheckDistance = 0.1f;
        RaycastHit wallCheckHitInfo;
        Vector3 ledgeTrulyWallCheckOrigin =
            new Vector3(
                hitInfo.point.x + hitInfo.normal.x * wallCheckDistance,
                ledgeHitInfo.point.y + minRequiredHeight,
                hitInfo.point.z + hitInfo.normal.z * wallCheckDistance
            );
        Vector3 wallCheckNormal = -hitInfo.normal; wallCheckNormal.y = 0f; wallCheckNormal.Normalize();
        bool wallCheckHit =
            Physics.Raycast(
                ledgeTrulyWallCheckOrigin,
                wallCheckNormal,
                out wallCheckHitInfo,
                wallCheckDistance * 2f,
                baseMovement.raycastLayers)
            && wallCheckHitInfo.collider.tag.Contains("ground");
        Debug.DrawLine(ledgeTrulyWallCheckOrigin, ledgeTrulyWallCheckOrigin + wallCheckNormal * (wallCheckDistance * 2f), Color.blue);
        if (wallCheckHit) return;
        // Debug.Break();

        //
        // Now it's time to do the actual ledge grab stuff
        //
        ledgeGrabFacingAngle = Mathf.Atan2(-wallFlatNormal.x, -wallFlatNormal.z) * Mathf.Rad2Deg;
        inputToMovement.facingTargetAngle = ledgeGrabFacingAngle;
        ledgeGrabAnchor = new Vector3(hitInfo.point.x, ledgeHitInfo.point.y, hitInfo.point.z);
        SetupLedgeGrabState(true);
    }

    private void ProcessLedgeGrab()
    {
        //
        // If hit the ground, then end the ledge grab
        //
        if (baseMovement.isGrounded)
        {
            SetupLedgeGrabState(false);
            return;
        }

        //
        // Ledge grab moving platform stuff
        //
        Vector3 movingPlatformVelocity = Vector3.zero;
        Vector3 findPlatformOrigin =
            body.position
            - new Vector3(0f, ledgeGrabAnchorOffset.y, 0f)
            + new Vector3(0f, -0.05f, 0f);
        RaycastHit findPlatformHitInfo;
        bool hitPlatform =
            Physics.Raycast(
                findPlatformOrigin,
                Quaternion.Euler(0f, ledgeGrabFacingAngle, 0f) * Vector3.forward,
                out findPlatformHitInfo,
                baseMovement.capsuleRadius + 1.5f,
                baseMovement.raycastLayers)
            && findPlatformHitInfo.collider.tag.Contains("ground");
        Debug.DrawLine(findPlatformOrigin, findPlatformOrigin + Quaternion.Euler(0f, ledgeGrabFacingAngle, 0f) * Vector3.forward * (baseMovement.capsuleRadius + 1.5f), Color.green);
        if (hitPlatform && findPlatformHitInfo.rigidbody != null)
        {
            movingPlatformVelocity = findPlatformHitInfo.rigidbody.GetPointVelocity(findPlatformHitInfo.point);
            body.velocity += movingPlatformVelocity;
        }

        //
        // Process Jumping out of ledge grab
        //
        if (!inputJump) return;
        inputJump = false;

        SetupLedgeGrabState(false);

        Vector3 velocity = body.velocity;
        velocity.y += jumpHeight;
        baseMovement.SetVelocity(velocity);
    }

    private void ProcessWallClimbState()
    {
        if (!isWallClimbState) return;

        //
        // Collected preliminary info to see if still wallclimbing
        //
        if (baseMovement.isGrounded)
        {
            SetupWallClimbState(false);
            return;
        }

        RaycastHit hitInfo;
        bool hit =
            Physics.Raycast(body.position + new Vector3(0f, baseMovement.capsuleCenterYOffset + eyeLevelMin, 0f), -flatWallClimbNormal, out hitInfo, baseMovement.capsuleRadius + 1.5f, baseMovement.raycastLayers)
            && hitInfo.collider.tag.Contains("ground");
        if (!hit)
        {
            SetupWallClimbState(false);
            return;
        }

        flatWallClimbNormal = hitInfo.normal;
        flatWallClimbNormal.y = 0f;
        flatWallClimbNormal.Normalize();

        //
        // Space character away from wall
        //
        body.MovePosition(
            hitInfo.point
            - new Vector3(0f, baseMovement.capsuleCenterYOffset + eyeLevelMin, 0f)
            + flatWallClimbNormal * wallClimbSpace
        );

        //
        // Set wall climbing speed
        //
        if (climbUpCurrentTime < climbUpTime)
        {
            climbUpCurrentTime += Time.deltaTime;

            // Climb up
            climbUpCurrentSpeed = climbUpSpeed;     // NOTE: this is initialized when entering the wallclimb state as 0f. This is in case if the climbuptime is set to 0 so then the climbup starting speed isn't set to the prop.
        }
        else
        {
            // Check if sliding faster
            float fallGravity = wallClimbGravityMin;
            Vector3 targetDirection = GetCameraAdjustedTargetDirection();
            float inputAngle = Vector3.Angle(targetDirection, flatWallClimbNormal);
            if (targetDirection.magnitude > 0f && inputAngle < 45f)
            {
                fallGravity = wallClimbGravityMax;
            }

            // Fall down
            climbUpCurrentSpeed += Physics.gravity.y * Time.deltaTime;
            climbUpCurrentSpeed = Mathf.Max(climbUpCurrentSpeed, -fallGravity);
        }

        animatorManager.SetWallClimbBlendValue(1.0f - Mathf.Clamp01(climbUpCurrentSpeed / climbUpSpeed));

        //
        // Slide up or down the wall
        //
        Vector3 slidingVelocity = Vector3.zero;
        if (hitInfo.rigidbody != null)
        {
            slidingVelocity += hitInfo.rigidbody.GetPointVelocity(hitInfo.point);       // In case if the wall is a moving/rotating platform
        }

        slidingVelocity +=
            Quaternion.FromToRotation(Vector3.up, hitInfo.normal)
            * (-flatWallClimbNormal * climbUpCurrentSpeed);

        inputToMovement.facingTargetAngle = Mathf.Atan2(-flatWallClimbNormal.x, -flatWallClimbNormal.z) * Mathf.Rad2Deg;
        baseMovement.SetVelocity(slidingVelocity);
    }

    private void SetupWallClimbState(bool isWallClimbState)
    {
        this.isWallClimbState = isWallClimbState;

        climbUpCurrentSpeed = 0f;
        climbUpCurrentTime = 0f;
        baseMovement.applyGravity = !isWallClimbState;      // If wallclimb, don't apply gravity
        inputToMovement.disableMovement = isWallClimbState; // If wallclimb, then don't process input and movement from this script

        animatorManager.SetWallClimbing(isWallClimbState);
    }

    private void SetupLedgeGrabState(bool isLedgeGrabbing)
    {
        SetupWallClimbState(false);

        this.isLedgeGrabbing = isLedgeGrabbing;

        baseMovement.applyGravity = !isLedgeGrabbing;
        inputToMovement.disableMovement = isLedgeGrabbing;

        if (isLedgeGrabbing)
        {
            numMidairJumpsUsed = 0;             // NOTE: numMidairJumpsUsed gets reset when doing a ledge grab!
            baseMovement.SetVelocity(Vector3.zero);

            body.MovePosition(
                ledgeGrabAnchor
                    + Quaternion.Euler(0f, ledgeGrabFacingAngle, 0f)
                        * new Vector3(0f, ledgeGrabAnchorOffset.y, ledgeGrabAnchorOffset.x)
            );
        }

        animatorManager.SetLedgeGrabbing(isLedgeGrabbing);
    }

    private Vector3 GetCameraAdjustedTargetDirection()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        return Quaternion.Euler(0f, mainCamera.eulerAngles.y, 0f)
            * Vector3.ClampMagnitude(new Vector3(horizontal, 0f, vertical), 1f);
    }
}
