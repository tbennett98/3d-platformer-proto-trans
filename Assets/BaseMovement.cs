using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
    [ExecuteAlways]
#endif

[RequireComponent(typeof(CapsuleCollider), typeof(Rigidbody))]
public class BaseMovement : MonoBehaviour
{
    [Header("Character Movement")]
    [SerializeField] private Vector3 velocity;
    private Vector3 previousVelocity;
    public bool applyGravity = true;
    public bool groundSticking = true;
    public bool isGrounded { get { return _grounded; } }
    private bool _grounded;
    private Rigidbody body;
    private Vector3 movingPlatformVelocity;
    
    [Header("Character Collider Attributes")]
    [Range(0f, 1f)] public float fakedSpaceUnderneath = 0.6f;     // NOTE: this is for climbing stairs mainly. There needs to be at least a little so that the raycast can sense the ground underneath
    public float capsuleCenterYOffset;
    public float capsuleHeight = 2f;
    public float capsuleRadius = 0.5f;
    private CapsuleCollider capsuleCollider;

    [Header("Ground detection")]
    public LayerMask raycastLayers;

    [Header("Stairs Corrective Movement")]
    public float stepHeight = 0.35f;        // NOTE: this could be a problem to constantly have this be the requirement, since if stepHeight and stair detection are in place, then that means that the capsule collider MUST have certain fudging attributes in order for this to work.
    

    #region Event Functions

    void Awake()
    {
        capsuleCollider = GetComponent<CapsuleCollider>();
        body = GetComponent<Rigidbody>();
    }

    void Start()
    {
        StartCoroutine(ResetVelocityOfRigibodyEveryFrame());
    }

    void Update()
    {
        #if UNITY_EDITOR
        
        if (!Application.isPlaying)
        {
            if (fakedSpaceUnderneath >= capsuleHeight)
            {
                Debug.LogError("FakedSpaceUnderneath is exceeding capsuleHeight", this);
            }

            //
            // Update the faked space for the character
            //
            capsuleCollider.center = new Vector3(0f, capsuleCenterYOffset + fakedSpaceUnderneath / 2f, 0f);
            capsuleCollider.height = capsuleHeight - fakedSpaceUnderneath;
            capsuleCollider.radius = capsuleRadius;
        }

        #endif


    }

    void FixedUpdate()
    {
        if (applyGravity)
        {
            velocity += Physics.gravity * Time.deltaTime;
        }

        ProcessGround();

        body.velocity += velocity;
    }

    void OnCollisionStay(Collision other)
    {
        if (!other.collider.tag.Contains("ground")) return;

        // If snatched on a wall, shift it a tiny bit in the direction of the normal
        float groundAngle = Vector3.Angle(Vector3.up, other.GetContact(0).normal);
        bool isWall = groundAngle >= 45f;
        if (isWall)
        {
            // velocity += new Vector3(other.impulse.x, 0f, other.impulse.z);       // TODO: NOTE: this line of code will blast off the player if moving into a wall with the lower part of their capsule collider (above the fake part) kind of just pushing it?? It increases per frame for some reason

            if (body.velocity != velocity)
            {
                Debug.Log("Snatch detected! " + other.impulse + "\t" + other.GetContact(0).normal);
                body.MovePosition(body.position + other.GetContact(0).normal * 0.01f);
            }
        }
    }

    #endregion

    public void SetVelocity(Vector3 velocity, bool flatVelocity = false)
    {
        Vector3 flatVelo = velocity;
        flatVelo.y = 0f;

        if (flatVelo.magnitude > 16f)
        {
            Debug.Log("HEYHEY You're too fast!!!");
        }

        if (flatVelocity)
        {
            this.velocity.x = velocity.x;
            this.velocity.z = velocity.z;
            return;
        }
        this.velocity = velocity;
    }

    public Vector3 GetVelocity()
    {
        return velocity;
    }

    private void ProcessGround()
    {
        // Alter the distance between ground and character
        if (groundSticking)
        {
            CheckForStairs();
        }

        FindGroundBelow();
        if (isGrounded && groundSticking)
        {
            velocity.y = 0f;
        }
    }

    private void CheckForStairs()
    {
        //
        // Shoot raycast from where real capsule ends (plus a little padding)
        // and a little more from stepHeight to try and find the next step
        //
        const float raycastHeightPadding = -0.1f;
        Ray groundDetectionRay = new Ray(
            body.position + new Vector3(0f, capsuleCenterYOffset + capsuleHeight / 2f + raycastHeightPadding, 0f),
            Vector3.down
        );

        RaycastHit hitInfo;
        bool hit =
            Physics.Raycast(groundDetectionRay, out hitInfo, capsuleHeight + raycastHeightPadding + stepHeight + 0.01f, raycastLayers)
            && hitInfo.collider.tag.Contains("ground");
        if (!hit)
        {
            return;
        }

        // Move to the found step!
        body.MovePosition(
            hitInfo.point + new Vector3(0f, capsuleCenterYOffset + capsuleHeight / 2f, 0f)
        );
    }

    private void FindGroundBelow()
    {
        //
        // Check if character is touching the ground
        //
        const float raycastHeightPadding = -0.1f;
        Vector3 topOfCollider = body.position + new Vector3(0f, capsuleCenterYOffset + capsuleHeight / 2f + raycastHeightPadding, 0f);
        RaycastHit hitInfo;
        bool hit =
            Physics.Raycast(topOfCollider, Vector3.down, out hitInfo, capsuleHeight + raycastHeightPadding + 0.01f, raycastLayers)
            && hitInfo.collider.tag.Contains("ground");
        Debug.DrawLine(
            topOfCollider,
            topOfCollider + Vector3.down * (capsuleHeight + raycastHeightPadding + 0.01f),
            Color.blue);
        if (!hit)
        {
            //
            // This "unleashes" the stored moving platform velocity.
            // So there can be velocity inheritence from moving platform velocity
            //
            movingPlatformVelocity.y = Mathf.Max(movingPlatformVelocity.y, 0f);
            velocity += movingPlatformVelocity;
            movingPlatformVelocity = Vector3.zero;

            PushAwayCharacterFromWall();

            _grounded = false;
            return;
        }

        _grounded = true;

        //
        // Find out if there is any moving platform business
        // going on where character is standing
        // TODO: When extending this, look at ThirdPersonMovement.cs:ApplyMovingPlatformVelocity() for more information (especially with ledgegrabbing)
        //
        if (hitInfo.rigidbody != null)
        {
            movingPlatformVelocity = hitInfo.rigidbody.GetPointVelocity(hitInfo.point);
            body.velocity += movingPlatformVelocity;
            // facingTargetAngle += hitInfo.rigidbody.angularVelocity.y;        // TODO: get facing direction implemented
        }
        else
        {
            // To prevent an accidental "stored" velocity inheritence from triggering 
            movingPlatformVelocity = Vector3.zero;
        }
    }

    private void PushAwayCharacterFromWall()
    {
        Vector3 sphereOrigin = body.position + new Vector3(0f, capsuleCenterYOffset - capsuleHeight / 2f + capsuleRadius, 0f);      // TODO: maybe extract into a function
        Collider[] overlappedColliders = Physics.OverlapSphere(sphereOrigin, capsuleRadius - 0.01f, raycastLayers);
        foreach (Collider collider in overlappedColliders)
        {
            if (collider == (Collider)capsuleCollider) continue;

            //
            // Process this collider and push away the character
            //
            Vector3 closestPoint = collider.ClosestPointOnBounds(body.position);
            Vector3 flatDelta = body.position - closestPoint;       flatDelta.y = 0f;
            Vector3 flatImpulse = Vector3.ClampMagnitude(flatDelta, capsuleRadius);
            body.MovePosition(body.position + flatImpulse);
        }
    }

    private IEnumerator ResetVelocityOfRigibodyEveryFrame()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            previousVelocity = body.velocity;
            body.velocity = Vector3.zero;
        }
    }
}
