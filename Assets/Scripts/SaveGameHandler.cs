﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGameHandler : MonoBehaviour
{
    public GameState gameState;
    public string passthroughMessage = "";

    void TriggerSaveGame()
    {
        SaveLoadSystem.Save(gameState);

        // If wanted to call another method after this.
        if (passthroughMessage.Length > 0)
        {
            SendMessage(passthroughMessage);
        }
    }
}
