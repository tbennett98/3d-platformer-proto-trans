﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DayNightCycle : MonoBehaviour
{
    public GameState gameState;

    [Header("Time Data Props")]
    public float timeOfDay;     // NOTE: 0 is sunrise, and 1 is sunset (nighttime)
    public bool isNighttime;
    public float timeElapsePerSecond;
    public Vector2 daytimeStartEnd;
    public float timeToLerpIntoNighttimeColors;
    private float lerpIntoNighttimeTimer;

    [Header("Rotation over the day/night")]
    public Vector3 angleAxis;
    public Vector2 dayAngleFromTo;
    public float nighttimeAngle;

    [Header("Cloud Layer props")]
    public Material cloudLayerMaterial;
    public Color[] daytimeCloudColors;
    public Color[] eveningCloudColors;
    public Color[] nighttimeCloudColors;

    void Update()
    {
        if (isNighttime)
        {
            lerpIntoNighttimeTimer += Time.deltaTime;
        }
        else
        {
            timeOfDay += Time.deltaTime * timeElapsePerSecond;
            if (timeOfDay > 1f)
            {
                ChangeToNight();
            }
        }

        ProcessCloudPlane();
        ProcessLightDirection();
    }

    public void OnGotoNextDay()
    {
        // // Reset the day
        // timeOfDay = 0f;
        // isNighttime = false;

        // Reload current scene after clearing serialized items
        gameState.ClearSerializedCollectedItems();
        // TODO: set some kind of flag to show that you're "waking up" for the next day, so you should like play a cutscene or something....
        SceneManager.LoadScene(
            SceneManager.GetActiveScene().buildIndex,
            LoadSceneMode.Single
        );
    }

    private void ProcessLightDirection()
    {
        if (!isNighttime)
        {
            // Lerp from one moment to another
            float angle = Mathf.LerpAngle(dayAngleFromTo.x, dayAngleFromTo.y, timeOfDay);       // TimeofDay is already normalized
            transform.rotation = Quaternion.AngleAxis(angle, angleAxis);
        }
        else
        {
            if (lerpIntoNighttimeTimer > timeToLerpIntoNighttimeColors)
            {
                // Finished. Just set the nighttime angle
                transform.rotation = Quaternion.AngleAxis(nighttimeAngle, angleAxis);
            }
            else
            {
                // Lerp to the nighttime angle
                float lerpTime = lerpIntoNighttimeTimer / timeToLerpIntoNighttimeColors;
                transform.rotation = Quaternion.AngleAxis(Mathf.LerpAngle(dayAngleFromTo.y, nighttimeAngle, lerpTime), angleAxis);
            }
        }
    }

    private void ProcessCloudPlane()
    {
        if (!isNighttime)
        {
            Color[] newColors = new Color[2];

            // 3 sections (sunrise, daylight, sunset)
            if (timeOfDay < daytimeStartEnd.x)
            {
                // Sunrise to daylight
                float lerpTime = timeOfDay / daytimeStartEnd.x;
                newColors[0] = Color.Lerp(eveningCloudColors[0], daytimeCloudColors[0], lerpTime);
                newColors[1] = Color.Lerp(eveningCloudColors[1], daytimeCloudColors[1], lerpTime);
            }
            else if (timeOfDay > daytimeStartEnd.y)
            {
                // Daylight to sunset
                float lerpTime = (timeOfDay - daytimeStartEnd.y) / (1f - daytimeStartEnd.y);
                newColors[0] = Color.Lerp(daytimeCloudColors[0], eveningCloudColors[0], lerpTime);
                newColors[1] = Color.Lerp(daytimeCloudColors[1], eveningCloudColors[1], lerpTime);
            }
            else
            {
                // Straight daylight
                newColors[0] = daytimeCloudColors[0];
                newColors[1] = daytimeCloudColors[1];
            }

            // Apply color
            cloudLayerMaterial.SetColor("_Color_cloud_peak", daytimeCloudColors[0]);
            cloudLayerMaterial.SetColor("_Color_cloud_trough", daytimeCloudColors[1]);
        }
        else
        {
            // Do nighttime stuff
            float lerpTime = lerpIntoNighttimeTimer / timeToLerpIntoNighttimeColors;
            cloudLayerMaterial.SetColor("_Color_cloud_peak", Color.Lerp(eveningCloudColors[0], nighttimeCloudColors[0], lerpTime));
            cloudLayerMaterial.SetColor("_Color_cloud_trough", Color.Lerp(eveningCloudColors[1], nighttimeCloudColors[1], lerpTime));
        }
    }

    private void ChangeToNight()
    {
        lerpIntoNighttimeTimer = 0f;
        isNighttime = true;
    }
}
