[System.Serializable]
public class Station
{
    // public static Station[] Stations = {
    //     // Test rooms
    //     new Station("001", "Test room 1", "original"),
    //     new Station("002", "Test room 2", "w1"),
    //     new Station("003", "Test room 3", "w2"),
    //     new Station("004", "Test room 4", "SampleScene"),
    //     new Station("005", "Test room 5", "w2_1"),
    //     new Station("006", "Test room 6", "w2_test1"),
    //     new Station("007", "Test room 7", "w2_test_shop"),

    //     // W1 stations (foreign code ???)
    //     new Station("GEG", "Geneki Ganbou", "w2_test1"),
    //     new Station("DHA", "Danchi Hakase", "w2")
    // };

    public string stationCode, fullStationName, sceneName;
    public int sceneEnterId;

    // private Station(string stnCode, string fullName, string sceneName, int sceneEnterId = 0)
    // {
    //     this.stationCode = stnCode.ToUpper();
    //     this.fullStationName = fullName;
    //     this.sceneName = sceneName;
    //     this.sceneEnterId = sceneEnterId;
    // }
}