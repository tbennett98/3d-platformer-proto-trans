﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GondolaEntrance : MonoBehaviour
{
    public StationSelector stationSelector;
    private GondolaMovingPlatform gmp;

    void Start()
    {
        gmp = GetComponent<GondolaMovingPlatform>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Contains("Player"))
        {
            Station curStn = stationSelector.GetCurrentlySetStn();
            if (curStn != null && curStn.sceneName.Trim().Length > 0)
            {
                // // TODO: Load scene async
                // // TODO: Load scene with the sceneEnterId var in curStn
                if (curStn.sceneName != SceneManager.GetActiveScene().name)
                {
                    SceneManager.LoadScene(curStn.sceneName);
                    return;
                }

                // Move the gondola to the wanted station
                // TODO: if moving to the country (slime, human, monster, lava) crossing stations, start async loading the needed scene (if changing scenes once you get off the station, then start async loading it eh)
                gmp.MoveTowardsStation(curStn);
                
                stationSelector.UnsetStn();
            }
        }
    }
}
