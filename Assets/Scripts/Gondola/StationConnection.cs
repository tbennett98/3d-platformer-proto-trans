﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StationConnection
{
    public GondolaMovingPlatform start;
    public GondolaMovingPlatform end;
    public Transform optionalStartControlPoint;
    public Transform optionalEndControlPoint;
    public bool startLeft;
    public bool endLeft;
    [SerializeField] private LineRenderer _generatedConnection;

    public void SetGeneratedConnection(LineRenderer connection)
    {
        this._generatedConnection = connection;
    }

    public LineRenderer GetGeneratedConnection()
    {
        return this._generatedConnection;
    }

    public override string ToString()
    {
        return start.gondolaID + ":::" + end.gondolaID;
    }
}
