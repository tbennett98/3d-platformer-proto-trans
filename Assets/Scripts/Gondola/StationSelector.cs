﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StationSelector : MonoBehaviour
{
    public GameState gameState;
    public StationsSO stationsSO;

    public GameObject actionIndicator;
    public GameObject selectorMenuUI;
    public InputField uiTextbox;
    public GameObject uiErrorMessage;

    public GameObject purchaseMenuUI;
    public Text uiPurchaseLabel;
    public GameObject uiPurchaseError;
    public GameObject uiPurchaseSuccess;

    private Station foundStation = null;
    private Station purchasedStation = null;

    void Start()
    {
        actionIndicator.SetActive(false);
    }

    void Update()
    {
        if (actionIndicator.activeInHierarchy
            && Input.GetButtonDown("Interact") && !gameState.IsPlayerMovementDisabled())
        {
            selectorMenuUI.SetActive(!selectorMenuUI.activeInHierarchy);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Contains("Player"))
        {
            actionIndicator.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag.Contains("Player"))
        {
            actionIndicator.SetActive(false);
        }
    }

    public void OnSubmitForm()
    {
        // Reset and then pull box text
        uiErrorMessage.SetActive(false);

        string stnName = uiTextbox.text;
        
        // See if station is in list
        foundStation = Array.Find<Station>(
                stationsSO.stations,
                element => element.stationCode.ToUpper().Equals(stnName.ToUpper())
            );

        if (foundStation == null)
        {
            StartCoroutine(ShowUIMessageCoroutine(uiErrorMessage));
            return;
        }

        // Turn off the selector menu and on with the purchase menu
        selectorMenuUI.SetActive(false);
        purchaseMenuUI.SetActive(true);
        uiPurchaseLabel.text = $"A ticket to {foundStation.fullStationName} ({foundStation.stationCode}) costs $50.\nDo you want to purchase one?";
    }

    public void OnAcceptPurchase()
    {
        // Check if enough money
        if (gameState.money >= 50)
        {
            // Purchase the ticket
            StartCoroutine(ShowUIMessageCoroutine(uiPurchaseSuccess, true));
            gameState.money -= 50;

            purchasedStation = foundStation;
        }
        else
        {
            // Display error message
            StartCoroutine(ShowUIMessageCoroutine(uiPurchaseError));
        }
    }

    public void OnCancelPurchase()
    {
        selectorMenuUI.SetActive(false);
        purchaseMenuUI.SetActive(false);
    }

    private IEnumerator ShowUIMessageCoroutine(GameObject message, bool exitUI = false)
    {
        message.SetActive(true);
        yield return new WaitForSeconds(1.25f);
        message.SetActive(false);

        if (exitUI)
        {
            selectorMenuUI.SetActive(false);
            purchaseMenuUI.SetActive(false);
        }
    }

    public Station GetCurrentlySetStn()
    {
        return purchasedStation;
    }

    public void UnsetStn()
    {
        purchasedStation = null;
    }
}
