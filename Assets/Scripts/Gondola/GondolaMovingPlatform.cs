﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GondolaMovingPlatform : MonoBehaviour
{
    [Header("Gondola Identity")]
    public int gondolaID;
    public string gondolaStationCode;
    public Transform connectingLineNodePostion;

    private StationConnection[] stationConnectionsToRide = null;
    private GondolaMovingPlatform currentStation = null;
    private StationSet sceneStationsSet;
    private int currentConnection;
    private int currentSubcon;
    private bool descending;    // Desc: x -> 0     Asc: 0 -> x
    private Vector3 currentTargetPosition;
    private float ridingOffsetY;

    [Header("Actual moving platform stuff")]
    public Rigidbody body;
    public float movementVelocity = 10f;        // TODO: Make this animation curve
    public float turnTime = 0.5f;
    private GondolaMovingPlatform[] allGondolaMovingPlatformsCache = null;
    private Vector3 originalPosition;
    private Quaternion originalRotation;

    [Header("UI Menu station name indicator")]
    public UnityEngine.UI.Text uiStationNameIndicator;

    void Awake()
    {
        allGondolaMovingPlatformsCache = null;
    }

    void Start()
    {
        sceneStationsSet = FindObjectOfType<StationSet>();
        stationConnectionsToRide = null;
        currentTargetPosition = Vector3.zero;

        // Some error logging
        if (gondolaStationCode.Trim().Length == 0)
        {
            Debug.LogError("This GondolaMovingPlatform does not have a station code.", this);
        }
        foreach (GondolaMovingPlatform other in FindObjectsOfType<GondolaMovingPlatform>(true))
        {
            if (other != this && other.gondolaID == this.gondolaID)
            {
                Debug.LogError($"This GondolaMovingPlatform's gondolaID ({this.gondolaID}) conflicts with another.", this);
            }
        }

        // Set prompt title
        uiStationNameIndicator.text = $"{gondolaStationCode}:\nEnter Station Code";
    }

    void FixedUpdate()
    {
        if (stationConnectionsToRide == null)
        {
            if (!body.isKinematic)
            {
                body.isKinematic = true;
            }

            return;
        }
        ProcessStationMoving();
    }

    public void MoveTowardsStation(Station station)
    {
        Debug.Log("New Station received! " + station.stationCode + $"({sceneStationsSet.GetGondolaStnID(station.stationCode)})");
        originalPosition = body.position;
        originalRotation = body.rotation;
        stationConnectionsToRide =
            sceneStationsSet.FindConnectionsToStation(
                gondolaID,
                sceneStationsSet.GetGondolaStnID(station.stationCode)
            ).ToArray();

        foreach (var item in stationConnectionsToRide)
        {
            Debug.Log(item);
        }
        currentStation = this;
        currentConnection = 0;
        UseNextConnection(true);
        SetActiveAllOtherGondolas(false);
        body.isKinematic = false;
    }

    private void ProcessStationMoving()
    {
        Debug.Log(currentTargetPosition);
        float moveAmount = movementVelocity * Time.deltaTime;
        if ((currentTargetPosition - body.position).sqrMagnitude < moveAmount * moveAmount)
        {
            Debug.Log("Node Finished!");
            bool nextConnTime = false;
            StationConnection conn = stationConnectionsToRide[currentConnection];

            if (descending)
            {
                currentSubcon--;
                if (currentSubcon < 0) nextConnTime = true;
            }
            else
            {
                currentSubcon++;
                if (currentSubcon >= conn.GetGeneratedConnection().positionCount) nextConnTime = true;
            }

            if (nextConnTime)
            {
                if (currentConnection == stationConnectionsToRide.Length - 1)
                {
                    // Exit out
                    EndMoveToStation();
                    return;
                }

                currentStation = descending ? conn.start : conn.end;
                currentConnection++;
                UseNextConnection();
            }
            else
            {
                UseNextSubcon();
            }
        }

        Vector3 targetDirection = currentTargetPosition - body.position;
        targetDirection.y = 0f;
        targetDirection.Normalize();
        Vector3 facingDirection = transform.forward;
        if (Vector3.Dot(targetDirection, transform.forward) < 0f)
        {
            // Prevent gondola from having to turn more than 90 degrees around
            facingDirection = -facingDirection;
        }

        // Calculate the rbody mvt
        body.velocity = (currentTargetPosition - body.position).normalized * movementVelocity;
        body.angularVelocity =
            new Vector3(
                0f,
                GetAngleNormalized(Quaternion.FromToRotation(facingDirection, targetDirection).eulerAngles.y) * turnTime,
                0f
            );
        // Debug.LogWarning($"{facingDirection}:::{targetDirection}:::{GetAngleNormalized(Quaternion.FromToRotation(facingDirection, targetDirection).eulerAngles.y)}");
    }

    private float GetAngleNormalized(float angleDegrees)
    {
        return angleDegrees > 180f ? angleDegrees - 360f : angleDegrees;
    }

    private void UseNextSubcon()
    {
        StationConnection conn = stationConnectionsToRide[currentConnection];
        currentTargetPosition = conn.GetGeneratedConnection().GetPosition(currentSubcon) - new Vector3(0f, ridingOffsetY, 0f);      // It's already in useWorldSpace mode so no worries here...
    }

    private void UseNextConnection(bool firstTime = false)
    {
        StationConnection conn = stationConnectionsToRide[currentConnection];

        if (conn.start == currentStation)
        {
            descending = false;
            currentSubcon = 1;

            if (firstTime)
            {
                Debug.Log("Recalculate Riding Offset");
                ridingOffsetY = (conn.GetGeneratedConnection().GetPosition(0) - body.position).y;
            }
        }
        else
        {
            descending = true;
            currentSubcon = conn.GetGeneratedConnection().positionCount - 2;

            if (firstTime)
            {
                Debug.Log("Recalculate Riding Offset");
                ridingOffsetY = (conn.GetGeneratedConnection().GetPosition(conn.GetGeneratedConnection().positionCount - 1) - body.position).y;
            }
        }

        currentTargetPosition = conn.GetGeneratedConnection().GetPosition(currentSubcon) - new Vector3(0f, ridingOffsetY, 0f);
    }

    private void EndMoveToStation()
    {
        stationConnectionsToRide = null;
        SetActiveAllOtherGondolas(true);
        body.position = originalPosition;
        body.rotation = originalRotation;
        body.velocity = Vector3.zero;
    }

    private void SetActiveAllOtherGondolas(bool flag)
    {
        if (allGondolaMovingPlatformsCache == null || allGondolaMovingPlatformsCache.Length == 0)
        {
            Debug.Log("Mawaru Sekai!");
            allGondolaMovingPlatformsCache = FindObjectsOfType<GondolaMovingPlatform>(true);
        }
        Debug.Log("Junjitu!");
        Debug.Log(allGondolaMovingPlatformsCache.Length);

        foreach (GondolaMovingPlatform gondola in allGondolaMovingPlatformsCache)
        {
            if (gondola != this)
            {
                gondola.gameObject.SetActive(flag);
            }
        }
    }
}
