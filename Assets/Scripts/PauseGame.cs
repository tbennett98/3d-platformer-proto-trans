﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    public GameState gameState;
    public GameObject pauseMenuGO;
    private float timescalePrev;
    private bool isPaused = false;

    void Update()
    {
        // Pause/unpause game
        if (Input.GetButtonDown("Pause Game") && !gameState.IsPlayerMovementDisabled())
        {
            isPaused = !isPaused;
            Refresh();
        }

        // Load last save
        if (Input.GetButtonDown("Jump"))
        {
            if (isPaused)
            {
                // Load the last save file!
                isPaused = false;
                Refresh();
                SaveLoadSystem.Load(gameState);
            }
        }
    }

    private void Refresh()
    {
        if (isPaused)
        {
            timescalePrev = Time.timeScale;
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = timescalePrev;
        }
        pauseMenuGO.SetActive(isPaused);
    }
}
