﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityInteractor : MonoBehaviour
{
    public GameState gameState;
    private List<InteractableEntity> entities = new List<InteractableEntity>();

    void Update()
    {
        if (Input.GetButtonDown("Interact") && !gameState.IsPlayerMovementDisabled())
        {
            if (entities.Count > 0)
            {
                entities[0].OnInteracted();
            }
        }
    }

    void PushToInteractionStack(InteractableEntity interactableEntity)
    {
        entities.Insert(0, interactableEntity);
        interactableEntity.SetIsFocused(true);
    }

    void PopFromInteractionStack(InteractableEntity interactableEntity)
    {
        interactableEntity.SetIsFocused(false);
        entities.Remove(interactableEntity);

        if (entities.Count > 0)
        {
            entities[0].SetIsFocused(true);
        }
    }
}
