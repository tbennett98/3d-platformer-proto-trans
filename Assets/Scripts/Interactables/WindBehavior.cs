﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindBehavior : MonoBehaviour
{
    public float windStrength = 10f;
    public float maxMagnitude = 50f;
    public Vector3 windDirection = new Vector3(0f, 1f, 0f);

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Send it to the Human Events, since heliSpin is the only thing that can use wind
            other.SendMessageUpwards("ReportWind", new object[] { windDirection.normalized * windStrength, maxMagnitude });
        }
    }
}
