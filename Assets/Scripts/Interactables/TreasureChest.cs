﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureChest : MonoBehaviour
{
    public GameObject[] lootDrops;

    void HealthDepleted()
    {
        foreach (GameObject lootDrop in lootDrops)
        {
            Instantiate(lootDrop, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }
}
