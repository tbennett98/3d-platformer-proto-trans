﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FearVolume : MonoBehaviour
{
    void OnTriggerStay(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        other.SendMessage("SignalInEnemyBounds", true, SendMessageOptions.RequireReceiver);     // Way hacky!!!!
    }
}
