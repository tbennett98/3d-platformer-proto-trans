﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchpadHandler : MonoBehaviour
{
    public Transform shootDirection;
    public float shootSpeed = 50f;

    [Header("For Gizmo path simulation")]
    public float xAndZMidairDrag = 2f;
    public int numberOfSimulations = 10;
    public float timeStep = 0.5f;
    private Vector3 shootDirectionCached;
    private float shootSpeedCached;
    private float xAndZMidairDragCached = -1;
    private int numberOfSimulationsCached = -1;
    private float timeStepCached = -1;
    private List<Vector3[]> cachedSimulationPoints;

    void OnDrawGizmos()
    {
        // Draw the launch direction
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, shootDirection.position);

        // Update caches if dirty
        if (cachedSimulationPoints == null ||
            shootDirectionCached != shootDirection.localPosition ||
            shootSpeedCached != shootSpeed ||
            xAndZMidairDragCached != xAndZMidairDrag ||
            numberOfSimulationsCached != numberOfSimulations ||
            timeStepCached != timeStep)
        {
            CalculateAndCacheSimulation();
        }

        // Draw the cached simulation points
        Vector3[] previousSimPoint = new Vector3[] {
            Vector3.zero, Vector3.zero
        };
        foreach (Vector3[] simPoint in cachedSimulationPoints)
        {
            // Draw far simulation
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(
                transform.position + previousSimPoint[0],
                transform.position + simPoint[0]
            );
            previousSimPoint[0] = simPoint[0];

            // Draw close simulation
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(
                transform.position + previousSimPoint[1],
                transform.position + simPoint[1]
            );
            previousSimPoint[1] = simPoint[1];
        }
    }

    private void CalculateAndCacheSimulation()
    {
        // Update dirty flags
        shootDirectionCached = shootDirection.localPosition;
        shootSpeedCached = shootSpeed;
        xAndZMidairDragCached = xAndZMidairDrag;
        numberOfSimulationsCached = numberOfSimulations;
        timeStepCached = timeStep;

        // Init/clear the simulation cache
        if (cachedSimulationPoints == null)
        {
            cachedSimulationPoints = new List<Vector3[]>();
        }
        else
        {
            cachedSimulationPoints.Clear();
        }

        // Draw the launch projectile arc
        Vector3[] currentSimulationPositions = new Vector3[] {
            Vector3.zero, Vector3.zero
        };
        Vector3 velo = (shootDirection.position - transform.position).normalized * shootSpeed;
        for (int i = 0; i < numberOfSimulations; i++)
        {
            // Update far simulation
            currentSimulationPositions[0] += velo * timeStep;

            // Update velo
            Vector3 flatVelo = velo;
            flatVelo.y = 0f;
            flatVelo = Vector3.MoveTowards(flatVelo, Vector3.zero, xAndZMidairDrag * 10f * timeStep);
            velo.x = flatVelo.x;
            velo.z = flatVelo.z;
            velo += Physics.gravity * timeStep;

            // Update near simulation
            currentSimulationPositions[1] += velo * timeStep;

            // Insert into the simulation cache
            cachedSimulationPoints.Add(new Vector3[] { currentSimulationPositions[0], currentSimulationPositions[1] });     // Need this reassembly instead of passing the reference or else the simulation points would all equal the last simulation point
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject
                .GetComponent<ThirdPersonMovementRigidbody>()
                .SetVelocity(
                    (shootDirection.position - transform.position).normalized * shootSpeed
                );
        }
    }
}
