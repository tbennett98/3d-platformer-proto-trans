﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableObject : MonoBehaviour
{
    public GameObject objectToBreak;
    public bool breakOnMonsterBash = true;

    void Start()
    {
        if (objectToBreak == null)
        {
            objectToBreak = gameObject;
        }
    }

    void OnMonsterBash()
    {
        // Break if breaks on this kind of thing
        if (breakOnMonsterBash)
        {
            Destroy(objectToBreak);
        }
    }
}
