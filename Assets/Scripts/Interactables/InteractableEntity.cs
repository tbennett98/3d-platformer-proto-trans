﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: DESIGN: See if the action indicators can display at all times or need to be shut off if not at top of stack of interactable entities.
public class InteractableEntity : MonoBehaviour
{
    public string callMessage = "OnInteracted";
    public GameObject actionIndicator;
    private bool isFocused = false;
    private bool isBusy = false;

    private Collider tempOther;

    void Start()
    {
        isFocused = false;
        actionIndicator.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            tempOther = other;          // HACK: This will end up being broken if multiple players.
            other.SendMessage("PushToInteractionStack", this);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.SendMessage("PopFromInteractionStack", this);     // Tech debt with line 55
        }
    }

    public void SetIsFocused(bool flag)
    {
        isFocused = flag;
        actionIndicator.SetActive(isFocused);
    }

    public void OnInteracted()
    {
        if (isBusy) return;

        // Stop interactibility with this entity and hog the stack
        isBusy = true;
        actionIndicator.SetActive(false);
        SendMessage(callMessage);
    }

    public void UndoBusyFlag()
    {
        isBusy = false;
        actionIndicator.SetActive(true);
    }

    public void SignalLeftOrDestroyed()
    {
        tempOther.SendMessage("PopFromInteractionStack", this);
    }
}
