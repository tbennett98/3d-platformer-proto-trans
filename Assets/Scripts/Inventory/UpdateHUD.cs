﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateHUD : MonoBehaviour
{
    public GameState gameState;
    public Text uiMoney;

    void Awake()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }

    void Update()
    {
        uiMoney.text = $"${gameState.money}";
    }
}
