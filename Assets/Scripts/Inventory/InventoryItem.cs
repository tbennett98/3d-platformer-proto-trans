[System.Serializable]
public class InventoryItem
{
    public string name;
    public int amount;

    public InventoryItem(string name, int amount)
    {
        this.name = name;
        this.amount = amount;
    }
}