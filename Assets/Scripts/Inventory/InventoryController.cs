﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    public GameState gameState;
    private Cinemachine.CinemachineFreeLook orbit;

    public bool isInventoryOn = false;
    public GameObject uiGameObj;
    public Cinemachine.CinemachineVirtualCamera uiGameObjVirtCam;
    public InventoryUIPopulator inventoryPopulator;

    void Awake()
    {
        orbit = FindObjectOfType<Cinemachine.CinemachineFreeLook>(true);
    }

    void Start()
    {
        isInventoryOn = false;
        uiGameObj.SetActive(false);
    }

    void Update()
    {
        if (Input.GetButtonDown("Inventory")
            && (!gameState.IsPlayerMovementDisabled() || isInventoryOn)
            && (Cinemachine.CinemachineCore.Instance.IsLive(orbit)
                || Cinemachine.CinemachineCore.Instance.IsLive(uiGameObjVirtCam)))
        {
            ToggleInventory();
        }
    }

    private void ToggleInventory()
    {
        isInventoryOn = !isInventoryOn;

        // Set everything depending on the flags
        uiGameObj.SetActive(isInventoryOn);
        inventoryPopulator.PopulateInventoryItems(gameState.GetInventoryItems());
    }
}
