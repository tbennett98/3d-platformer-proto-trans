﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryUIPopulator : MonoBehaviour
{
    public EventSystem eventSystem;
    private InventoryUISlot[] inventorySlots;

    void Awake()
    {
        // Get inventory slots
        inventorySlots = GetComponentsInChildren<InventoryUISlot>();
    }

    public void PopulateInventoryItems(List<InventoryItem> items)
    {
        // Fill in as many slots as there are
        // TODO: make pagination or something for this...
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            inventorySlots[i].SetEventSystem(eventSystem);
            if (i < items.Count)
            {
                inventorySlots[i].SetItem(items[i]);
            }
            else
            {
                inventorySlots[i].SetItem(null);
            }
        }
    }
}
