﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventoryUISlot : MonoBehaviour
{
    public GameState gameState;
    public InventoryItemsSO inventoryItemsDatabase;
    public Text uiText;
    private InventoryItem currentInvItem;
    private TextboxLoader textboxLoader;
    private Button uiMyButton;
    private EventSystem eventSystem;

    void Start()
    {
        textboxLoader = GetComponent<TextboxLoader>();
        uiMyButton = GetComponent<Button>();
    }

    public void SetItem(InventoryItem item)
    {
        currentInvItem = item;

        if (currentInvItem == null)
        {
            uiText.text = "Butto";
            return;
        }

        uiText.text = $"{currentInvItem.name} ({currentInvItem.amount})";
    }

    public void OnClick()
    {
        if (currentInvItem == null) return;

        this.eventSystem.SetSelectedGameObject(null);

        // Setup message in textbox and show it
        string[] descriptionTexts = inventoryItemsDatabase.GetItemByName(currentInvItem.name).itemDescriptionText;
        List<TextboxQueue.TextboxData> descriptionDataTexts = new List<TextboxQueue.TextboxData>();
        foreach (string descriptionText in descriptionTexts)
        {
            descriptionDataTexts.Add(new TextboxQueue.TextboxData(descriptionText));
        }
        textboxLoader.textboxDatas = descriptionDataTexts.ToArray();

        SendMessage("OnInteracted");
    }

    public void SetEventSystem(EventSystem eventSystem)
    {
        this.eventSystem = eventSystem;
    }

    public void ResetSelectedGameObject()
    {
        this.eventSystem.SetSelectedGameObject(gameObject);
    }
}
