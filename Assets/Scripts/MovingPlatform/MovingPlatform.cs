﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public bool freezeRotationGlobal = true;
    public Vector3 freezeRotationValue;
    public Vector3 origin;          private Vector3 absoluteOrigin;
    public AnimationCurve moveVelocityX;
    public AnimationCurve moveVelocityY;
    public AnimationCurve moveVelocityZ;
    public Vector3 moveVelocityScale = new Vector3(1, 1, 1);
    public float animationTimeOffset;
    public float animationSpeed = 1;

    private Rigidbody rb;
    private MovementReporter reporter;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        absoluteOrigin = transform.position + origin;
        reporter = GetComponent<MovementReporter>();
    }

    void FixedUpdate()
    {
        if (freezeRotationGlobal)
        {
            transform.eulerAngles = freezeRotationValue;
        }

        Vector3 nextPos = new Vector3(
            absoluteOrigin.x + moveVelocityScale.x * moveVelocityX.Evaluate(animationSpeed * Time.timeSinceLevelLoad + animationTimeOffset),
            absoluteOrigin.y + moveVelocityScale.y * moveVelocityY.Evaluate(animationSpeed * Time.timeSinceLevelLoad + animationTimeOffset),
            absoluteOrigin.z + moveVelocityScale.z * moveVelocityZ.Evaluate(animationSpeed * Time.timeSinceLevelLoad + animationTimeOffset)
        );
        nextPos *= Time.fixedDeltaTime * 50f;           // FIXME: THIS IS CAUSING the huge bug with the platforms resetting during slow motion!!!

        // transform.position = (nextPos);
        rb.MovePosition(nextPos);

        // Callback to MovementReporter
        reporter.UpdateMvtReport(nextPos);
    }
}
