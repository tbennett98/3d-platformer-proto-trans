﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementReporterObserver : MonoBehaviour
{
    public string onGroundTagContains = "ground";
    public GameObject motherObject;

    public void ReportColliders(Collider[] colliders)
    {
        foreach (Collider other in colliders)
        {
            if (!other.isTrigger &&
                other.gameObject.tag.ToLower().Contains(onGroundTagContains.ToLower()))
            {
                MovementReporter reporter;
                if (other.TryGetComponent<MovementReporter>(out reporter))
                {
                    reporter.AddObserver(motherObject);
                }
            }
        }
    }
}
