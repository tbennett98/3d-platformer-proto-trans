﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreadmillPlatform : MonoBehaviour
{
    public Vector3 treadmillVelocity;
    private MovementReporter reporter;

    void Start()
    {
        reporter = GetComponent<MovementReporter>();
    }

    void FixedUpdate()
    {
        Vector3 nextPos = transform.rotation * treadmillVelocity * Time.fixedDeltaTime;
        nextPos *= Time.fixedDeltaTime * 50f;

        // Callback to MovementReporter
        reporter.UpdateMvtReport(nextPos, Quaternion.identity, true);
    }
}
