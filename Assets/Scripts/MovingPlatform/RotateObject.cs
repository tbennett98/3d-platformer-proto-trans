﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    public Vector3 angularVelocity;

    private Rigidbody rb;
    private MovementReporter reporter;
    // private Vector3 prevForward;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        reporter = GetComponent<MovementReporter>();
        // prevForward = transform.forward;
    }

    void FixedUpdate()
    {
        // transform.localEulerAngles += angularVelocity * Time.deltaTime;
        // rb.angularVelocity = angularVelocity;
        rb.MoveRotation(
            Quaternion.Euler(transform.eulerAngles + angularVelocity * Time.fixedDeltaTime * 50f)
        );

        // Callback to MovementReporter
        // Quaternion quat = Quaternion.FromToRotation(prevForward, transform.forward);
        // prevForward = transform.forward;
        reporter.UpdateMvtReport(rb.position, rb.rotation);
    }

    public Rigidbody GetRigidbody()
    {
        return rb;
    }
}
