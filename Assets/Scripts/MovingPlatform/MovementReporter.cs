﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementReporter : MonoBehaviour
{
    public MovementReporter deferToParent;
    public Vector3 deltaPosition;
    public Quaternion deltaRotation;

    private Vector3 prevPosition;
    private Quaternion prevRotation;

    private List<GameObject> observers;

    void Start()
    {
        prevPosition = transform.position;
        prevRotation = transform.rotation;
        observers = new List<GameObject>();
    }

    public void ResetPrevPosition(Vector3 newPosition)
    {
        prevPosition = newPosition;
    }

    public void UpdateMvtReport(Vector3 nextPosition, Quaternion newRotation=new Quaternion(), bool inputDeltaPosition=false)
    {
        if (inputDeltaPosition)
        {
            deltaPosition = nextPosition;
            ResetPrevPosition(transform.position);
        }
        else
        {
            deltaPosition = nextPosition - prevPosition;
            ResetPrevPosition(nextPosition);
        }

        deltaRotation = Quaternion.Inverse(prevRotation) * newRotation;
        prevRotation = newRotation;
        observers.ForEach(delegate(GameObject obj)
        {
            obj.SendMessage("ProcessMvtReport", this, SendMessageOptions.RequireReceiver);
        });

        observers.Clear();
    }

    public void AddObserver(GameObject obj)
    {
        if (deferToParent == null)
        {
            if (!observers.Contains(obj))
                observers.Add(obj);
        }
        else
        {
            // Defer to parent
            deferToParent.AddObserver(obj);
        }
    }

    public Vector3 GetPrevPosition()
    {
        return prevPosition;
    }

    public Quaternion GetPrevRotation()
    {
        return prevRotation;
    }
}
