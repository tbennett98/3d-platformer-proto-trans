using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlledMovingPlatform : MonoBehaviour
{
    public AnimationCurve lerpCurve;        // NOTE: this animation curve must be [0-1] or else it could cause lerp value booboos. Thusly, the code will clamp this to [0-1]
    public float animationSpeed = 1f;
    private float currentLerpCurveTime = 0f;
    private Vector3 startingPositionAbsolute;

    public Vector3[] controlPoints;
    private float[] controlPointLerpWeights;
    private float totalControlPointsWeight;

    [Header("Angular velocity")]
    public Vector3 angularVelocity;
    
    [Header("Animation Curve Offset")]
    public float startingOffsetNormalized;

    private Rigidbody body;

    void Awake()
    {
        body = GetComponent<Rigidbody>();
    }

    void Start()
    {
        currentLerpCurveTime = startingOffsetNormalized;
        startingPositionAbsolute = transform.position;

        // Calculate the corresponding weights for the control points
        controlPointLerpWeights = new float[controlPoints.Length - 1];
        totalControlPointsWeight = 0f;
        for (int i = 1; i < controlPoints.Length; i++)
        {
            controlPointLerpWeights[i - 1] = Vector3.Distance(controlPoints[i - 1], controlPoints[i]);
            totalControlPointsWeight += controlPointLerpWeights[i - 1];
        }

        // After recording, you want to just move the rigidbody to the specific position it has to be off the get go or else it will just end up fighting other rigidbodies that are near it.
        body.position = GetGotoPositionInPath(startingOffsetNormalized) + startingPositionAbsolute;

    }

    void OnDrawGizmos()
    {
        Vector3 _startingPosition = Application.isPlaying ? startingPositionAbsolute : transform.position;

        Gizmos.color = Color.green;
        for (int i = 1; i < controlPoints.Length; i++)
        {
            Gizmos.DrawLine(controlPoints[i - 1] + _startingPosition, controlPoints[i] + _startingPosition);
        }
    }

    void FixedUpdate()
    {
        body.angularVelocity = angularVelocity;

        // Update lerp value
        currentLerpCurveTime += animationSpeed * Time.deltaTime;

        // Find position to move to
        float lerpValue = Mathf.Clamp01(lerpCurve.Evaluate(currentLerpCurveTime));
        Vector3 aimingPosition = GetGotoPositionInPath(lerpValue);
        
        // TODO: make this code the same with the gondolamoving platform, because you could reuse the premade bezier curves, and it'd be
        // a smoother and possibly less calculation intense of an algorithm to use this one for the gondola lines
        body.velocity = (aimingPosition + startingPositionAbsolute - body.position) / Time.deltaTime;
    }

    private Vector3 GetGotoPositionInPath(float lerpValue)
    {
        lerpValue *= totalControlPointsWeight;
        
        int regionId = controlPoints.Length - 1;
        for (int i = 1; i < controlPoints.Length; i++)
        {
            float nextlerpValue = lerpValue - controlPointLerpWeights[i - 1];
            if (nextlerpValue < 0f || i == controlPoints.Length - 1)
            {
                // Too far: that means the current i is the right region
                // NOTE: the extra OR statement of `i == controlPoints.length - 1`
                // was added because the final round of checking would happen and end up
                // giving the last boundary but `nextlerpValue` would get added,
                // making the final lerp calculation in the end very weird     -Timo 03/28/21
                regionId = i;
                break;
            }
            lerpValue = nextlerpValue;
        }

        float normalizedLerpValue = Mathf.Clamp01(lerpValue / controlPointLerpWeights[regionId - 1]);
        return Vector3.Lerp(controlPoints[regionId - 1], controlPoints[regionId], normalizedLerpValue);
    }
}
