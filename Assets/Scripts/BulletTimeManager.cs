﻿// Derived from https://github.com/Brackeys/Bullet-Time-Project/blob/master/Bullet%20Time/Assets/TimeManager.cs

using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class BulletTimeManager : MonoBehaviour
{
    [Header("Effects Curves")]
    public VolumeProfile mainVolume;
    public AnimationCurve lensDistortionCurve;
    public AnimationCurve chromAberrCurve;

    [Header("Slowdown factor")]
	public float slowdownFactor = 0.05f;
	private float slowdownLength;
	private float slowdownLengthTimer;
    private bool doSlowdown = false;
    private bool doScreenEffects = false;

    private const float DEFAULT_FIXED_DELTA_TIME = 0.02f;


	void Update()
	{
        if (!doSlowdown || slowdownLength <= 0f) return;

		Time.timeScale += (1f / slowdownLength) * Time.unscaledDeltaTime;
		Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
        Time.fixedDeltaTime = Time.timeScale * DEFAULT_FIXED_DELTA_TIME;

        if (doScreenEffects)
        {
            float amount = 1f - slowdownLengthTimer / slowdownLength;       // Makes counter go from 0 to 1
            LensDistortion lensDistortion;
            if (mainVolume.TryGet<LensDistortion>(out lensDistortion))
            {
                lensDistortion.active = true;
                lensDistortion.intensity.overrideState = true;
                lensDistortion.intensity.value = lensDistortionCurve.Evaluate(amount);
            }

            ChromaticAberration chromatic;
            if (mainVolume.TryGet<ChromaticAberration>(out chromatic))
            {
                chromatic.active = true;
                chromatic.intensity.overrideState = true;
                chromatic.intensity.value = chromAberrCurve.Evaluate(amount);
            }
        }

        // Turn off slow-mo eventually
        slowdownLengthTimer -= Time.unscaledDeltaTime;
        if (slowdownLengthTimer < 0f)
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime = DEFAULT_FIXED_DELTA_TIME;
            doSlowdown = false;

            // Turn off screen effects
            if (doScreenEffects)
            {
                doScreenEffects = false;

                LensDistortion lensDistortion;
                if (mainVolume.TryGet<LensDistortion>(out lensDistortion)) { lensDistortion.active = false; }

                ChromaticAberration chromatic;
                if (mainVolume.TryGet<ChromaticAberration>(out chromatic)) { chromatic.active = false; }
            }
        }
	}

	public void DoSlowmotion(bool doScreenEffects, float slowdownLength = 2f, float pauseLength = 0f)
	{
        StartCoroutine(DoSlowmotionCoroutine(doScreenEffects, slowdownLength, pauseLength));
	}

    private IEnumerator DoSlowmotionCoroutine(bool doScreenEffects, float slowdownLength, float pauseLength)
    {
        // Do pause if pauselength is valid
        if (pauseLength > 0f)
        {
            Time.timeScale = 0f;
            yield return new WaitForSecondsRealtime(pauseLength);
            Time.timeScale = 1f;
        }

        if (slowdownLength > 0f)
        {
            doSlowdown = true;
            this.slowdownLength = slowdownLength;
            slowdownLengthTimer = slowdownLength;

            if (doScreenEffects)
            {
                this.doScreenEffects = true;
            }

            // Do normal slowdown
            Time.timeScale = slowdownFactor;
            Time.fixedDeltaTime = Time.timeScale * DEFAULT_FIXED_DELTA_TIME;
        }
    }
}
