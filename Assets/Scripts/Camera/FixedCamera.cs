﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedCamera : MonoBehaviour
{
    public bool switchToCamera = true;
    public GameObject referenceCamera;
 
    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Contains("Player"))
        {
            // Switch to that camera!
            // Or unswitch depending on var 'switchToCamera'
            referenceCamera.SetActive(switchToCamera);
        }
    }
}
