﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSetViewWhenInactive : MonoBehaviour
{
    private Cinemachine.CinemachineFreeLook orbit;
    private Transform mainCamera;

    void Start()
    {
        orbit = FindObjectOfType<Cinemachine.CinemachineFreeLook>(true);
        mainCamera = Camera.main.transform;
    }

    void Update()
    {
        if (!Cinemachine.CinemachineCore.Instance.IsLive(orbit))
        {
            orbit.m_XAxis.Value = mainCamera.eulerAngles.y;
            orbit.m_YAxis.Value = 0.5f;
        }
    }
}
