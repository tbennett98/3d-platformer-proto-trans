﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangBehavior : MonoBehaviour
{
    public float retractWaitTime = 1f;
    public float enableCatchableWaitTime = 0.1f;
    private bool catchable = false;

    [Header("Boomerang Params")]
    public float startSpeed = 10f;
    private bool isRetracting = false;
    public float retractAcceleration = 2f;
    public float maxVelocityMagnitude = 50f;
    private float maxVelocityMagnitudeSqr;
    public float catchDistance = 2f;
    private float catchDistanceSqr = 2f;

    public Transform returnPostion;
    private Rigidbody body;

    [Header("Weapon Stats")]
    public int damageToInflict = 0;
    public int poise = 0;


    [Header("Feedback effects")]
    public BulletTimeManager bulletTimeManager;
    public ParticleSystem hitSuccParticles;
    public FearBrain fearBrain;
    public float pauseTime = 0.1f;
    public float bulletTime = 0f;

    void Start()
    {
        body = GetComponent<Rigidbody>();
        maxVelocityMagnitudeSqr = maxVelocityMagnitude * maxVelocityMagnitude;
        catchDistanceSqr = catchDistance * catchDistance;

        // Start the boomerang throw!
        body.velocity = transform.rotation * new Vector3(0f, 0f, startSpeed);
        StartCoroutine(EnableRetractAfterTime());
        StartCoroutine(EnableCatchableAfterTime());
    }

    void FixedUpdate()
    {
        Vector3 deltaPos = returnPostion.position - body.position;
        float distanceSqr = deltaPos.sqrMagnitude;

        if (catchable)
        {
            if (deltaPos.sqrMagnitude < catchDistanceSqr)
            {
                // Say it's caught!
                returnPostion.SendMessage("BoomerangCaught");
                Destroy(gameObject);
            }
            else if (isRetracting)
            {
                // Move towards the player's position
                body.AddForce(deltaPos.normalized * retractAcceleration, ForceMode.Acceleration);
                body.velocity = Vector3.ClampMagnitude(body.velocity, maxVelocityMagnitude);
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag.Contains("enemy"))
        {
            bool success = other.gameObject.GetComponent<EnemyReaction>().ReceiveAttack(damageToInflict, poise, Vector3.zero);
            if (success)
            {
                Instantiate(hitSuccParticles.gameObject, other.GetContact(0).point, hitSuccParticles.transform.rotation);
                bulletTimeManager.DoSlowmotion(false, bulletTime, pauseTime);
                fearBrain.AttackSuccess(1);
            }
        }
    }

    private IEnumerator EnableRetractAfterTime()
    {
        yield return new WaitForSeconds(retractWaitTime);
        isRetracting = true;
    }

    private IEnumerator EnableCatchableAfterTime()
    {
        yield return new WaitForSeconds(enableCatchableWaitTime);
        catchable = true;
    }
}
