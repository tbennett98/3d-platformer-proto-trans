﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///////
///////         NOTE:
///////             This CS file is deprecated. Instead of using linecast systems, there is now a
///////             trigger system in MeleeHitboxReporter.cs for finding weapon collisions
///////

[Obsolete("Not used for weapon interaction anymore. See class file WeaponPath.cs, and use MeleeHitboxReporter.cs", true)]
public class WeaponPath : MonoBehaviour
{
    public Vector3[] localPoints;
    public Color debugColor;
    public LayerMask checkLayers;
    public ParticleSystem hitParticles;

    void OnDrawGizmos()
    {
        // NOTE: ONLY DEBUG FOR THE EDITOR
        if (localPoints.Length >= 2)
        {
            for (int i = 1; i < localPoints.Length; i++)
            {
                Gizmos.color = debugColor;
                Gizmos.DrawLine(transform.position + transform.rotation * localPoints[i - 1], transform.position + transform.rotation * localPoints[i]);
            }
        }
    }

    public RaycastHit? CheckForCollision()
    {
        if (localPoints.Length >= 2)
        {
            for (int i = 1; i < localPoints.Length; i++)
            {
                RaycastHit hitInfo;
                bool hit = Physics.Linecast(
                    transform.position + transform.rotation * localPoints[i - 1],
                    transform.position + transform.rotation * localPoints[i],
                    out hitInfo,
                    checkLayers
                );

                Debug.DrawLine(transform.position + transform.rotation * localPoints[i - 1],
                    transform.position + transform.rotation * localPoints[i], hit ? Color.green : Color.blue, 1f);

                if (hit)
                {
                    return hitInfo;
                }
            }
        }
        else
        {
            Debug.LogError("WeaponPath localPoints needs to have at least 2 points to make a path", this);
            return null;
        }

        return null;
    }

    public void SetParticles(Vector3 location)
    {
        Instantiate(hitParticles.gameObject, location, hitParticles.transform.rotation);
    }
}
