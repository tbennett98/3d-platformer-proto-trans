﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowBehavior : MonoBehaviour
{
    public float zoomSpeed = 50f;
    public int damageToInflict = 1;
    public int poise = 0;

    [Header("Feedback effects")]
    public BulletTimeManager bulletTimeManager;
    public ParticleSystem hitSuccParticles;
    public FearBrain fearBrain;
    public float pauseTime = 0f;
    public float bulletTime = 2f;

    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.rotation * new Vector3(0f, 0f, zoomSpeed);
    }

    void OnCollisionEnter(Collision other)
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        if (other.transform.tag.Contains("enemy"))
        {
            bool success = other.gameObject.GetComponent<EnemyReaction>().ReceiveAttack(damageToInflict, poise, Vector3.zero);      // I can imagine the params could be (offense, poise, knockback)
            if (success)
            {
                Instantiate(hitSuccParticles.gameObject, other.GetContact(0).point, hitSuccParticles.transform.rotation);
                bulletTimeManager.DoSlowmotion(false, bulletTime, pauseTime);
                fearBrain.AttackSuccess(1);
            }

            // Plant myself at enemy (remove physics but keep rendering)
            transform.parent = other.transform;
        }

        // Become a dead arrow (TODO: maybe make collectable??)
        Destroy(GetComponent<Rigidbody>());
        Destroy(GetComponent<BoxCollider>());
        Destroy(this);
    }
}
