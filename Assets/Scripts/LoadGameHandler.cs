﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGameHandler : MonoBehaviour
{
    public GameState gameState;

    void Awake()
    {
        // Save/overwrite new file!
        bool requireOverwrite = false;
        if (Input.GetKey(KeyCode.Escape))
        {
            requireOverwrite = true;
        }
        else if (!SaveLoadSystem.Load(gameState))
        {
            requireOverwrite = true;
        }

        if (requireOverwrite)
        {
            // This forces a new save file to be created (or to reset the save file if holding esc key)
            SaveLoadSystem.Save(gameState, SceneManager.GetActiveScene().buildIndex + 1);       // We don't want the load file to be taking us to an infinite load game loop
        }

        // Load next scene (or SaveLoadSystem.Load() would've already loaded us somewhere else)
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
