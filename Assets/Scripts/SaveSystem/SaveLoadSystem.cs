﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

// See https://www.youtube.com/watch?v=XOjd_qU2Ido for more help
public static class SaveLoadSystem
{
    // Serialized class for holding the needed player data
    [System.Serializable]
    class PlayerSaveData
    {
        public int sceneIndex;
        public int money;
        public string[] recordedGondolaStnCodes;
        public InventoryItem[] inventoryItems;
        public int health;
        public int maxHealth;
        public bool[] allowedTransformations;
        public int currentTransformation;
    }

    public static void Save(GameState gameState, int forceSceneIndex = -1)
    {
        string path = Application.persistentDataPath + "/solanine_save.dat";

        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = new FileStream(path, FileMode.Create);

        PlayerSaveData psd = new PlayerSaveData();
        psd.sceneIndex = forceSceneIndex < 0 ? SceneManager.GetActiveScene().buildIndex : forceSceneIndex;
        psd.money = gameState.money;
        psd.recordedGondolaStnCodes = gameState.savedStationCodes.ToArray();
        psd.inventoryItems = gameState.inventoryItems.ToArray();
        psd.health = gameState.health;
        psd.maxHealth = gameState.maxHealth;
        psd.allowedTransformations = gameState.allowedTransformations;
        psd.currentTransformation = gameState.currentTransformation;

        // Write all this data to save.dat
        bf.Serialize(fs, psd);
        fs.Close();

        Debug.Log("Successfully saved file: " + path);
    }

    public static bool Load(GameState gameState)
    {
        string path = Application.persistentDataPath + "/solanine_save.dat";

        if (File.Exists(path))
        {
            // Load in all the data into the given gamestate object
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(path, FileMode.Open);

            PlayerSaveData psd = bf.Deserialize(fs) as PlayerSaveData;
            fs.Close();

            gameState.money = psd.money;
            gameState.savedStationCodes = new List<string>(psd.recordedGondolaStnCodes);
            gameState.inventoryItems = new List<InventoryItem>(psd.inventoryItems);
            gameState.health = psd.health;
            gameState.maxHealth = psd.maxHealth;
            gameState.allowedTransformations = psd.allowedTransformations;
            gameState.currentTransformation = psd.currentTransformation;
            Debug.Log("Successfully loaded file: " + path);

            SceneManager.LoadScene(psd.sceneIndex);

            return true;
        }
        else
        {
            Debug.LogError("No save file was found in: " + path);
            return false;
        }
    }
}
