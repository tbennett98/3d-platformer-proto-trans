﻿/**
 *  Code by:    Cobertos
 *  See:        https://cobertos.com/blog/post/how-to-climb-stairs-unity3d/
 *
 *  Adapted by: Timothy Bennett
 *
 *  DEPRECATED ON 2020-12-19
 *  NOTE: See stairs implementation in SampleScene.unity
 */
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Obsolete("This is borken, and should not be used", true)]
public class ClimbStairsRigidbody : MonoBehaviour
{
    public float maxStepHeight = 0.4f;
    public float stepSearchOvershoot = 0.01f;
    public Transform bottomOfCollider;
    public OnGroundManager groundManager;

    private List<ContactPoint> allContactPts;
    private ThirdPersonMovementRigidbody tpms;

    void Start()
    {
        allContactPts = new List<ContactPoint>();
        tpms = GetComponent<ThirdPersonMovementRigidbody>();
    }

    void FixedUpdate()
    {
        if (allContactPts.Count <= 0) return;
        // Debug.Log("Hi there! " + Time.frameCount);

        // Find step if grounded and moving
        Vector3 flatVelo = tpms.GetVelocity();    flatVelo.y = 0f;
        if (groundManager.onGround && flatVelo.sqrMagnitude > 0.0001f)
        {
            Vector3 stepUpOffset = new Vector3();
            bool stepUp = FindStep(out stepUpOffset, flatVelo);
            Debug.Log(stepUp + " :::: " + Time.frameCount);
            if (stepUp)
            {
                Rigidbody body = GetComponent<Rigidbody>();
                body.position += stepUpOffset;
                body.velocity = tpms.GetVelocity();
            }
        }

        // Clear contact points once processed
        allContactPts.Clear();
    }

    void OnCollisionEnter(Collision other)
    {
        allContactPts.AddRange(other.contacts);
    }

    void OnCollisionStay(Collision other)
    {
        allContactPts.AddRange(other.contacts);
    }

    private bool FindStep(out Vector3 stepUpOffset, Vector3 velo)
    {
        stepUpOffset = new Vector3();
        
        //No chance to step if the player is not moving
        Vector2 velocityXZ = new Vector2(velo.x, velo.z);
        if (velocityXZ.sqrMagnitude < 0.0001f)
            return false;
        
        foreach (ContactPoint cp in this.allContactPts)
        {
            bool test = ResolveStepUp(out stepUpOffset, cp);
            if (test)
                return test;
        }

        return false;
    }

    private bool ResolveStepUp(out Vector3 stepUpOffset, ContactPoint stepTestCP)
    {
        stepUpOffset = new Vector3();
        Collider stepCol = stepTestCP.otherCollider;
        
        //( 1 ) Check if the contact point normal matches that of a step (y close to 0)
        if (Mathf.Abs(stepTestCP.normal.y) >= 0.01f)
        {
            return false;
        }
        
        //( 2 ) Make sure the contact point is low enough to be a step
        if (!(stepTestCP.point.y - bottomOfCollider.position.y < maxStepHeight))
        {
            return false;
        }
        
        //( 3 ) Check to see if there's actually a place to step in front of us
        //Fires one Raycast
        RaycastHit hitInfo;
        float stepHeight = bottomOfCollider.position.y + maxStepHeight + 0.0001f;
        Vector3 stepTestInvDir = new Vector3(-stepTestCP.normal.x, 0, -stepTestCP.normal.z).normalized;
        Vector3 origin = new Vector3(stepTestCP.point.x, stepHeight, stepTestCP.point.z) + (stepTestInvDir * stepSearchOvershoot);
        Vector3 direction = Vector3.down;
        if (!stepCol.Raycast(new Ray(origin, direction), out hitInfo, maxStepHeight))
        {
            return false;
        }
        
        //We have enough info to calculate the points
        Vector3 stepUpPoint = new Vector3(stepTestCP.point.x, hitInfo.point.y + 0.0001f, stepTestCP.point.z) + (stepTestInvDir * stepSearchOvershoot);
        Vector3 stepUpPointOffset = stepUpPoint - new Vector3(stepTestCP.point.x, bottomOfCollider.position.y, stepTestCP.point.z);
        
        //We passed all the checks! Calculate and return the point!
        stepUpOffset = stepUpPointOffset;
        return true;
    }
}
