﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="GameState", menuName="States/GameState")]
public class GameState : ScriptableObject
{
    public int money = 0;
    public List<InventoryItem> inventoryItems = new List<InventoryItem>();
    public List<string> todayCollectedItems = new List<string>();
    public List<string> savedStationCodes = new List<string>();

    public int entranceId = -1;                                             // For entering a room and which entrance should use
    
    public int currentTransformation = 0;                                   // That way the transformation stays the same
    public bool[] allowedTransformations = { true, true, true, true };      // If true, can do this transformation

    public int health, maxHealth;

    private List<Object> disablePlayerMovementLocks;

    void Awake()
    {
        if (disablePlayerMovementLocks == null)
        {
            disablePlayerMovementLocks = new List<Object>();
        }
    }

    void Reset()
    {
        money = 0;
        inventoryItems = new List<InventoryItem>();
        todayCollectedItems = new List<string>();
        savedStationCodes = new List<string>();
        entranceId = -1;
        currentTransformation = 0;
        allowedTransformations = new bool[] { true, true, true, true };
        maxHealth = 5;      // TODO: Idk if this is the best health value, but hey.
        health = maxHealth;

        disablePlayerMovementLocks = new List<Object>();

        Debug.Log("Reset all variables!");
    }

    public void LogCollectedItem(string guid)
    {
        todayCollectedItems.Add(guid);
    }

    public bool IsItemCollected(string guid)
    {
        return todayCollectedItems.Contains(guid);
    }

    public void AddInventoryItem(string itemName, int amount = 1)
    {
        foreach (InventoryItem item in inventoryItems)
        {
            if (item.name.ToUpper() == itemName.ToUpper())
            {
                // Add to counter of existing
                item.amount += amount;
                return;
            }
        }
        
        // Insert as new
        inventoryItems.Add(new InventoryItem(itemName, amount));
    }

    public List<InventoryItem> GetInventoryItems() { return inventoryItems; }

    public void ClearSerializedCollectedItems() { todayCollectedItems.Clear(); }

    public void DisablePlayerMovement(Object key, bool disable)
    {

        if (disable)
        {
            if (!disablePlayerMovementLocks.Contains(key))       // NOTE: to prevent double keying
            {
                disablePlayerMovementLocks.Add(key);
            }
        }
        else
        {
            disablePlayerMovementLocks.Remove(key);
        }
    }

    public bool IsPlayerMovementDisabled()
    {
        return disablePlayerMovementLocks.Count > 0;
    }
}
