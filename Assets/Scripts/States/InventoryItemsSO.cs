﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="Inventory Items Database", menuName="States/Inventory Items Database")]
public class InventoryItemsSO : ScriptableObject
{
    [System.Serializable]
    public class ItemReference
    {
        public string itemName;
        public string[] itemDescriptionText;
        public int itemValue;
    }

    [SerializeField]
    public ItemReference[] itemReferences;

    public ItemReference GetItemByName(string itemName)
    {
        foreach (ItemReference itemRef in itemReferences)
        {
            if (itemRef.itemName.ToUpper() == itemName.ToUpper())
            {
                return itemRef;
            }
        }

        // Throw error but give default values that will work!
        // HACK: maybe there could be a better way to keep the items working while also throwing errors.
        ItemReference defaultItem = new ItemReference();
        defaultItem.itemName = itemName;
        defaultItem.itemDescriptionText = new string[] { "THIS ITEM IS NOT IN THE DATABASE. PLEASE ADD IT INTO THE SCRIPTABLE OBJECT" };
        defaultItem.itemValue = 10;
        Debug.LogError("Requested inventory item does not exist inside the scriptable object database: \"" + itemName + "\"", this);
        return defaultItem;
    }
}
