﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="Stations", menuName="States/Stations")]
public class StationsSO : ScriptableObject
{
    [SerializeField]
    public Station[] stations;
}
