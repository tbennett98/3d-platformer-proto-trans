﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{
    public const string MESSAGE_NAME = "ReceiveAttackHazard";

    public int offensePwr = 1;
    public int poise = 3;

    public Vector3 knockbackDirection;
    public bool faceKnockbackTowardsOther;

    private MovementReporter reporter;

    void Start()
    {
        reporter = GetComponent<MovementReporter>();
    }

    void OnTriggerEnter(Collider other)
    {
        other.SendMessage(MESSAGE_NAME, this, SendMessageOptions.DontRequireReceiver);
    }

    public Vector3 CalculateKnockback(Vector3 otherPos)
    {
        if (!faceKnockbackTowardsOther)
        {
            return knockbackDirection;
        }

        // Calc with otherPos
        Vector3 direction = otherPos - transform.position;
        direction.y = 0f;
        direction.Normalize();

        Vector3 knockback = direction;
        knockback *= knockbackDirection.z;
        knockback.y = knockbackDirection.y;
        knockback += new Vector3(direction.z, 0f, -direction.x) * knockbackDirection.x;
        return knockback;
    }
}
