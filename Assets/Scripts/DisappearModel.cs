﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearModel : MonoBehaviour
{
    public float maxDisappearDistance = 0.5f;
    private Transform mainCamera;
    private SkinnedMeshRenderer[] myModels;
    private bool renderEnabled = true;

    void Start()
    {
        mainCamera = Camera.main.transform;
        myModels = GetComponentsInChildren<SkinnedMeshRenderer>(false);
    }

    void Update()
    {
        // Check if should display or not
        if ((transform.position - mainCamera.position).sqrMagnitude > maxDisappearDistance * maxDisappearDistance)
        {
            // Show the renderers
            if (!renderEnabled)
            {
                foreach (SkinnedMeshRenderer renderer in myModels)
                {
                    renderer.enabled = true;
                }
                renderEnabled = true;
            }
        }
        else
        {
            // Hide the renderers
            if (renderEnabled)
            {
                foreach (SkinnedMeshRenderer renderer in myModels)
                {
                    renderer.enabled = false;
                }
                renderEnabled = false;
            }
        }
    }
}
