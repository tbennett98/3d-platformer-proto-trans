﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TradingEventUtils : MonoBehaviour
{
    public GameState gameState;

    [Header("Buying props")]
    public CollectableMaterial buyingMaterial;
    public UnityEvent buySuccessEvent;
    public UnityEvent buyFailEvent;

    public void DoBuy(int amount)
    {
        // Check if enough money in gamestate, and then commit to doing the buy
        if (gameState.money >= amount)
        {
            gameState.AddInventoryItem(buyingMaterial.materialName, buyingMaterial.materialAmount);     // HACK: Adding the material like this is not very good for the system, since ideally you should be able to call the OnInteracted() method, however it kills the gameObject too so that's why it's a little unfeasable at the moment
            gameState.money -= amount;
            buySuccessEvent.Invoke();
        }
        else
        {
            buyFailEvent.Invoke();
        }
    }

    // TODO: Make a sell system, but for now this will be a little interesting...
    // public void DoSell(int amount)
    // {

    // }
}
