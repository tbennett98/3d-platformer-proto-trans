﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shopkeeper : MonoBehaviour
{
    public GameState gameState;
    public InventoryItemsSO inventoryItemsDatabase;

    public GameObject menuUI;
    public Text uiSellInventoryText;

    public InputField uiSellItemInputField;
    public InputField uiSellQuantityInputField;

    public Text uiError;

    private InteractableEntity interactableEntity;
    private bool isMenuUIOpen = false;
    private bool waitAFrame = false;        // HACK: this is so that GetbuttonDown isn't called at the same time as the OnInteracted() since they use the same button

    void Start()
    {
        interactableEntity = GetComponent<InteractableEntity>();
    }

    void Update()
    {
        if (waitAFrame)
        {
            waitAFrame = false;
            return;
        }

        // Exit out of the shopping ui
        if (isMenuUIOpen && Input.GetButtonDown("Interact"))
        {
            ToggleSellScreen();
            interactableEntity.UndoBusyFlag();
        }
    }

    public void StartShopkeeperScreen()
    {
        waitAFrame = true;
        ToggleSellScreen();
    }

    private void ToggleSellScreen()
    {
        isMenuUIOpen = !isMenuUIOpen;

        // Set everything depending on the flags
        menuUI.SetActive(isMenuUIOpen);
        RefreshInventoryView();
    }


    private void RefreshInventoryView()
    {
        string invContents = "";
        foreach (InventoryItem item in gameState.GetInventoryItems())
        {
            int marketValue = inventoryItemsDatabase.GetItemByName(item.name).itemValue;
            invContents += $"{item.name} ({item.amount}):\t${marketValue} each\n";
        }
        uiSellInventoryText.text = invContents;
    }
    

    private IEnumerator ShowUIMessageCoroutine(string errorText)
    {
        uiError.text = errorText;
        uiError.gameObject.SetActive(true);
        yield return new WaitForSeconds(1.25f); 
        uiError.gameObject.SetActive(false);
    }


    public void SubmitSelling()
    {
        string itemText = uiSellItemInputField.text;
        int quantity = int.Parse(uiSellQuantityInputField.text);        // No sanitizing! ;)

        // Check if item exists and has the quantity
        InventoryItem invItem = Array.Find(
                gameState.GetInventoryItems().ToArray(),
                element => element.name.ToUpper().Equals(itemText.ToUpper())
            );
        
        if (invItem == null)
        {
            StartCoroutine(ShowUIMessageCoroutine("Item \"" + itemText + "\" does not exist"));
            return;
        }

        // Check if quantity is ok
        if (invItem.amount < quantity)
        {
            StartCoroutine(ShowUIMessageCoroutine("This item does not have enough quantity"));
            return;
        }

        // Subtract the amount and move on
        gameState.AddInventoryItem(invItem.name, -quantity);
        gameState.money += inventoryItemsDatabase.GetItemByName(invItem.name).itemValue * quantity;       // Here is that real monetary system that's been in todo for so long
        RefreshInventoryView();
    }
}
