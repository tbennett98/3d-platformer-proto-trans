﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableMaterial : MonoBehaviour
{
    public string materialName = "Spider";
    public int materialAmount = 1;
    public bool interactabilityEnabled = true;
    public GameState gameState;

    private string guid;

    private InteractableEntity interactableEntity;

    void Start()
    {
        interactableEntity = GetComponent<InteractableEntity>();

        if (gameState != null)
        {
            // Calculate guid
            guid = $"{materialName};{transform.position.ToString()};{materialAmount};{gameObject.name};{gameObject.scene.buildIndex}";
            if (gameState.IsItemCollected(guid))    // Don't show item if it's already been collected today!!!
            {
                Destroy(gameObject);
                return;
            }
        }
    }

    void OnInteracted()
    {
        if (!interactabilityEnabled) return;

        // Collect the item
        AddToInventory();
        gameState.LogCollectedItem(guid);
        interactableEntity.SignalLeftOrDestroyed();
        Destroy(gameObject);
    }

    public void AddToInventory()
    {
        gameState.AddInventoryItem(materialName, materialAmount);
    }
}
