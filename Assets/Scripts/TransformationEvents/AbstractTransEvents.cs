using UnityEngine;

public abstract class AbstractTransEvents : MonoBehaviour
{
    public abstract void OnGroundEvent();
    public abstract void DoMidairEvent(ref Vector3 velocity);
    public virtual bool OnWallJumpEnterEvent(Collision other) { return true; }
}
