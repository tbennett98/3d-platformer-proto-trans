using System.Collections;
using UnityEngine;

public class SlimeEvents : AbstractTransEvents
{
    public GameState gameState;

    public GameObject slimeballModel;
    public GameObject mainModels;
    public Collider slimeCollider;
    public Collider mainCollider;

    public AnimationCurve accelerationCurve;
    public float accelerationAmplitude = 35f;
    public float maxSpeed = 100f;

    [Header("Throw Boomerang Props")]
    public OverTheShoulderLookAround overTheShoulderLookAround;
    public float overTheShoulderYoff;
    private bool isChargingBoomThrow = false;
    public float boomerangThrowChargeTime = 1f;
    private float boomerangThrowChargeTimer;
    public GameObject boomerangProjectilePrefab;
    private bool hasBoomerang = true;
    public GameObject boomerangOnModel;

    private Vector3 previousNormal = Vector3.up;
    private float rememberNormalDebounce;

    private Rigidbody rbody;
    private ThirdPersonMovementRigidbody tpmr;

    private bool isSlimeMode;
    private float debounce;

    [Header("General Props")]
    public FearBrain fearBrain;
    public BulletTimeManager bulletTimeManager;

    private bool prevFireSpecial;


    void Start()
    {
        rbody = GetComponent<Rigidbody>();
        tpmr = GetComponent<ThirdPersonMovementRigidbody>();

        isSlimeMode = false;
        slimeballModel.SetActive(false);
    }

    void FixedUpdate()
    {
        if (isSlimeMode)
        {
            Vector3 input = 
                Quaternion.Euler(0f, tpmr.cam.eulerAngles.y, 0f)
                    * Vector3.ClampMagnitude(
                        new Vector3(
                            Input.GetAxisRaw("Horizontal"),
                            0f,
                            Input.GetAxisRaw("Vertical")
                        ),
                        1f
                    );
            
            if (gameState.IsPlayerMovementDisabled())
            {
                input = Vector3.zero;
            }

            // rbody.velocity = Vector3.ClampMagnitude(rbody.velocity, maxSpeed);
            Vector3 flatVelo = Quaternion.FromToRotation(previousNormal, Vector3.up) * rbody.velocity;  flatVelo.y = 0f;
            float limit = /*Mathf.Pow(*/Vector3.Dot(flatVelo.normalized, input.normalized) / 2f + 0.5f/*, 8f)*/;

            rbody.AddForce(
                Quaternion.FromToRotation(Vector3.up, previousNormal)
                    * input
                    * accelerationCurve.Evaluate(flatVelo.magnitude / maxSpeed * limit)
                    * accelerationAmplitude * 100f
                    * Time.deltaTime,
                ForceMode.Acceleration
            );

            // Debug.Log(
            //     (
            //         // /*accelerationCurve.Evaluate*/(flatVelo.magnitude / maxSpeed * limit)
            //         previousNormal
            //     )
            //     + " " + Time.frameCount
            // );


            // Change facing direction (for resetting back
            // to regular slime and also for L targeting)
            if (new Vector3(rbody.velocity.x, 0f, rbody.velocity.z).magnitude > 0.2f)
            {
                float facingAngle = Mathf.Atan2(rbody.velocity.x, rbody.velocity.z) * Mathf.Rad2Deg;
                tpmr.models.eulerAngles = new Vector3(0f, facingAngle,  0f);
                tpmr.SetFacingTargetAngle(facingAngle, true);
            }
        }

        // Process debounce of the vector
        if (rememberNormalDebounce > 0f)
        {
            rememberNormalDebounce -= Time.deltaTime;
        }
        else
        {
            previousNormal = Vector3.up;
        }
    }

    void Update()
    {
        if (gameState.currentTransformation != (int)Transformations.SLIME) return;

        bool fireSpecial = Input.GetAxis("Fire Special") > 0.5f && !gameState.IsPlayerMovementDisabled();

        if (isSlimeMode)
        {
            if (debounce > 0f)
            {
                debounce -= Time.deltaTime;
            }
            else
            {
                if (Input.GetButtonDown("Jump") && !gameState.IsPlayerMovementDisabled())
                {
                    SetSlimeMode(false);
                    tpmr.ChangeState(PlayerStates.JUMP);
                }
            }
        }
        else
        {
            // See if can do boomerang throw (Do if on ground)
            if (tpmr.onGround && hasBoomerang)
            {
                if (fireSpecial && !prevFireSpecial)
                {
                    overTheShoulderLookAround.SetEnableLookAround(true, overTheShoulderYoff);
                    isChargingBoomThrow = true;
                    boomerangThrowChargeTimer = 0f;
                }
            }

            if (isChargingBoomThrow)
            {
                boomerangThrowChargeTimer += Time.deltaTime;

                if (boomerangThrowChargeTimer > boomerangThrowChargeTime)
                {
                    if (!fireSpecial)
                    {
                        isChargingBoomThrow = false;
                        StartCoroutine(ThrowBoomerangAfter(0.1f));
                    }
                }
            }
            tpmr.animatorManager.DoSpecialAttack(isChargingBoomThrow);
        }
        prevFireSpecial = fireSpecial;
    }


    public override void OnGroundEvent()
    {
        // canMidairJump = true;
    }

    public override void DoMidairEvent(ref Vector3 velocity)
    {
        SetSlimeMode(true);
    }

    private void SetSlimeMode(bool flag)
    {
        isSlimeMode = flag;
        rbody.useGravity = flag;
        tpmr.enabled = !flag;

        slimeballModel.SetActive(flag);
        mainModels.SetActive(!flag);
        slimeCollider.enabled = flag;
        mainCollider.enabled = !flag;

        if (!flag)
        {
            tpmr.SetVelocity(rbody.velocity);
        }

        if (flag)
        {
            debounce = 0.25f;
        }
    }

    void BoomerangCaught()
    {
        hasBoomerang = true;
        boomerangOnModel.SetActive(true);
    }

    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag.Contains("ground"))
        {
            Vector3 avgNormal = Vector3.zero;
            // for (int i = 0; i < other.contactCount; i++)
            {
                avgNormal += other.GetContact(0).normal;
            }

            previousNormal = avgNormal.normalized;
            rememberNormalDebounce = 0.2f;
        }
    }

    private IEnumerator ThrowBoomerangAfter(float seconds)
    {
        yield return new WaitForSecondsRealtime(seconds);
        overTheShoulderLookAround.SetEnableLookAround(false);

        // Do boomerang throw
        BoomerangBehavior bb =
            Instantiate(
                boomerangProjectilePrefab,
                transform.position + tpmr.models.transform.rotation * new Vector3(0f, 0f, 1.5f),
                overTheShoulderLookAround.transform.rotation
            ).GetComponent<BoomerangBehavior>();
        bb.returnPostion = transform;
        bb.fearBrain = fearBrain;
        bb.bulletTimeManager = bulletTimeManager;
        hasBoomerang = false;
        boomerangOnModel.SetActive(false);
    }
}