using System.Collections;
using UnityEngine;

public class HumanEvents : AbstractTransEvents
{
    public GameState gameState;

    [Header("Midair Props")]
    public float jumpHeight = 10f;
    public float floatMaxGrav = 4f;

    [Header("Float Off Ground Props")]
    public Transform bottomOfCollider;
    public float amountToFloatOffGround = 1f;
    public float timeToAccelFloatMax = 1f;
    private float timeToAccelFloatMaxTimer;
    public AnimationCurve floatUpCurve;

    private ThirdPersonMovementRigidbody tpmr;
    private Rigidbody body;
    private PlayerAnimatorManager manager;
    private FocusCamera focusCamera;

    private bool canMidairJump;

    void Start()
    {
        tpmr = GetComponent<ThirdPersonMovementRigidbody>();
        body = GetComponent<Rigidbody>();
        focusCamera = GetComponent<FocusCamera>();
        manager = tpmr.animatorManager;
        canMidairJump = true;
    }

    void FixedUpdate()
    {
        if (gameState.currentTransformation != (int)Transformations.HUMAN || !tpmr.enabled) return;

        bool fireSpecial = Input.GetAxis("Fire Special") > 0.5f && !gameState.IsPlayerMovementDisabled();

        if (fireSpecial && !tpmr.IsState(PlayerStates.HELI_SPIN))
        {
            tpmr.ChangeState(PlayerStates.HELI_SPIN);
        }
        else if (!fireSpecial && tpmr.IsState(PlayerStates.HELI_SPIN))      // Only when was in helispin state
        {
            tpmr.ChangeState(PlayerStates.FALLING);
        }

        // Helicopter falling
        Vector3 velo = tpmr.GetVelocity();
        if (fireSpecial && velo.y <= 0f)
        {
            // Case 1: Float above the ground
            RaycastHit hitInfo;
            const float yOffset = 0.5f;
            bool hit = Physics.Raycast(bottomOfCollider.position + new Vector3(0f, yOffset, 0f), Vector3.down, out hitInfo, amountToFloatOffGround + yOffset, tpmr.raycastLayers);

            timeToAccelFloatMaxTimer += Time.deltaTime;
            if (hit)
            {
                float amountToFloatOffGroundReal = amountToFloatOffGround * floatUpCurve.Evaluate(timeToAccelFloatMaxTimer / timeToAccelFloatMax);
                float hitDistance = hitInfo.distance - yOffset;

                if (hitDistance < amountToFloatOffGroundReal)
                {
                    float newVeloY = (amountToFloatOffGroundReal - hitDistance) * 35f;      // Perfect setPosition-type movement would be (* 50f) btw
                    body.velocity += new Vector3(0f, newVeloY > velo.y ? newVeloY : 0f, 0f);
                }
            }

            // Case 2: falling down
            if (!tpmr.onGround)
            {
                Vector3 velocity = tpmr.GetVelocity();
                if (velocity.y < -floatMaxGrav)
                {
                    velocity.y = -floatMaxGrav;
                    tpmr.SetVelocity(velocity);
                }
            }
        }
        else
        {
            timeToAccelFloatMaxTimer = 0f;
        }

        manager.IsMidairHeliJump(fireSpecial);
    }

    void ReportWind(object[] inputs)
    {
        // Only react if human and helispin
        if (gameState.currentTransformation != (int)Transformations.HUMAN || !tpmr.enabled) return;

        bool fireSpecial = Input.GetAxis("Fire Special") > 0.5f && !gameState.IsPlayerMovementDisabled();
        if (!fireSpecial || tpmr.IsState(PlayerStates.LEDGE_GRAB)) return;

        Vector3 windVelocity = (Vector3)inputs[0];
        float maxMagnitude = (float)inputs[1];

        // Do increase in velocity!
        Vector3 velocity = tpmr.GetVelocity();
        if (velocity.sqrMagnitude < maxMagnitude * maxMagnitude)
        {
            velocity += windVelocity * Time.deltaTime * 50f;
            velocity = Vector3.ClampMagnitude(velocity, maxMagnitude);
        }
        tpmr.SetVelocity(velocity);
    }

    public override void OnGroundEvent()
    {
        canMidairJump = true;
    }

    public override bool OnWallJumpEnterEvent(Collision other)
    {
        // canMidairJump = true;    // TODO: See if design should allow for multiple heli spins in the air if you wall jump
        return true;
    }

    public override void DoMidairEvent(ref Vector3 velocity)
    {
        if (!canMidairJump) return;
        canMidairJump = false;

        // Keep the x and z velocity the
        // same so that it's different feel ;)
        //
        // Scratch that!!! (timo)
        manager.DoHumanFrontFlip();

        Vector3 targetDirection = tpmr.GetTargetDirection();
        if (targetDirection.magnitude > 0.1f)
        {
            Vector3 flatVelo = velocity;    flatVelo.y = 0f;
            float magnitude = flatVelo.magnitude;
            velocity = Quaternion.LookRotation(targetDirection, Vector3.up) * new Vector3(0f, 0f, magnitude);
            tpmr.SetFacingTargetAngle(Mathf.Atan2(targetDirection.x, targetDirection.z) * Mathf.Rad2Deg, true);
        }

        velocity.y = jumpHeight;
    }
}