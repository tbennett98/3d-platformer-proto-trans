using System.Collections;
using UnityEngine;

public class MonsterEvents : AbstractTransEvents
{
    public GameState gameState;

    private ThirdPersonMovementRigidbody tpmr;
    private PlayerAnimatorManager manager;
    public FocusCamera focusCamera;

    [Header("Diving Params")]
    public Vector2 drillDiveVec;
    public Vector2 knockbackVec;
    public Vector2 diveExitVec;
    public float knockbackTimeOnceGrounded = 1f;

    [Header("Climbing Params")]
    public float climbAccel = 5f;
    public Vector2 climbSpeed = new Vector2(3f, 3f);
    private Vector2 currentClimbSpeed = new Vector2();
    public float climbSkinFromWall = 0.5f;
    private Vector3 climbFacingDir;

    private float drillDiveTimer;
    private Vector3 diveDirection;

    private Rigidbody body;

    // Private variables for keeping track
    // of the state of the monster transformation
    // program.
    private bool canMidairJump, firstMidairJumpFrame;
    private bool isDrillDiving;
    private bool isKnockbacked;
    private float cachedKnockbackFacingDirection;
    private bool cancelDrillDiving;
    private bool endKnockbackCoroutineLock;
    private Vector3 diveVelo = new Vector3();

    private bool prevJumpInput;

    [Header("Bow and Arrow Props")]
    public OverTheShoulderLookAround overTheShoulderLookAround;
    public float overTheShoulderYoff;
    public GameObject arrowProjectilePrefab;
    public Vector2 arrowSpeedMinMax;
    public Vector2 arrowChargeTimeMinMax;
    private bool isChargingBowAndArrow;
    private float arrowChargeTimer;
    private IEnumerator shoulderCamCoroutineRef = null;

    [Header("General Props")]
    public FearBrain fearBrain;
    public BulletTimeManager bulletTimeManager;

    private bool prevFireSpecial;


    void Start()
    {
        tpmr = GetComponent<ThirdPersonMovementRigidbody>();
        body = GetComponent<Rigidbody>();
        manager = tpmr.animatorManager;
        canMidairJump = true;
        isDrillDiving = false;
    }

    void FixedUpdate()
    {
        if (gameState.currentTransformation != (int)Transformations.MONSTER) return;

        bool inputJump = !prevJumpInput && Input.GetButton("Jump") && !gameState.IsPlayerMovementDisabled();
        bool fireSpecial = Input.GetAxis("Fire Special") > 0.5f && !gameState.IsPlayerMovementDisabled();

        // Check if grabbed ledge
        if (tpmr.IsState(PlayerStates.LEDGE_GRAB))
        {
            canMidairJump = true;
            isDrillDiving = false;
            tpmr.SetDisableProcess(false);      // TODO: Make it so that this doesn't get spammed bc of the isClimbing check

            // Purge other implied variables
            firstMidairJumpFrame = false;
            isKnockbacked = false;
            cancelDrillDiving = false;
            endKnockbackCoroutineLock = false;

            manager.DoMonsterKnockback(false);
            manager.DoMonsterDiveBounce(false);
        }

        // Process climbing
        if (tpmr.IsState(PlayerStates.SCALING_WALL))
        {
            bool passed = false;
            RaycastHit hitData = default(RaycastHit);
            // See if should mirror x input axis
            float dot = Vector3.Dot(climbFacingDir, tpmr.cam.rotation * Vector3.forward);

            currentClimbSpeed = Vector2.MoveTowards(
                currentClimbSpeed,
                new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) * (gameState.IsPlayerMovementDisabled() ? Vector2.zero : climbSpeed),
                climbAccel * 10f * Time.deltaTime
            );

            Vector3 inputMvt =
                Quaternion.Euler(0f, tpmr.GetFacingTargetAngle(), 0f)
                    * new Vector3(
                        currentClimbSpeed.x * (dot < -0.25f ? -1f : 1f),
                        0f,
                        currentClimbSpeed.y
                    );

            // Leave via wall jump?
            if (inputJump)
            {
                passed = false;
                tpmr.DoWallJump(-climbFacingDir, true);
                tpmr.ChangeState(PlayerStates.FALLING);
                tpmr.ApplyVelocity();
            }
            else
            {
                passed = Physics.Raycast(tpmr.GetCollider().transform.position + tpmr.GetCollider().center, climbFacingDir, out hitData, 3.5f, tpmr.raycastLayers);
            }

            if (passed && hitData.transform.tag.Contains("climbable"))
            {
                // Move in the wall's normal
                tpmr.SetVelocity(new Vector3(0f, -1f, 0f));     // HACK: to force checking for ledgegrabs
                body.MovePosition(hitData.point + hitData.normal * (climbSkinFromWall + tpmr.GetCollider().radius) - tpmr.GetCollider().center);
                body.velocity = Quaternion.FromToRotation(Vector3.up, hitData.normal) * inputMvt;

                tpmr.SetFacingTargetAngle(Mathf.Atan2(-hitData.normal.x, -hitData.normal.z) * Mathf.Rad2Deg, false);
            }
            else
            {
                // Exit from climbing
                currentClimbSpeed = new Vector2();      // Reset input
                tpmr.ChangeState(PlayerStates.FALLING);
                tpmr.SetDisableProcess(false);
            }
        }

        // See if can climb wall
        if (tpmr.transformationManager.GetCurrentFormEvent() == this && !isDrillDiving && !tpmr.IsState(PlayerStates.SCALING_WALL) && !tpmr.IsState(PlayerStates.LEDGE_GRAB) && !tpmr.IsState(PlayerStates.JUMP))
        {
            if (tpmr.GetTargetDirection().magnitude > 0.25f)   // First, input and facing a rough area is required
            {
                // Then, we pop the position and rotation of the actor right to the wall
                RaycastHit hitData;
                bool passed = Physics.Raycast(
                    tpmr.GetCollider().transform.position + tpmr.GetCollider().center,
                    Quaternion.Euler(0f, tpmr.GetFacingTargetAngle(), 0f) * Vector3.forward,
                    out hitData,
                    tpmr.GetCollider().radius + 0.5f,
                    tpmr.raycastLayers
                );
                Debug.DrawRay(tpmr.GetCollider().transform.position + tpmr.GetCollider().center, Quaternion.Euler(0f, tpmr.GetFacingTargetAngle(), 0f) * Vector3.forward, Color.blue);

                if (passed && hitData.transform.tag.Contains("climbable"))
                {
                    Vector3 wallNormal = hitData.normal;
                    float facingAngleDiff = Mathf.Abs(Mathf.DeltaAngle(tpmr.GetFacingTargetAngle(), Mathf.Atan2(-wallNormal.x, -wallNormal.z) * Mathf.Rad2Deg));
                    float wallAngle = Vector3.Angle(wallNormal, Vector3.up);

                    // Check to see if actually facing the wall
                    if (facingAngleDiff <= 45f &&
                        wallAngle >= tpmr.wallJumpNormalAngles.x &&
                            wallAngle <= tpmr.wallJumpNormalAngles.y &&
                        (tpmr.GetVelocity().y < 0f || (tpmr.IsState(PlayerStates.RUNNING) && tpmr.onGround)))     // NOTE: so that when midair don't check if onground in case of false positives.
                    {
                        climbFacingDir = -new Vector3(wallNormal.x, 0f, wallNormal.z).normalized;

                        // Flag climbing on rough surface
                        tpmr.ChangeState(PlayerStates.SCALING_WALL);
                        tpmr.SetDisableProcess(true);
                        StartCoroutine(FocusCameraForTime(0.25f));
                    }
                }
            }
        }

        // Do dive processing
        if (isDrillDiving)
        {
            // HACK: Let's see if this hack works...
            // It's supposed to let the first Jump button
            // press get input-eaten, then let subsequent
            // Jump btn down inputs be valid.
            if (inputJump && !firstMidairJumpFrame)
            {
                cancelDrillDiving = true;
            }
            if (firstMidairJumpFrame)
            {
                firstMidairJumpFrame = false;
            }

            // Do drill dive!
            Vector3 horizDiveVelo = diveDirection * drillDiveVec.x;
            diveVelo.x = horizDiveVelo.x;   diveVelo.z = horizDiveVelo.z;
            if (drillDiveTimer == 0f) diveVelo.y = drillDiveVec.y;
            diveVelo += Physics.gravity * Time.deltaTime;
            tpmr.SetVelocity(diveVelo);
            body.velocity = diveVelo;

            // Break from input
            if (cancelDrillDiving)
            {
                isDrillDiving = false;
                // focusCamera.SetForceFocus(false);
                tpmr.ChangeState(PlayerStates.FALLING);
                tpmr.SetDisableInput(false);
                manager.DoMonsterDiveBounce(true);

                // Do exit dive
                Vector3 exitVelo = diveDirection * diveExitVec.x;
                exitVelo.y = diveExitVec.y;
                tpmr.SetVelocity(exitVelo);
                body.velocity = exitVelo;
            }
            drillDiveTimer += Time.deltaTime;
            cancelDrillDiving = false;
        }

        if (isKnockbacked)
        {
            Vector3 knockbackVelo = diveDirection * -knockbackVec.x * Mathf.Max(0f, 0.75f - drillDiveTimer);
            if (drillDiveTimer == 0f) knockbackVelo.y = knockbackVec.y;
            knockbackVelo += Physics.gravity * drillDiveTimer;       // HACK: Simulates gravity in a janky way haha
            tpmr.SetVelocity(knockbackVelo);
            tpmr.SetFacingTargetAngle(cachedKnockbackFacingDirection, false);
            body.velocity = knockbackVelo;
            drillDiveTimer += Time.deltaTime;
        }

        // Check if can do bow and arrow
        if (tpmr.transformationManager.GetCurrentFormEvent() == this && !isDrillDiving && !tpmr.IsState(PlayerStates.SCALING_WALL) && !tpmr.IsState(PlayerStates.LEDGE_GRAB))
        {
            if (fireSpecial && !prevFireSpecial)
            {
                isChargingBowAndArrow = true;
                overTheShoulderLookAround.SetEnableLookAround(true, overTheShoulderYoff);
                arrowChargeTimer = 0f;

                if (shoulderCamCoroutineRef != null)
                {
                    StopCoroutine(shoulderCamCoroutineRef);
                    shoulderCamCoroutineRef = null;
                }
            }
        }

        if (isChargingBowAndArrow)
        {
            arrowChargeTimer += Time.deltaTime;

            if (!fireSpecial && prevFireSpecial)
            {
                isChargingBowAndArrow = false;
                shoulderCamCoroutineRef = TurnOffShoulderCamAfterTime(1f);
                StartCoroutine(shoulderCamCoroutineRef);

                if (arrowChargeTimer > arrowChargeTimeMinMax.x)
                {
                    // Find the normalized between of the charge time
                    float t = (arrowChargeTimer - arrowChargeTimeMinMax.x) / (arrowChargeTimeMinMax.y - arrowChargeTimeMinMax.x);
                    float speed = Mathf.Lerp(arrowSpeedMinMax.x, arrowSpeedMinMax.y, t);

                    // Create and load up prefab
                    ArrowBehavior ab =
                        Instantiate(
                            arrowProjectilePrefab,
                            transform.position + tpmr.models.transform.rotation * new Vector3(0f, 0f, 1.5f),        // HACK: Using playerAnimatorManager since it's where Models is at is hacky
                            overTheShoulderLookAround.transform.rotation
                        ).GetComponent<ArrowBehavior>();
                    ab.fearBrain = fearBrain;
                    ab.bulletTimeManager = bulletTimeManager;
                }
            }
        }

        tpmr.animatorManager.DoSpecialAttack(isChargingBowAndArrow);

        prevJumpInput = Input.GetButton("Jump");
        prevFireSpecial = fireSpecial;
    }

    public override void OnGroundEvent()
    {
        canMidairJump = true;
        firstMidairJumpFrame = true;

        if (tpmr.GetVelocity().y < 0f)
        {
            manager.DoMonsterDiveBounce(false);
        }

        if (tpmr.IsState(PlayerStates.SCALING_WALL) && Input.GetAxisRaw("Vertical") < 0f && !gameState.IsPlayerMovementDisabled())
        {
            tpmr.ChangeStateToGrounded();     // Cancel out when hit the ground while lowering
            tpmr.SetDisableProcess(false);
        }

        if (isDrillDiving)
        {
            cancelDrillDiving = true;
        }

        if (isKnockbacked && !endKnockbackCoroutineLock)
        {
            StartCoroutine(EndKnockbackCoroutine());
        }
    }

    public override bool OnWallJumpEnterEvent(Collision other)
    {
        // Use this as knockback triggering code
        bool allowWallJump = true;

        if (isDrillDiving)
        {
            allowWallJump = false;

            // Perform knockback
            isDrillDiving = false;
            cancelDrillDiving = false;
            // focusCamera.SetForceFocus(false);       // Note: disabled input is still happening
            drillDiveTimer = 0f;                    // HACK: To reset the falling timer hack
            isKnockbacked = true;
            manager.DoMonsterKnockback(true);
            cachedKnockbackFacingDirection = tpmr.GetFacingTargetAngle();

            // Search if other object needs to be broken
            if (other.gameObject.tag.Contains("ground"))
            {
                other.gameObject.SendMessage("OnMonsterBash");
            }
        }

        if (isKnockbacked)
        {
            allowWallJump = false;
        }

        return allowWallJump;
    }

    public override void DoMidairEvent(ref Vector3 velocity)
    {
        if (!canMidairJump) return;
        canMidairJump = false;

        // Start drill dive!
        isDrillDiving = true;
        tpmr.ChangeState(PlayerStates.CHARGE_DIVE);
        manager.DoMonsterDiveBash();
        drillDiveTimer = 0f;
        // focusCamera.SetForceFocus(true);
        tpmr.SetDisableInput(true);

        // Base drill direction off of the facing direction
        diveDirection = Quaternion.Euler(0f, tpmr.models.eulerAngles.y, 0f) * Vector3.forward;
    }

    private IEnumerator EndKnockbackCoroutine()
    {
        if (endKnockbackCoroutineLock) yield return null;
        endKnockbackCoroutineLock = true;
        yield return new WaitForSecondsRealtime(knockbackTimeOnceGrounded);
        endKnockbackCoroutineLock = false;

        // End Knockback
        isKnockbacked = false;
        manager.DoMonsterKnockback(false);
        tpmr.SetDisableInput(false);    // Since disable input isn't turned off for knockbacks, we turn it off now
    }

    private IEnumerator TurnOffShoulderCamAfterTime(float seconds)
    {
        yield return new WaitForSecondsRealtime(seconds);
        if (!isChargingBowAndArrow)
        {
            overTheShoulderLookAround.SetEnableLookAround(false);
        }
    }

    private IEnumerator FocusCameraForTime(float seconds)
    {
        focusCamera.SetForceFocus(true);
        yield return new WaitForSecondsRealtime(seconds);
        focusCamera.SetForceFocus(false);
    }
}