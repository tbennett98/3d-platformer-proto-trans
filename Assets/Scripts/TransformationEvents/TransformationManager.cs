﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TransformationManager : MonoBehaviour
{
    [Header("Menu Props")]
    public GameObject uiMenuObject;
    public EventSystem uiMenuFormEventSystem;

    [Header("Global Props")]
    public GameState gameState;

    public CapsuleCollider controller;
    public SphereCollider onGroundTrigger;
    public ThirdPersonMovementRigidbody thirdPersonMovement;

    [Header("Different character props")]
    public GameObject[] goMeshes;
    public AbstractTransEvents[] transformationScripts;
    public float[] speed = { 15f, 15f, 20f, 15f };
    public float[] jumpSpeed = { 20f, 15f, 20f, 20f };
    public Vector2[] wallJumpJumpVec = {
        new Vector2(12, 12),
        new Vector2(12, 12),
        new Vector2(0, 15),
        new Vector2(12, 12)
    };
    public float[] wallClimbSpeed = { 10.75f, 10.75f, 10.75f, 10.75f };
    public float[] wallClimbTime = { 0.15f, 0.15f, 0.15f, 0.15f };
    public Vector2[] wallClimbGravMinMax = {
        new Vector2(5, 20),
        new Vector2(5, 20),
        new Vector2(2, 2),
        new Vector2(5, 20)
    };
    public bool[] wallJumpUpwards = { false, false, true, false };
    public bool[] wallClimbFallDisabled = { false, true, false, false };
    public Vector2[] ledgeClimboutVelo = {
        new Vector2(6, 12),
        new Vector2(10, 17),
        new Vector2(6, 12),
        new Vector2(6, 12)
    };

    [Header("Collider positioning")]
    public float[] controllerSlopeLimit = { 45f, 45f, 70f, 45f };
    public Vector3[] controllerCenter = {
        new Vector3(0, 0, 0.07f),
        new Vector3(0, 0.85f, 0.07f),
        new Vector3(0, -0.05f, 0.07f),
        new Vector3(0, 0.125f, 0.07f)
    };
    public float[] controllerRadius = { 0.25f, 0.75f, 0.2f, 0.25f };
    public float[] controllerHeight = { 2f, 3.7f, 1.9f, 2.25f };

    void Start()
    {
        UpdateTransformation();
    }

    void Update()
    {
        if (Input.GetButtonDown("Switch Forms") && !gameState.IsPlayerMovementDisabled())
        {
            // Open up the change forms menu
            uiMenuObject.SetActive(true);
            Time.timeScale = 0f;

            // Get the correct child and return it as default selected gameobject
            uiMenuFormEventSystem.SetSelectedGameObject(uiMenuObject.transform.GetChild(0).GetChild(gameState.currentTransformation).gameObject);
        }

        if ((Input.GetButtonUp("Switch Forms") || Input.GetButtonDown("Jump") || Input.GetButtonDown("Fire")) && uiMenuObject.activeInHierarchy)
        {
            // Read which form was selected when this event was fired
            int newTransformation = int.Parse(uiMenuFormEventSystem.currentSelectedGameObject.name.Substring(0, 1));

            // Close the change forms menu
            uiMenuObject.SetActive(false);
            Time.timeScale = 1f;

            if (newTransformation != gameState.currentTransformation)
            {
                // Reassign the transformation
                gameState.currentTransformation = newTransformation;
                UpdateTransformation();
            }
        }
    }

    private void UpdateTransformation()
    {
        int requestedForm = gameState.currentTransformation;
        while (!gameState.allowedTransformations[gameState.currentTransformation])
        {
            // Toggle thru forms until loop around or find a form that is allowed
            gameState.currentTransformation = (gameState.currentTransformation + 1) % 4;
            if (gameState.currentTransformation == requestedForm)
            {
                Debug.LogError("None of the transformations are enabled!");
                break;
            }
        }

        for (int i = 0; i < goMeshes.Length; i++)
        {
            goMeshes[i].SetActive(i == gameState.currentTransformation);
        }

        thirdPersonMovement.maxSpeed = speed[gameState.currentTransformation];
        thirdPersonMovement.jumpHeight = jumpSpeed[gameState.currentTransformation];
        thirdPersonMovement.transSlopeLimit = controllerSlopeLimit[gameState.currentTransformation];
        thirdPersonMovement.wallJumpJumpVec = wallJumpJumpVec[gameState.currentTransformation];
        thirdPersonMovement.wallClimbSpeed = wallClimbSpeed[gameState.currentTransformation];
        thirdPersonMovement.wallClimbTime = wallClimbTime[gameState.currentTransformation];
        thirdPersonMovement.wallClimbGravityMin = wallClimbGravMinMax[gameState.currentTransformation].x;
        thirdPersonMovement.wallClimbGravityMax = wallClimbGravMinMax[gameState.currentTransformation].y;
        thirdPersonMovement.wallJumpUpwards = wallJumpUpwards[gameState.currentTransformation];
        thirdPersonMovement.wallClimbFallDisabled = wallClimbFallDisabled[gameState.currentTransformation];
        thirdPersonMovement.ledgeClimboutVelo = ledgeClimboutVelo[gameState.currentTransformation];

        controller.center = controllerCenter[gameState.currentTransformation];
        controller.radius = controllerRadius[gameState.currentTransformation];
        controller.height = controllerHeight[gameState.currentTransformation];

        onGroundTrigger.radius = controllerRadius[gameState.currentTransformation] - 0.05f;
        onGroundTrigger.transform.localPosition =
            new Vector3(
                controllerCenter[gameState.currentTransformation].x,
                controllerCenter[gameState.currentTransformation].y
                    - (controllerHeight[gameState.currentTransformation] / 2f)
                    + controllerRadius[gameState.currentTransformation]
                    - 0.1f,
                controllerCenter[gameState.currentTransformation].z
            );
    }

    public int CurrentForm()
    {
        return gameState.currentTransformation;
    }

    public AbstractTransEvents GetCurrentFormEvent()
    {
        return transformationScripts[gameState.currentTransformation];
    }

    public void OnGroundEvent()
    {
        transformationScripts[gameState.currentTransformation].OnGroundEvent();
    }

    public void DoMidairEvent(ref Vector3 velocity)
    {
        transformationScripts[gameState.currentTransformation].DoMidairEvent(ref velocity);
    }

    public bool OnWallJumpEnterEvent(Collision other)
    {
        return transformationScripts[gameState.currentTransformation].OnWallJumpEnterEvent(other);
    }

    public static Transform GetNearestObjPosition(Vector3 origin, float searchDistance, LayerMask mask)
    {
        Collider[] colliders = Physics.OverlapSphere(origin, searchDistance, mask);
        Transform nearestPos = null;
        float curDistSqr = float.MaxValue;

        foreach (Collider collider in colliders)
        {
            Vector3 distVector = collider.transform.position - origin;
            float sqrDistance = distVector.sqrMagnitude;
            if (curDistSqr > sqrDistance)
            {
                // Update best
                nearestPos = collider.transform;
                curDistSqr = sqrDistance;
            }
        }
        return nearestPos;
    }
}
