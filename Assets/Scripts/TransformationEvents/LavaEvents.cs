using UnityEngine;

public class LavaEvents : AbstractTransEvents
{
    public GameState gameState;

    private ThirdPersonMovementRigidbody tpmr;
    private PlayerAnimatorManager manager;

    public float superJumpSpeed = 100f;
    private bool superJumped = false;
    private bool canMidairJump;
    private float storedFacingDirection;

    void Start()
    {
        tpmr = GetComponent<ThirdPersonMovementRigidbody>();
        manager = tpmr.animatorManager;
    }

    void Update()
    {
        if (gameState.currentTransformation != (int)Transformations.LAVA) return;

        // Keep camera pointed downwards if super jumping
        if (superJumped)
        {
            tpmr.orbit.m_YAxis.Value += 1.5f * Time.deltaTime;   // Point cam downwards
        }
    }

    public override void OnGroundEvent()
    {
        if (superJumped)
        {
            // Disable player movement for a little bit
            StartCoroutine(tpmr.TempDisableInputCoroutine(0.1f));
            tpmr.SetVelocity(Vector3.zero);
            tpmr.SetFacingTargetAngle(storedFacingDirection, true);
        }

        canMidairJump = true;
        superJumped = false;
    }
    public override void DoMidairEvent(ref Vector3 velocity)
    {
        if (!canMidairJump) return;
        canMidairJump = false;
        superJumped = true;

        storedFacingDirection = tpmr.models.eulerAngles.y;
        velocity = new Vector3(0f, superJumpSpeed, 0f);
        manager.DoLavaRocketJump();
    }
}