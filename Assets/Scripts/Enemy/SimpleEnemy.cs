﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///// TODO: NOTE: This is extremely simple/temporary, in that there is no enemy AI or really anything, rather that the enemy will know how to use the different states dependent upon the group
public class SimpleEnemy : MonoBehaviour//, IEnemy
{
    // IDEA: have a bezier curve between the left and right of the player and the point of the enemy, so they go in a curve towards the sides of the player
    public RotateObject weapon;
    [Header("TEMP: Color props")]
    public Color materialIdleColor;
    public Color materialDefenseColor;
    public Color materialOffenseColor;
    private Material myMaterial;

    [Header("Boundary props")]
    public float defensiveDistance = 5f;
    public float relaxDefenseDistance = 7.5f;
    public float offensiveDistance = 5f;
    public float relaxOffensiveDistance = 7.5f;
    public float revertToOffensiveApproachDistance = 5f;
    public float offenseAttackRange = 2f;
    public float offenseAttackRangePadding = 1f;

    [Header("Movement props")]
    public float acceleration = 2f;

    [Header("Defensive mode props")]
    public float idealDistanceFromTreasure = 4f;
    public float moveToDefendTreasureVelocity = 1f;

    [Header("Offensive mode props")]
    public float offensiveChargeSpeed = 2f;
    public float normalAttackChargeSpeed = 4f;
    public float offensiveSpinSpeed = 5f;
    public float offensiveSpinSpeedRushIn = 10f;
    public float attackCoolOffTime = 0.5f;
    private Vector3 approachPosition;

    private enum SimpleEnemyState
    {
        IDLE, DEFENSIVE,
        OFFENSIVE_APPROACH, NORMAL_ATTACK, RUSH_IN_ATTACK, LAST_RESORT_THROW, ATTACK_COOLOFF,
        DYING
    }

    [Header("State machine")]
    [SerializeField] private SimpleEnemyState currentState;
    private bool normalAttackCoroutineRunning = false;
    private bool rushInAttackCoroutineRunning = false;
    private bool lastResortAttackCoroutineRunning = false;
    private bool attackCoolOffCoroutineRunning = false;
    private bool dyingCoroutineRunning = false;

    [Header("References")]
    // NOTE: we're just using EnemyGroup to store the location of treasure chests and which one is the closest. This is probably the best is what I'd say, since we don't want this super monolithic centralized ai
    private EnemyGroup myEnemyGroup;     // NOTE: having enemy groups was an okay idea, but I feel like now I need to really get the enemy state machine down, and maybe in the future there can be more of a "warn others" type of functionality, but it'd be better for the enemies to be independent at this point.
    private Rigidbody body;
    private Transform playerTransform;

    void Awake()
    {
        myEnemyGroup = GetComponentInParent<EnemyGroup>();
        // myEnemyGroup.RegisterObserver(this);

        body = GetComponentInParent<Rigidbody>();
        playerTransform = FindObjectOfType<ThirdPersonMovementRigidbody>().models;
        myMaterial = GetComponent<MeshRenderer>().material;
    }

    void Start()
    {
        ChangeState(SimpleEnemyState.IDLE);
    }

    void Update()
    {
        switch (this.currentState)
        {
            case SimpleEnemyState.IDLE:
            case SimpleEnemyState.LAST_RESORT_THROW:
            case SimpleEnemyState.DYING:
            {
                myMaterial.color = materialIdleColor;
            }
            break;

            case SimpleEnemyState.DEFENSIVE:
            {
                myMaterial.color = materialDefenseColor;
            }
            break;
            
            case SimpleEnemyState.OFFENSIVE_APPROACH:
            case SimpleEnemyState.NORMAL_ATTACK:
            case SimpleEnemyState.RUSH_IN_ATTACK:
            case SimpleEnemyState.ATTACK_COOLOFF:
            {
                myMaterial.color = materialOffenseColor;
            }
            break;

            default:
            Debug.LogError("State is not correctly set.", this);
            break;
        }
    }

    void FixedUpdate()
    {
        switch (this.currentState)
        {
            case SimpleEnemyState.IDLE:
            {
                ProccessIdle();

                if (Vector3.Distance(body.position, playerTransform.position) < defensiveDistance)
                {
                    ChangeState(SimpleEnemyState.DEFENSIVE);
                }
            }
            break;

            case SimpleEnemyState.DEFENSIVE:
            {
                ProcessDefensive();

                float distance = Vector3.Distance(body.position, playerTransform.position);
                if (distance < offensiveDistance)
                {
                    ChangeState(SimpleEnemyState.OFFENSIVE_APPROACH);
                }
                else if (distance > relaxDefenseDistance)
                {
                    ChangeState(SimpleEnemyState.IDLE);
                }
            }
            break;
            
            case SimpleEnemyState.OFFENSIVE_APPROACH:
            {
                ProcessOffensiveApproach();

                float distance = Vector3.Distance(body.position, playerTransform.position);
                if (distance > relaxOffensiveDistance)
                {
                    ChangeState(SimpleEnemyState.IDLE);
                }
            }
            break;

            case SimpleEnemyState.NORMAL_ATTACK:
            {
                StartCoroutine(DoNormalAttack(0.25f, 1f));
            }
            break;

            case SimpleEnemyState.RUSH_IN_ATTACK:
            {
                StartCoroutine(DoRushInAttack(0.65f));
            }
            break;

            case SimpleEnemyState.LAST_RESORT_THROW:
            {
                Debug.LogWarning("DO LAST RESORT!!!!");
                StartCoroutine(DoLastResortAttack(1.5f, 50f, 1.5f));
            }
            break;

            case SimpleEnemyState.ATTACK_COOLOFF:
            {
                StartCoroutine(DoAttackCoolOff());
            }
            break;

            case SimpleEnemyState.DYING:
            {
                // Die!
                StartCoroutine(DieTimer(0.5f));
            }
            break;

            default:
            Debug.LogError("State is not correctly set.", this);
            break;
        }
    }

    void OnDrawGizmos()
    {
        switch (this.currentState)
        {
            case SimpleEnemyState.IDLE:
            {
                Gizmos.color = Color.gray;
                Gizmos.DrawWireSphere(transform.position, defensiveDistance);
            }
            break;

            case SimpleEnemyState.DEFENSIVE:
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(transform.position, offensiveDistance);
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(transform.position, relaxDefenseDistance);
            }
            break;
            
            case SimpleEnemyState.OFFENSIVE_APPROACH:
            case SimpleEnemyState.LAST_RESORT_THROW:
            case SimpleEnemyState.DYING:
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, relaxOffensiveDistance);
                Gizmos.color = Color.cyan;
                Gizmos.DrawWireSphere(transform.position, offenseAttackRange);
            }
            break;

            case SimpleEnemyState.NORMAL_ATTACK:
            case SimpleEnemyState.RUSH_IN_ATTACK:
            case SimpleEnemyState.ATTACK_COOLOFF:
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(transform.position, revertToOffensiveApproachDistance);
                Gizmos.color = Color.magenta;
                Gizmos.DrawWireSphere(transform.position, offenseAttackRange);
            }
            break;

            default:
            Debug.LogError("State is not correctly set.", this);
            break;
        }
    }

    private void ProccessIdle()
    {
        // Enemy should go back to defending and just return to base (wherever the nearest treasure chest is)
        weapon.gameObject.SetActive(false);
        body.velocity = new Vector3(0f, body.velocity.y, 0f);
    }

    private void ProcessDefensive()
    {
        // Imagine the enemy facing the player and slowly stepping to the chest or standing ground waiting to see if they're being threatened by the player or not
        weapon.gameObject.SetActive(false);

        GameObject nearestChest = myEnemyGroup.NearestTreasureChest(transform.position);
        if (nearestChest == null)
        {
            body.velocity = new Vector3(0f, body.velocity.y, 0f);
            return;
        }

        Vector3 flatDistance = nearestChest.transform.position - transform.position;
        flatDistance.y = 0f;
        flatDistance *= moveToDefendTreasureVelocity;
        flatDistance -= flatDistance.normalized * idealDistanceFromTreasure;
        body.velocity = Vector3.ClampMagnitude(flatDistance, moveToDefendTreasureVelocity) + new Vector3(0f, body.velocity.y, 0f);
    }

    private void ProcessOffensiveApproach()
    {
        weapon.gameObject.SetActive(false);

        // Choose position to walk to
        Vector3 playerToMe = transform.position - playerTransform.position;
        bool approachRightSide = Vector3.Dot(playerTransform.right, playerToMe.normalized) > 0f;
        if (approachRightSide)
        {
            // Right side
            approachPosition = playerTransform.position + playerTransform.right * (offenseAttackRange - offenseAttackRangePadding);
        }
        else
        {
            // Left side
            approachPosition = playerTransform.position - playerTransform.right * (offenseAttackRange - offenseAttackRangePadding);
        }

        #region Debug Drawline the movement path
        Debug.DrawLine(transform.position, approachPosition, Color.cyan);
        #endregion

        // Only stop if the transform of the player is near you (enemy)
        Vector3 wantedFlatVelocity = Vector3.zero;
        if (Vector3.Distance(transform.position, playerTransform.position) > offenseAttackRange)
        {
            // Move towards the chosen position!!!
            Vector3 flatMovement = approachPosition - body.position;
            flatMovement.y = 0f;
            wantedFlatVelocity = Vector3.ClampMagnitude(flatMovement * offensiveChargeSpeed, offensiveChargeSpeed);

            // Apply the wanted flat velocity!
            Vector3 appliedBodyVelocity = body.velocity;
            appliedBodyVelocity.y = 0f;
            wantedFlatVelocity.y = 0f;
            body.velocity =
                Vector3.MoveTowards(
                    appliedBodyVelocity,
                    wantedFlatVelocity,
                    acceleration * Time.deltaTime
                )
                + new Vector3(0f, body.velocity.y, 0f);
        }
        else
        {
            // "Chase" state is finished. Choose a random attack and go for it!
            ChooseAttack();
        }
    }

    private IEnumerator DoNormalAttack(float projectionDuration, float attackDuration)
    {
        if (normalAttackCoroutineRunning) yield break;
        normalAttackCoroutineRunning = true;

        weapon.gameObject.SetActive(true);

        Vector3 relativePosition = playerTransform.position - body.position;
        weapon.transform.eulerAngles =
            new Vector3(
                0f,
                Mathf.Atan2(relativePosition.x, relativePosition.z) * Mathf.Rad2Deg,
                0f
            );
        weapon.angularVelocity.y = 0f;
        body.velocity = new Vector3(0f, body.velocity.y, 0f);       // NOTE: this isn't getting reset to a velocity of 0, y, 0 all the time so the rigidbody will in fact slide down the slope inevitibly. Maybe if there were a way to set the desired velocity of the rigidbody?
        yield return new WaitForSeconds(projectionDuration);
        
        weapon.angularVelocity.y = offensiveSpinSpeed * (Random.value > 0.5f ? 1f : -1f);
        relativePosition.y = 0f;
        body.velocity = relativePosition.normalized * normalAttackChargeSpeed + new Vector3(0f, body.velocity.y, 0f);
        yield return new WaitForSeconds(attackDuration);

        weapon.gameObject.SetActive(false);
        GotoAttackCoolOffState();

        normalAttackCoroutineRunning = false;
    }

    private IEnumerator DoRushInAttack(float attackDuration)
    {
        if (rushInAttackCoroutineRunning) yield break;
        rushInAttackCoroutineRunning = true;

        weapon.gameObject.SetActive(true);

        Vector3 relativePosition = playerTransform.position - body.position;
        weapon.transform.eulerAngles =
            new Vector3(
                0f,
                Mathf.Atan2(relativePosition.x, relativePosition.z) * Mathf.Rad2Deg,
                0f
            );
        weapon.angularVelocity.y = offensiveSpinSpeedRushIn * (Random.value > 0.5f ? 1f : -1f);
        body.velocity = relativePosition.normalized * offensiveChargeSpeed + new Vector3(0f, body.velocity.y, 0f);
        yield return new WaitForSeconds(attackDuration);

        weapon.gameObject.SetActive(false);
        GotoAttackCoolOffState();

        rushInAttackCoroutineRunning = false;
    }

    private IEnumerator DoLastResortAttack(float chargeWaitTime, float throwSpeed, float waitUntilChase)
    {
        if (lastResortAttackCoroutineRunning) yield break;
        lastResortAttackCoroutineRunning = true;

        // Aim the weapon to throw it!
        weapon.gameObject.SetActive(true);

        Vector3 relativePosition = body.position - playerTransform.position;
        weapon.transform.eulerAngles =
            new Vector3(
                0f,
                Mathf.Atan2(relativePosition.x, relativePosition.z) * Mathf.Rad2Deg,
                0f
            );
        weapon.angularVelocity = Vector3.zero;

        yield return new WaitForSeconds(chargeWaitTime);

        // Redo the calculation bc I want the spear to be direction accurate
        relativePosition = body.position - playerTransform.position;
        weapon.transform.eulerAngles =
            new Vector3(
                0f,
                Mathf.Atan2(relativePosition.x, relativePosition.z) * Mathf.Rad2Deg,
                0f
            );

        // Undo the weapon from enemy and throw it
        // Throw the weapon like a spear!
        weapon.transform.parent = null;
        weapon.GetRigidbody().isKinematic = false;
        weapon.GetRigidbody().useGravity = true;
        weapon.GetRigidbody().constraints = RigidbodyConstraints.None;
        Collider[] colliders = weapon.GetComponentsInChildren<Collider>();
        foreach (Collider collider in colliders)
        {
            collider.enabled = true;
        }

        weapon.enabled = false;
        relativePosition.y = 0f;
        relativePosition.Normalize();
        weapon.GetRigidbody().velocity =
            new Vector3(
                -relativePosition.x * throwSpeed,
                10f,
                -relativePosition.z * throwSpeed
            );

        yield return new WaitForSeconds(waitUntilChase);

        // Go chase after the thrown weapon!
        while (Vector3.Distance(body.position, weapon.transform.position) > 2f)
        {
            Vector3 moveDirection = weapon.transform.position - body.position;
            moveDirection.y = 0f;
            moveDirection = moveDirection.normalized * offensiveChargeSpeed;
            body.velocity = moveDirection + new Vector3(0f, body.velocity.y, 0f);
            yield return null;
        }

        // Grab back the thrown weapon!
        weapon.transform.parent = transform;
        weapon.GetRigidbody().isKinematic = true;
        weapon.GetRigidbody().useGravity = false;
        weapon.GetRigidbody().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        colliders = weapon.GetComponentsInChildren<Collider>();
        foreach (Collider collider in colliders)
        {
            if (!collider.isTrigger)
            {
                collider.enabled = false;
            }
        }
        weapon.enabled = true;
        weapon.transform.localPosition = Vector3.zero;
        weapon.transform.localEulerAngles = Vector3.zero;

        weapon.gameObject.SetActive(false);

        // Exit this state!
        GotoAttackCoolOffState();

        lastResortAttackCoroutineRunning = false;
    }

    private IEnumerator DoAttackCoolOff()
    {
        if (attackCoolOffCoroutineRunning) yield break;
        attackCoolOffCoroutineRunning = true;

        body.velocity = new Vector3(0f, body.velocity.y, 0f);
        yield return new WaitForSeconds(attackCoolOffTime);
        ChooseAttack();

        attackCoolOffCoroutineRunning = false;
    }

    private IEnumerator DieTimer(float seconds)
    {
        if (dyingCoroutineRunning) yield break;
        dyingCoroutineRunning = true;

        yield return new WaitForSeconds(seconds);
        Destroy(gameObject);

        dyingCoroutineRunning = false;
    }

    void HealthDepleted()
    {
        ChangeState(SimpleEnemyState.DYING);
    }

    private void GotoAttackCoolOffState()
    {
        ChangeState(SimpleEnemyState.ATTACK_COOLOFF);
    }

    private void ChooseAttack()
    {
        if (Vector3.Distance(body.position, playerTransform.position) > revertToOffensiveApproachDistance)
        {
            // Player escaped enemy's attacking range.
            // You can either go after them or throw your weapon
            if (Random.value > 0.2f)
            {
                ChangeState(SimpleEnemyState.OFFENSIVE_APPROACH);
            }
            else
            {
                ChangeState(SimpleEnemyState.LAST_RESORT_THROW);
            }
            return;
        }

        // 0.00 - 0.70      Normal attack   (70%)
        // 0.70 - 1.00      Rush in attack  (30%)
        float randomValue = Random.value;

        if (randomValue <= 0.6f)
        {
            ChangeState(SimpleEnemyState.NORMAL_ATTACK);
            return;
        }
        ChangeState(SimpleEnemyState.RUSH_IN_ATTACK);
    }

    private void ChangeState(SimpleEnemyState newState)
    {
        this.currentState = newState;
    }
}
