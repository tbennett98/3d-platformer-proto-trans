﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGroup : MonoBehaviour
{
    public enum EnemyGroupStates
    {
        IDLE,
        DEFENSIVE,      // Sees the player and then on watch
        OFFENSIVE       // Gets too close to the treasure
    }

    public EnemyReaction[] enemies;
    public GameObject[] treasures;

    [Header("Defensive/Offensive Props")]
    public EnemyGroupStates currentState;
    public float treasureDistanceLimit = 5f;
    public float noticeLimit = 15f;
    public float letGoFromOffenseRadius = 25f;

    [Header("Group teammate formation props")]
    public float distanceBetweenEntities = 0.5f;

    private ThirdPersonMovementRigidbody player;

    private List<IEnemy> enemiesObserving;

    void Awake()
    {
        enemiesObserving = new List<IEnemy>();
    }

    void Start()
    {
        ChangeEnemyState(EnemyGroupStates.IDLE);
        player = FindObjectOfType<ThirdPersonMovementRigidbody>();
    }

    void FixedUpdate()
    {
        float distanceToPlayer = ShortestDistanceToPlayer();

        switch (currentState)
        {
            case EnemyGroupStates.IDLE:
            if (distanceToPlayer < noticeLimit)
            {
                ChangeEnemyState(EnemyGroupStates.DEFENSIVE);
            }
            break;

            case EnemyGroupStates.DEFENSIVE:
            if (distanceToPlayer < treasureDistanceLimit)
            {
                ChangeEnemyState(EnemyGroupStates.OFFENSIVE);
            }
            break;

            case EnemyGroupStates.OFFENSIVE:
            // Stay offensive!
            // TODO: Make a way for the enemies to go back to camps once player is far enough away from the enemies themselves this time.
            if (distanceToPlayer > letGoFromOffenseRadius)
            {
                ChangeEnemyState(EnemyGroupStates.DEFENSIVE);
            }
            break;

            default:
            break;
        }
    }

    // void OnDrawGizmos()
    // {
    //     for (int i = 0; i < enemies.Length + treasures.Length; i++)
    //     {
    //         GameObject current;
    //         if (i < enemies.Length)
    //         {
    //             EnemyReaction enemy = enemies[i];
    //             if (enemy == null) continue;
    //             current = enemy.gameObject;
    //         }
    //         else
    //         {
    //             current = treasures[i - enemies.Length];
    //             if (current == null) continue;
    //         }

    //         if (currentState == EnemyGroupStates.OFFENSIVE)
    //         {
    //             // Draw the escape fields
    //             Gizmos.color = Color.green;
    //             Gizmos.DrawWireSphere(current.transform.position, letGoFromOffenseRadius);
    //         }
    //         else
    //         {
    //             // Draw the fields of noticing
    //             Gizmos.color = Color.yellow;
    //             Gizmos.DrawWireSphere(current.transform.position, noticeLimit);
    //             Gizmos.color = Color.red;
    //             Gizmos.DrawWireSphere(current.transform.position, treasureDistanceLimit);
    //         }
    //     }
    // }

    private float ShortestDistanceToPlayer()
    {
        float shortestDistanceSqr = -1f;
        foreach (GameObject treasure in treasures)
        {
            if (treasure == null) continue;
            float distanceSqr = (treasure.transform.position - player.transform.position).sqrMagnitude;
            if (shortestDistanceSqr < 0f ||
                distanceSqr < shortestDistanceSqr)
            {
                shortestDistanceSqr = distanceSqr;
            }
        }

        foreach (EnemyReaction enemy in enemies)
        {
            if (enemy == null) continue;
            float distanceSqr = (enemy.transform.position - player.transform.position).sqrMagnitude;
            if (shortestDistanceSqr < 0f ||
                distanceSqr < shortestDistanceSqr)
            {
                shortestDistanceSqr = distanceSqr;
            }
        }

        return Mathf.Sqrt(shortestDistanceSqr);
    }

    public GameObject NearestTreasureChest(Vector3 position)
    {
        GameObject nearest = null;

        float shortestDistanceSqr = -1f;
        foreach (GameObject treasure in treasures)
        {
            if (treasure == null) continue;
            float distanceSqr = (treasure.transform.position - position).sqrMagnitude;
            if (shortestDistanceSqr < 0f ||
                distanceSqr < shortestDistanceSqr)
            {
                nearest = treasure;
                shortestDistanceSqr = distanceSqr;
            }
        }

        return nearest;
    }

    public void ChangeEnemyState(EnemyGroupStates newState)
    {
        currentState = newState;
        foreach (IEnemy observer in enemiesObserving)
        {
            observer.OnEnemyGroupStateChanged(newState);
        }
    }

    public Transform NearestPlayer()
    {
        return player.models;
    }

    public void RegisterObserver(IEnemy myself)
    {
        if (!enemiesObserving.Contains(myself))
        {
            enemiesObserving.Add(myself);
        }
    }
}
