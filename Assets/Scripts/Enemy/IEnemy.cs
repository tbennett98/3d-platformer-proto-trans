public interface IEnemy
{
    void OnEnemyGroupStateChanged(EnemyGroup.EnemyGroupStates newState);
}