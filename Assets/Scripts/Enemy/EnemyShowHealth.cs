﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyShowHealth : MonoBehaviour
{
    public RectTransform healthBar;
    public RectTransform healthBarImg;
    public Text uiHealthStats;
    public float maxHealthBarSize;
    public Transform whereToDisplayHUD;
    public Canvas canvas;
    private Camera mainCam;
    private bool showHealthBar;

    [Header("Props")]
    public EnemyReaction enemyStats;

    void Start()
    {
        healthBar.gameObject.SetActive(false);
        showHealthBar = false;
        mainCam = Camera.main;
    }

    void Update()
    {
        // Put health bar where it's supposed to be...
        if (showHealthBar)
        {
            // Position health bar
            Vector2 position = mainCam.WorldToScreenPoint(whereToDisplayHUD.position);
            position /= canvas.scaleFactor;
            healthBar.anchoredPosition = position;

            // Size up bar
            float amount = (float)enemyStats.currentHealth / (float)enemyStats.maxHealth * maxHealthBarSize;
            healthBarImg.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, amount);
            uiHealthStats.text = $"{enemyStats.currentHealth}/{enemyStats.maxHealth}";
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            healthBar.gameObject.SetActive(true);
            showHealthBar = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            healthBar.gameObject.SetActive(false);
            showHealthBar = false;
        }
    }
}
