﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyReaction : MonoBehaviour
{
    public int currentHealth;
    public int maxHealth;
    public int poise;
    public float receiveAttackDebounce = 0.25f;
    private float receiveAttackDebounceTimer;
    private Rigidbody body;

    void Start()
    {
        body = GetComponent<Rigidbody>();
        currentHealth = maxHealth;
    }

    void Update()
    {
        if (receiveAttackDebounceTimer > 0f)
            receiveAttackDebounceTimer -= Time.deltaTime;
    }

    public bool ReceiveAttack(int offense, int poise, Vector3 knockbackRequest)
    {
        if (this.currentHealth <= 0) return false;

        // Do debounce logic
        if (receiveAttackDebounceTimer > 0f) return false;
        receiveAttackDebounceTimer = receiveAttackDebounce;

        // Get health depleated a bit
        bool acceptKnockback = poise > this.poise;
        currentHealth -= offense;
        if (currentHealth <= 0f)
        {
            acceptKnockback = true;     // Force a poise break for the enemy on their dying hit
            SendMessage("HealthDepleted", SendMessageOptions.RequireReceiver);
        }

        // If poise is large enough, then it will do a knockback
        if (acceptKnockback)
        {
            body.velocity = knockbackRequest;
        }

        return true;
    }
}
