﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisablePlayerMovement : MonoBehaviour
{
    public GameState gameState;
    public bool findThirdpersonCameraAndPreload = true;
    public GameObject[] enableMovementCamList;
    private Cinemachine.CinemachineBrain cinemachineBrain;

    void Start()
    {
        this.cinemachineBrain = FindObjectOfType<Cinemachine.CinemachineBrain>(true);
        if (findThirdpersonCameraAndPreload)
        {
            this.enableMovementCamList[this.enableMovementCamList.Length - 1] = FindObjectOfType<Cinemachine.CinemachineFreeLook>().gameObject;
        }
    }

    void Update()
    {
        // We don't have any callbacks so we'll have to just poll for any changes in the live cameras
        GameObject activeCam = cinemachineBrain.ActiveVirtualCamera.VirtualCameraGameObject;
        bool disablePlayerMovement = true;
        foreach (GameObject allowedCam in enableMovementCamList)
        {
            if (allowedCam == activeCam)
            {
                disablePlayerMovement = false;
                break;
            }
        }

        // Only change the state if the property of disabling/enabling has actually changed.
        if (disablePlayerMovement != gameState.IsPlayerMovementDisabled())
        {
            gameState.DisablePlayerMovement(this, disablePlayerMovement);
        }

        // Debug.LogWarning(gameState.IsPlayerMovementDisabled());
        if (Cursor.visible != gameState.IsPlayerMovementDisabled())
        {
            SetLockCursor(!gameState.IsPlayerMovementDisabled());
        }
    }

    private void SetLockCursor(bool flag)
    {
        Cursor.lockState = flag ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !flag;
    }
}
