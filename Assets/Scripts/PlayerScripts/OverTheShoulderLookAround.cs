﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverTheShoulderLookAround : MonoBehaviour
{
    public ThirdPersonMovementRigidbody thirdPersonMovementRigidbody;
    public Cinemachine.CinemachineVirtualCamera virtualCamera;
    public float sensitivityHor = 1;
    public float sensitivityVer = 1;

    private bool isEnabled = false;

    private Vector3 originalLocalPosition;

    public Vector2 lookUpDownBounds = new Vector2(-85f, 85f);
    private float upDownRotation;

    void Start()
    {
        originalLocalPosition = transform.localPosition;
    }

    void Update()
    {
        if (!isEnabled) return;

        // Look around
        float yEulerAxisDelta = Input.GetAxisRaw("Mouse X") * sensitivityHor;
        upDownRotation += Input.GetAxisRaw("Mouse Y") * sensitivityVer * -1 * Time.deltaTime * 100f;

        Vector3 eulerAngs = transform.eulerAngles;
        upDownRotation = Mathf.Clamp(upDownRotation, lookUpDownBounds.x, lookUpDownBounds.y);
        eulerAngs.x = upDownRotation;
        transform.eulerAngles = eulerAngs;

        // Set look direction of player to the lookDelta's y euler
        thirdPersonMovementRigidbody.SetFacingTargetAngle(transform.eulerAngles.y + yEulerAxisDelta * Time.deltaTime * 100f, true);
    }

    public void SetEnableLookAround(bool flag, float yOff = 0f)
    {
        virtualCamera.gameObject.SetActive(flag);
        thirdPersonMovementRigidbody.SetDisableTurning(flag);

        // If enable, set the camera facing direction to the direction the camera is already facing....
        if (flag && !isEnabled)
        {
            // Reset camera view props
            upDownRotation = 0f;
            transform.localPosition = originalLocalPosition + new Vector3(0f, yOff, 0f);
            transform.localEulerAngles = new Vector3();

            thirdPersonMovementRigidbody.SetFacingTargetAngle(
                thirdPersonMovementRigidbody.cam.eulerAngles.y,
                true
            );
        }

        // Set after so that (flag && !isEnabled) check can detect if is transitioning to camera
        isEnabled = flag;
    }
}
