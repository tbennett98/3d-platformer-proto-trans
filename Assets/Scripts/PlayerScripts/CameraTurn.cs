﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTurn : MonoBehaviour
{
    public GameState gameState;

    [Header("Cam turning timing props")]
    public bool camTurnEnabled = true;
    public bool disableBackTurn = true;
    public float autoEnableBackTurnTime = 0.5f;
    private float autoEnableBackTurnTimer;
    public float manualOverrideDebounce = 0.5f;
    private float manualOverrideDebounceTimer;

    [Header("Cam turning amount props")]
    public float chaseTurnSpeed;
    private float chaseTurnVelo;

    private Cinemachine.CinemachineFreeLook orbit;
    private Transform cam;
    private Vector3 _prevPosition;

    void Start()
    {
        orbit = GetComponent<ThirdPersonMovementRigidbody>().orbit;
        cam = GetComponent<ThirdPersonMovementRigidbody>().cam;
        _prevPosition = transform.position;
        autoEnableBackTurnTimer = autoEnableBackTurnTime;
    }

    void Update()
    {
        CheckIfCanDoTurn();
        if (camTurnEnabled)
        {
            ProcessCamTurning();
        }

        _prevPosition = transform.position;
    }

    private void CheckIfCanDoTurn()
    {
        if ((Input.GetAxisRaw("Mouse X") != 0f || Input.GetAxisRaw("Mouse Y") != 0f) && !gameState.IsPlayerMovementDisabled())
        {
            camTurnEnabled = false;
            manualOverrideDebounceTimer = manualOverrideDebounce;
        }

        manualOverrideDebounceTimer -= Time.unscaledDeltaTime;

        if (manualOverrideDebounceTimer < 0f)
        {
            camTurnEnabled = true;
        }
    }

    private void ProcessCamTurning()
    {
        Vector3 deltaPosition = transform.position - _prevPosition;

        // Check if moving towards camera via dot product
        Vector3 flatDeltaPos = deltaPosition;   flatDeltaPos.y = 0f;
        Vector3 flatCamForward = cam.forward;   flatCamForward.y = 0f;

        disableBackTurn = autoEnableBackTurnTimer > 0f;
        float moveDot = Vector3.Dot(flatDeltaPos.normalized, flatCamForward.normalized);
        if (moveDot < 0f && disableBackTurn)
        {
            // Vectors are facing towards each other. Mirror!
            deltaPosition = Quaternion.AngleAxis(180f, cam.right) * deltaPosition;
        }

        // Make the move backwards filter a little more strict (+-45 degrees)
        if (moveDot < -0.65f)
        {
            autoEnableBackTurnTimer -= Time.deltaTime;
        }
        else
        {
            // Reset timer
            autoEnableBackTurnTimer = autoEnableBackTurnTime;
        }

        // Calculate target angle
        float targetAngle = orbit.m_XAxis.Value;
        if (flatDeltaPos.magnitude > 0.01f)
        {
            targetAngle = Mathf.Atan2(deltaPosition.x, deltaPosition.z) * Mathf.Rad2Deg;
        }

        // Apply delta angle
        // float newFacingAngle = Mathf.SmoothDampAngle(
        //     orbit.m_XAxis.Value,
        //     targetAngle,
        //     ref chaseTurnVelo,
        //     chaseTurnSpeed,
        //     float.MaxValue,
        //     Time.deltaTime
        // );
        float newFacingAngle = Mathf.MoveTowardsAngle(
            orbit.m_XAxis.Value,
            targetAngle,
            chaseTurnSpeed *
                Mathf.Abs(Mathf.DeltaAngle(orbit.m_XAxis.Value, targetAngle)) *
                Time.deltaTime
        );
        orbit.m_XAxis.Value = newFacingAngle;
    }
}
