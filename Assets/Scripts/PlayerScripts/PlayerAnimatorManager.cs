﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorManager : MonoBehaviour
{
    public TransformationManager transformationManager;
    public Animator[] animators;

    private float jumpFallBlend;
    public float blendToFallingSpeed = 3;
    private float wallClimbBlend;
    public float blendToFallingWall = 3;

    public void SetIsHumanHeli(bool flag)
    {
        animators[CurrentForm()].SetBool("IsHumanHeli", flag);
        jumpFallBlend = 1;      // To be falling when exiting human heli
    }

    public void DoMonsterDiveBash()
    {
        animators[CurrentForm()].SetTrigger("DoDiveBash");
        DoMonsterDiveBounce(false);
    }

    public void DoMonsterKnockback(bool flag)
    {
        if (flag && animators[CurrentForm()].GetBool("DoKnockback") != flag)
            animators[CurrentForm()].SetTrigger("DoKnockbackTrigger");

        animators[CurrentForm()].SetBool("DoKnockback", flag);
    }

    public void DoMonsterDiveBounce(bool flag)
    {
        animators[CurrentForm()].SetBool("DoDiveBounce", flag);
    }

    public void SetDoSlimeBall(bool flag)
    {
        animators[CurrentForm()].SetBool("DoSlimeBall", flag);
    }

    public void DoLavaRocketJump()
    {
        animators[CurrentForm()].SetTrigger("DoRocketJump");
    }

    public void SetIsGrounded(bool flag)
    {
        animators[CurrentForm()].SetBool("IsGrounded", flag);
    }

    public void DoLedgeGrab(bool flag)
    {
        if (flag && animators[CurrentForm()].GetBool("DoLedgeGrab") != flag)
            animators[CurrentForm()].SetTrigger("DoLedgeGrabTrigger");

        animators[CurrentForm()].SetBool("DoLedgeGrab", flag);
    }

    public void DoWallClimb(bool flag)
    {
        if (flag && animators[CurrentForm()].GetBool("DoWallClimb") != flag)
            animators[CurrentForm()].SetTrigger("DoWallClimbTrigger");

        animators[CurrentForm()].SetBool("DoWallClimb", flag);
        UpdateWallClimbBlend(true);
    }

    public void DoJump()
    {
        animators[CurrentForm()].SetTrigger("DoJumpTrigger");
    }
    public void DoAttack()
    {
        animators[CurrentForm()].SetTrigger("DoAttack");
    }
    public void DoAttackJump(bool flag)
    {
        if (flag && animators[CurrentForm()].GetBool("DoAttackJump") != flag)
            animators[CurrentForm()].SetTrigger("DoAttackJumpTrigger");

        animators[CurrentForm()].SetBool("DoAttackJump", flag);
    }

    public void DoSpecialAttack(bool flag)
    {
        if (flag && animators[CurrentForm()].GetBool("DoSpecialAttack") != flag)
            animators[CurrentForm()].SetTrigger("DoSpecialAttackTrigger");

        animators[CurrentForm()].SetBool("DoSpecialAttack", flag);
    }

    public void DoSwimming(bool flag)
    {
        if (flag && animators[CurrentForm()].GetBool("DoSwimming") != flag)
            animators[CurrentForm()].SetTrigger("DoSwimmingTrigger");

        animators[CurrentForm()].SetBool("DoSwimming", flag);
    }

    public void UpdateWallClimbBlend(bool goingUp)
    {
        wallClimbBlend = Mathf.Clamp(wallClimbBlend + Time.deltaTime * blendToFallingWall, 0, 1);
        if (goingUp) wallClimbBlend = 0;
        animators[CurrentForm()].SetFloat("Wall-Climb Blend", wallClimbBlend);
    }

    public void UpdateIdleRunBlend(float magnitude)
    {
        animators[CurrentForm()].SetBool("IsMoving", magnitude > 0.1f);
        animators[CurrentForm()].SetFloat("Idle-Run Blend", magnitude);
    }

    public void UpdateJumpFallBlend(float yVelo)
    {
        jumpFallBlend = Mathf.Clamp(jumpFallBlend + Time.deltaTime * blendToFallingSpeed, 0, 1);

        if (yVelo > 0)
        {
            jumpFallBlend = 0;
        }

        // NOTE: do not set if grounded here
        animators[CurrentForm()].SetFloat("Jump-Fall Blend", jumpFallBlend);
    }

    public void IsMidairHeliJump(bool flag)
    {
        if (flag && animators[CurrentForm()].GetBool("IsMidairHeliJump") != flag)
            animators[CurrentForm()].SetTrigger("DoMidairHeliJump");

        animators[CurrentForm()].SetBool("IsMidairHeliJump", flag);
    }

    public bool GetIsMidairHeliJump()
    {
        return animators[CurrentForm()].GetBool("IsMidairHeliJump");
    }

    public void DoHumanFrontFlip()
    {
        animators[CurrentForm()].SetTrigger("DoFrontFlip");
    }

    private int CurrentForm()
    {
        return Mathf.Clamp(
            transformationManager.CurrentForm(),
            0,
            animators.Length - 1
        );
    }
}
