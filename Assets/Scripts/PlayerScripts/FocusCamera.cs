﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusCamera : MonoBehaviour
{
    public GameState gameState;
    private Cinemachine.CinemachineFreeLook orbit;
    public Transform model;
    public Vector2 recenterCamVelocity = new Vector2(40f, 0.1f);
    // public float recenterCamTime = 0.05f;

    private bool prevCamTargeting;
    private float savedCamTargetAngle;

    private bool forceFocus;

    [Header("Focus on a target")]
    public float camXAxisOffsetValue = -90f;
    public float camYAxisValue = 0.35f;
    private Transform focusTarget;
    private bool focusOnTarget;
    private bool needToReenableFocus = false;

    private bool camTargeting = false;
    private bool camTargetingLock = false;

    void Awake()
    {
        orbit = FindObjectOfType<Cinemachine.CinemachineFreeLook>(true);
    }

    void Start()
    {
        forceFocus = false;
    }

    void Update()
    {
        // Recenter camera (focus camera or "Z Targeting")
        if (!camTargetingLock)
        {
            camTargeting = Input.GetAxis("Target Camera") > 0.5f && !gameState.IsPlayerMovementDisabled();
            if (camTargeting)
            {
                camTargetingLock = true;
                StartCoroutine(UndoLockAfter(0.25f));
            }
        }

        if (camTargeting && !prevCamTargeting && !forceFocus)
        {
            // See if should target a target (usu. enemy) instead
            focusTarget = TransformationManager.GetNearestObjPosition(transform.position, 15f, LayerMask.GetMask("Enemy Models"));
            if (focusTarget != null)
            {
                // tpmr.orbit.LookAt = focusTarget;     // NOTE: This didn't seem right
                focusOnTarget = true;
            }
        }
        else if (!camTargeting || needToReenableFocus)
        {
            // tpmr.orbit.LookAt = transform;     // NOTE: This didn't seem right
            focusTarget = null;
            focusOnTarget = false;
        }

        // Block focusing from happening if needToReenableFocus
        if (needToReenableFocus)
        {
            if (camTargeting)
                camTargeting = false;
            else
                needToReenableFocus = false;    // Turns off when not camTargeting in the input
        }


        //
        // Do actual camera focus logic
        //
        // Focus on a target
        //
        if (focusOnTarget && focusTarget != null)
        {
            Vector3 recenterDirection = focusTarget.position - transform.position;
            float recenterXValue = Mathf.Atan2(recenterDirection.x, recenterDirection.z) * Mathf.Rad2Deg + camXAxisOffsetValue;
            bool stillNeedLock = false;
            if (Mathf.Abs(orbit.m_XAxis.Value - recenterXValue) > 0.05f)
            {
                stillNeedLock = true;
                orbit.m_XAxis.Value = Mathf.MoveTowardsAngle(orbit.m_XAxis.Value, recenterXValue, recenterCamVelocity.x * Time.deltaTime);
            }
            if (Mathf.Abs(orbit.m_YAxis.Value - camYAxisValue) > 0.05f)
            {
                stillNeedLock = true;
                orbit.m_YAxis.Value = Mathf.MoveTowards(orbit.m_YAxis.Value, camYAxisValue, recenterCamVelocity.y * Time.deltaTime);
            }

            // HACK: Exit the focus logic and convert to straight-forward focus logic
            if (recenterDirection.sqrMagnitude > 289f)     // 17^2
            {
                needToReenableFocus = true;
            }

            // Undo the lock so user can stop focuscamera
            if (!stillNeedLock && !needToReenableFocus)
            {
                camTargetingLock = false;
            }
        }

        //
        // Focus forwards
        //
        else if (forceFocus || camTargeting)
        {
            if (!prevCamTargeting)
            {
                savedCamTargetAngle = model.rotation.eulerAngles.y;
            }

            float recenterTarget = savedCamTargetAngle /*- tpmr.cam.rotation.eulerAngles.y*/;       // Uncomment if using Simple Follow Camera for player
            bool stillNeedLock = false;
            if (Mathf.Abs(orbit.m_XAxis.Value - recenterTarget) > 0.05f)
            {
                stillNeedLock = true;
                orbit.m_XAxis.Value = Mathf.MoveTowardsAngle(orbit.m_XAxis.Value, recenterTarget, recenterCamVelocity.x * Time.deltaTime);
            }
            if (Mathf.Abs(orbit.m_YAxis.Value - camYAxisValue) > 0.05f)
            {
                stillNeedLock = true;
                orbit.m_YAxis.Value = Mathf.MoveTowards(orbit.m_YAxis.Value, camYAxisValue, recenterCamVelocity.y * Time.deltaTime);
            }

            // Undo the lock so user can stop focuscamera
            if (!stillNeedLock)
            {
                camTargetingLock = false;
            }
        }
        prevCamTargeting = camTargeting;
    }

    public void SetForceFocus(bool flag)
    {
        this.forceFocus = flag;
    }

    private IEnumerator UndoLockAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        camTargetingLock = false;
    }
}
