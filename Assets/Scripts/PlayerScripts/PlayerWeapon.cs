﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    public GameState gameState;
    public ThirdPersonMovementRigidbody tpmr;
    public PlayerAnimatorManager animatorManager;
    public BulletTimeManager bulletTimeManager;
    private Animator myAnimator;
    public FearBrain fearBrain;
    public Transformations executeOnTransformation;

    [Header("Contact Colliders Params")]
    public GameObject[] attackHitboxes;
    public MeleeHitboxReporter reporter;
    
    [Header("Inch Direction")]
    public Vector3 inchDir = new Vector3(0f, 0f, 20f);
    public CapsuleCollider capsule;
    private float colliderOrigRadius;
    public AnimationCurve colliderRadiusBalloonCurve;

    [Header("Feedback effects")]
    public float knockbackStrength = 10f;
    public ParticleSystem hitSuccParticles;
    public float pauseTime = 0f;
    public float bulletTime = 2f;

    [Header("Multiple Attacks")]
    public bool allowJumpAttack = false;
    private bool doingJumpAttack = false;

    private bool isAttacking = false;

    void Start()
    {
        myAnimator = GetComponent<Animator>();
    }

    void Update()
    {
        if (gameState.currentTransformation != (int)executeOnTransformation) return;

        // Find the input for attacking
        if (Input.GetButtonDown("Fire") && !gameState.IsPlayerMovementDisabled() && tpmr.onGround && !isAttacking)
        {
            Debug.Log("START Attack");
            isAttacking = true;
            tpmr.ChangeState(PlayerStates.MELEE);
            SaveStartColliderOffset();
            animatorManager.DoAttack();

            // Turn towards nearest enemy
            Transform nearestPos = TransformationManager.GetNearestObjPosition(transform.position, 15f, LayerMask.GetMask("Enemy Models"));
            if (nearestPos != null)
            {
                Vector3 newDirection = nearestPos.position - transform.position;
                tpmr.SetFacingTargetAngle(Mathf.Atan2(newDirection.x, newDirection.z) * Mathf.Rad2Deg, false);
            }
        }
        // TODO: These blocks of commented code show another attack. The attack system needs to be revamped later!!!
        // else if (allowJumpAttack && Input.GetButtonDown("Fire") && !gameState.IsPlayerMovementDisabled() && !tpmr.groundManager.onGround && !isAttacking)
        // {
        //     // Do the jump attack!!!
        //     isAttacking = true;
        //     doingJumpAttack = true;
        //     animatorManager.DoAttackJump(true);
        // }
    }

    void FixedUpdate()
    {
        // Use this to do a consistent check of colliding with the weapon
        if (isAttacking)
        {
            // Do an animated movement (of collider) depending on curve param
            float time = myAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            float offset = colliderRadiusBalloonCurve.Evaluate(time);
            capsule.radius = Mathf.Lerp(colliderOrigRadius, capsule.height / 2f, offset);

            // if (allowJumpAttack && doingJumpAttack && tpmr.groundManager.onGround)
            // {
            //     isAttacking = false;
            //     doingJumpAttack = false;
            //     animatorManager.DoAttackJump(false);
            // }
        }
    }

    public void DoInchVelocity()
    {
        tpmr.SetVelocity(Quaternion.Euler(0f, tpmr.GetFacingTargetAngle(), 0f) * inchDir);
    }

    public void TryAttack(int index)
    {
        reporter.CheckAndCallbackForContact(this, attackHitboxes[index], "OnReturnContact");
    }

    void OnReturnContact(Collider[] colliders)
    {
        foreach (Collider collider in colliders)
        {
            if (collider.isTrigger) continue;

            if (collider.transform.tag.Contains("ground"))
            {
                // Bonk!
                Debug.Log("BONK!");
                Vector3 knockbackVelo = Quaternion.Euler(0f, tpmr.GetFacingTargetAngle(), 0f) * new Vector3(0f, 0f, -25f);
                tpmr.SetVelocity(knockbackVelo);
                // weaponModelCreated.SetParticles(hitInfo.point);

                Instantiate(hitSuccParticles.gameObject, collider.ClosestPointOnBounds(transform.position), hitSuccParticles.transform.rotation);
            }
            else if (collider.transform.tag.Contains("enemy"))
            {
                // Perform knockback
                Vector3 knockbackRequest = collider.transform.position - transform.position;
                knockbackRequest.y = 0f;
                knockbackRequest.Normalize();
                knockbackRequest *= knockbackStrength;
                // TODO: Put in where poise and offense are calculated
                bool success = collider.GetComponent<EnemyReaction>().ReceiveAttack(1, 1, knockbackRequest);      // I can imagine the params could be (offense, poise, knockback)
                // if (success) weaponModelCreated.SetParticles(hitInfo.point);
                if (success)
                {
                    Instantiate(hitSuccParticles.gameObject, collider.ClosestPointOnBounds(transform.position), hitSuccParticles.transform.rotation);
                    bulletTimeManager.DoSlowmotion(false, bulletTime, pauseTime);
                    fearBrain.AttackSuccess(1);
                }
            }
        }
    }

    public void EndAttack()
    {
        isAttacking = false;
        tpmr.ChangeStateToGrounded();
        capsule.radius = colliderOrigRadius;
        Debug.Log("Player ended attack!");
    }

    private void SaveStartColliderOffset()
    {
        colliderOrigRadius = capsule.radius;
    }
}
