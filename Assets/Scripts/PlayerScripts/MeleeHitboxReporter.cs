﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeHitboxReporter : MonoBehaviour
{
    private MonoBehaviour invoker;
    private GameObject colliderGOToEnable;
    private string returnFunction;
    private bool requestStart, isWaitingOneFrame;

    // Callback var
    private List<Collider> discoveredColliders;

    void FixedUpdate()
    {
        // This will align with Collision events
        if (requestStart)
        {
            requestStart = false;
            colliderGOToEnable.SetActive(true);
            discoveredColliders = new List<Collider>();

            isWaitingOneFrame = true;
        }
        else if (isWaitingOneFrame)
        {
            // Shut down the check and do a callback
            isWaitingOneFrame = false;
            colliderGOToEnable.SetActive(false);
            invoker.SendMessage(returnFunction, discoveredColliders.ToArray());
        }
    }

    void OnTriggerStay(Collider other)
    {
        discoveredColliders.Add(other);
    }

    public void CheckAndCallbackForContact(MonoBehaviour invoker, GameObject colliderGOToEnable, string returnFunction)
    {
        requestStart = true;
        this.invoker = invoker;
        this.colliderGOToEnable = colliderGOToEnable;
        this.returnFunction = returnFunction;
    }
}
