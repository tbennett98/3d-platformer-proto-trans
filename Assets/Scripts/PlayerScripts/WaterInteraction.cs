﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterInteraction : MonoBehaviour
{
    private const int WATER_LAYER = 4;

    public GameState gameState;
    public PlayerAnimatorManager animatorManager;
    public Transform models;
    private ThirdPersonMovementRigidbody tpmr;
    private LedgeGrab ledgeGrab;
    private Rigidbody body;

    [Header("Water Movement params")]
    public Vector2 floatUpVeloMinMax = new Vector2(5f, 10f);
    private float floatUpCurrentSpeed;
    public float floatUpVeloDampen = 1f;
    public float maxSpeed = 5f;
    public float acceleration = 1f;
    private Vector3 velocity;

    [Header("Water Turning")]
    public float turnSmoothTime = 0.05f;
    private float turnSmoothModelVelo;
    private float facingTargetAngle;

    [Header("Resurfacing Params")]
    public CapsuleCollider myCollider;
    public float headHeight = 0.1f;

    private bool canEnterWater = true;
    private bool isSurfaced = false;
    private bool letFallMore = false;   // For first time submerging
    private LayerMask waterLayer;

    void Start()
    {
        tpmr = GetComponent<ThirdPersonMovementRigidbody>();
        ledgeGrab = GetComponent<LedgeGrab>();
        body = GetComponent<Rigidbody>();
        floatUpCurrentSpeed = floatUpVeloMinMax.x;
        waterLayer = LayerMask.GetMask("Water");
        canEnterWater = true;
    }

    void Update()
    {
        if (tpmr.IsState(PlayerStates.IN_WATER))
        {
            if (isSurfaced)
            {
                floatUpCurrentSpeed = 0f;

                if (Input.GetButton("Jump") && !gameState.IsPlayerMovementDisabled())
                {
                    // Jump out!
                    LeaveWater(velocity);
                    tpmr.ChangeState(PlayerStates.JUMP);
                    StartCoroutine(WaitToEnterWater(0.2f));
                }
            }
            else
            {
                if (!letFallMore)
                {
                    floatUpCurrentSpeed =
                        Mathf.MoveTowards(
                            floatUpCurrentSpeed,
                            floatUpVeloMinMax.x,
                            floatUpCurrentSpeed < 0f ? floatUpVeloDampen * 15f * Time.deltaTime : floatUpVeloDampen * Time.deltaTime
                        );
                }

                if (Input.GetButtonDown("Jump") && !gameState.IsPlayerMovementDisabled())
                {
                    // Swim up faster!
                    floatUpCurrentSpeed = floatUpVeloMinMax.y;
                }
            }

            // Turn Model
            float angle = Mathf.SmoothDampAngle(
                models.eulerAngles.y,
                facingTargetAngle,
                ref turnSmoothModelVelo,
                turnSmoothTime
            );
            models.eulerAngles = new Vector3(0f, angle, 0f);
        }
    }

    void FixedUpdate()
    {
        if (tpmr.IsState(PlayerStates.IN_WATER))
        {
            Vector3 targetDirection =
                Quaternion.Euler(0f, tpmr.cam.eulerAngles.y, 0f)
                * Vector3.ClampMagnitude(new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical")), 1f);
            
            if (gameState.IsPlayerMovementDisabled())
            {
                targetDirection = Vector3.zero;
            }

            Vector3 flatVelo = body.velocity;   flatVelo.y = 0f;
            velocity = Vector3.MoveTowards(flatVelo, targetDirection * maxSpeed, acceleration * 10f * Time.deltaTime);
            animatorManager.UpdateIdleRunBlend(velocity.magnitude / maxSpeed);

            if (velocity.magnitude > 0.1f)
            {
                facingTargetAngle = Mathf.Atan2(velocity.x, velocity.z) * Mathf.Rad2Deg;
            }


            // Check how far head is from water height
            RaycastHit hitInfo;
            bool hit = Physics.Raycast(
                myCollider.transform.position + myCollider.center + new Vector3(0f, myCollider.height / 2f, 0f),
                Vector3.down,
                out hitInfo,
                myCollider.height,
                waterLayer,
                QueryTriggerInteraction.Collide
            );

            if (hit)
            {
                // Check if larger than the headHeight
                if (hitInfo.distance >= headHeight)
                {
                    isSurfaced = true;
                }
            }
            else
            {
                isSurfaced = false;
            }

            if (body.velocity.y < 0f)
            {
                letFallMore = isSurfaced;   // Let fall if falling into the water until the algorithm deems "not surfaced" anymore
                isSurfaced = false;
            }
            else
            {
                letFallMore = false;
            }

            if (isSurfaced)
            {
                // Move to distance height
                float headHeightDiff = headHeight - hitInfo.distance;
                Vector3 newPos = body.position;
                newPos.y += headHeightDiff;
                body.MovePosition(newPos);
                tpmr.transformationManager.OnGroundEvent();
            }

            // Apply velocity to rigidbody!
            velocity.y = floatUpCurrentSpeed;
            body.velocity = velocity;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (canEnterWater && other.gameObject.layer == WATER_LAYER && !tpmr.IsState(PlayerStates.IN_WATER))
        {
            EnterWater();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == WATER_LAYER && tpmr.IsState(PlayerStates.IN_WATER))
        {
            LeaveWater(this.velocity);
        }
    }

    private IEnumerator WaitToEnterWater(float seconds)
    {
        canEnterWater = false;
        yield return new WaitForSeconds(seconds);
        canEnterWater = true;
    }

    private void EnterWater()
    {
        animatorManager.DoSwimming(true);
        tpmr.enabled = false;
        ledgeGrab.enabled = false;
        facingTargetAngle = tpmr.GetFacingTargetAngle();
        tpmr.ChangeState(PlayerStates.IN_WATER);
        isSurfaced = false;
        letFallMore = true;
        velocity = tpmr.GetVelocity();
        floatUpCurrentSpeed = velocity.y;
    }

    private void LeaveWater(Vector3 velocity)
    {
        animatorManager.DoSwimming(false);
        tpmr.enabled = true;
        tpmr.SetVelocity(velocity);
        tpmr.SetFacingTargetAngle(facingTargetAngle, true);
        ledgeGrab.enabled = true;
        tpmr.ChangeState(PlayerStates.FALLING);
    }
}
