﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHazardReaction : MonoBehaviour
{
    public GameObject hurtParticleSysGO;
    private ThirdPersonMovementRigidbody tpmr;
    private CapsuleCollider myCollider;
    public FearBrain fearBrain;
    public BulletTimeManager bulletTimeManager;

    [Header("Invincibility Frames")]
    public float invincibilityTime = 1f;
    private float invincibilityTimer;
    public float bulletTime = 0.75f;

    // Plugins
    public PlayerLife playerLife;

    void Start()
    {
        tpmr = GetComponent<ThirdPersonMovementRigidbody>();
        myCollider = GetComponent<CapsuleCollider>();
    }

    void Update()
    {
        if (invincibilityTimer > 0f)
        {
            invincibilityTimer -= Time.deltaTime;
        }
    }

    void ReceiveAttackHazard(Hazard hazard)
    {
        if (invincibilityTimer > 0f) return;
        invincibilityTimer = invincibilityTime;

        // TODO: Put in where poise and offense are calculated (subtract hp and whatnot here... check my poise vs theirs)
        Debug.Log("Hit on me!!!");
        tpmr.SetVelocity(hazard.CalculateKnockback(transform.position));
        tpmr.ChangeState(PlayerStates.RUNNING);     // TODO: In the future, I'd like a better "knockbacked" state where the player will lose control and fly back onto their butt
        Instantiate(hurtParticleSysGO, myCollider.ClosestPointOnBounds(hazard.transform.position), hurtParticleSysGO.transform.rotation);

        int leftOverAmt = fearBrain.ReceiveAttack(1);
        Debug.Log("Leftover attack amount: " + leftOverAmt);
        bulletTimeManager.DoSlowmotion(leftOverAmt > 0, bulletTime, 0f);

        // Apply health loss to life
        if (leftOverAmt > 0)
        {
            playerLife.RemoveLife(leftOverAmt);
        }
    }
}
