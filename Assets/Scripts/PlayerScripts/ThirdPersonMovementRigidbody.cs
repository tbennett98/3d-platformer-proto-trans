﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonMovementRigidbody : MonoBehaviour
{
    [Header("References")]
    public GameState gameState;
    public PlayerAnimatorManager animatorManager;
    public TransformationManager transformationManager;
    public Transform models;
    public Transform bottomCollider;
    public LayerMask raycastLayers;
    public Transform cam;
    public Cinemachine.CinemachineFreeLook orbit;
    private CapsuleCollider capsuleCollider;

    [Header("Player State")]
    [SerializeField] private PlayerStates playerState;

    [Header("Movement Params")]
    public float acceleration = 5f;
    public float midairAcceleration = 1f;
    public float maxSpeed = 15f;
    public float jumpHeight = 20f;
    [HideInInspector] public float transSlopeLimit = 45f;
    private Rigidbody body;
    private LedgeGrab ledgeGrabHandler;

    [Header("External Forces")]
    private Vector3 addedVelocityPlatforms, addedVelocityWallClimbPress, addedVelocityCorrectDownwardsWalk;

    [Header("Input Vars")]
    private bool inputJump, inputJumpRelease;
    private bool prevJumpInput;

    private Vector3 targetDirection;
    private Vector3 velocity;
    private Vector3 previousBodyVelocity;


    private bool disableWallJumpUntilGround = false;
    private float wallJumpDebounceTimer = 0f;
    [HideInInspector] public bool wallJumpUpwards = false;
    [HideInInspector] public bool wallClimbFallDisabled = false;
    private Vector3 wallJumpFacingDir;

    [Header("Wall Climb Params")]
    public float wallClimbSpeed = 15f;
    private float wallClimbCurrentSpeed;
    public float wallClimbTime = 0.25f;
    private float wallClimbTimer;
    public Vector2 wallJumpJumpVec = new Vector2(15, 20);
    public Vector2 wallJumpNormalAngles = new Vector2(80, 105);
    public float wallClimbGravityMin = 5f;
    public float wallClimbGravityMax = 20f;

    [Header("Ledge grab climb out")]
    public Vector2 ledgeClimboutVelo = new Vector2(4, 4);

    [Header("Character Turning")]
    public float turnSmoothTime = 0.05f;
    private float facingTargetAngle;
    private float turnSmoothModelVelo;

    private float fallOffJumpTimeLeeway = 0.1f;
    private float fallOffTimer;

    private bool tempDisableMovement;
    private bool disableCharTurning;
    private float idleRunBlend;

    private bool disableProcess;        // This disables all things in FixedUpdate()

    private Vector3 groundNormalVec = Vector3.up;
    private float groundAngle = 90;


    private Vector3 sentVelocity;       // For collision snatch checking
    public bool onGround
    {
        get { return cachedOnGround; }
        private set { cachedOnGround = value; }
    }
    private bool cachedOnGround;


    void Start()
    {
        body = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        ledgeGrabHandler = GetComponent<LedgeGrab>();
        StartCoroutine(ResetVelocityOfRigibodyEveryFrame());
    }

    void FixedUpdate()
    {
        // if (onGround && !disableCharTurning)    disableCharTurning = false;      // TODO: see if you can reenable char turning when hitting the ground from a wall jump

        // Button inputs
        bool currJumpInput =                    Input.GetButton("Jump") && !tempDisableMovement && !gameState.IsPlayerMovementDisabled();
        inputJump =                             currJumpInput && !prevJumpInput;
        inputJumpRelease =                      !currJumpInput && prevJumpInput;
        prevJumpInput =                         Input.GetButton("Jump");

        addedVelocityPlatforms =                Vector3.zero;
        addedVelocityWallClimbPress =           Vector3.zero;
        addedVelocityCorrectDownwardsWalk =     Vector3.zero;
        velocity +=                             Physics.gravity * Time.deltaTime;
        if (velocity.y < -100f)                 velocity.y = -100f;

        if (onGround)
        {
            fallOffTimer = 0f;
            transformationManager.OnGroundEvent();
        }
        else
        {
            fallOffTimer += Time.deltaTime;
        }

        // Shutoff Valve for processing the update method
        if (disableProcess)
        {
            return;
        }

        CheckIfOnGround();

        // Get Input and pre-vars
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        if (gameState.IsPlayerMovementDisabled())
        {
            horizontal = vertical = 0f;
        }

        targetDirection =
            Quaternion.Euler(0f, cam.eulerAngles.y, 0f)
            * Vector3.ClampMagnitude(new Vector3(horizontal, 0f, vertical), 1f);

        if (targetDirection.magnitude < 0.1f)
        {
            targetDirection = Vector3.zero;
        }

        // Process the states
        ProcessTimers();
        ProcessStates();

        // Apply 
        ApplyVelocity();
        ApplyMovingPlatformVelocity();
    }

    public void ApplyVelocity()
    {
        sentVelocity =
            velocity
            + addedVelocityWallClimbPress
            + addedVelocityCorrectDownwardsWalk;

        body.velocity += sentVelocity;
    }

    private void ApplyMovingPlatformVelocity()
    {
        if (playerState == PlayerStates.LEDGE_GRAB)     // TODO: get out of ledge grab state when standing on ground eh (like if you stand on ground from a moving platform)
        {
            // Special raycast for ledgegrab states
            Ray midRay = new Ray(
                body.position + capsuleCollider.center + new Vector3(0f, capsuleCollider.height / 2f + ledgeGrabHandler.eyeLevelYOffset, 0f),
                Quaternion.Euler(0f, models.eulerAngles.y, 0f) * Vector3.forward
            );
            Debug.DrawRay(midRay.origin, midRay.direction, Color.blue);

            RaycastHit hitInfo;
            if (Physics.Raycast(midRay, out hitInfo, capsuleCollider.radius + 0.5f, raycastLayers) &&
                hitInfo.rigidbody != null)
            {
                Vector3 movingPlatformVelocity = hitInfo.rigidbody.GetPointVelocity(hitInfo.point);
                facingTargetAngle += hitInfo.rigidbody.angularVelocity.y;
                body.velocity += movingPlatformVelocity;
            }
        }
        else
        {
            // Get a rigidbody's velocity below and add it to my rigidbody's velocity
            RaycastHit hitInfo;
            if (cachedOnGround &&
                Physics.Raycast(bottomCollider.position + new Vector3(0f, 0.5f, 0f), Vector3.down, out hitInfo, 1f, raycastLayers) &&
                hitInfo.rigidbody != null)
            {
                Vector3 movingPlatformVelocity = hitInfo.rigidbody.GetPointVelocity(hitInfo.point);
                facingTargetAngle += hitInfo.rigidbody.angularVelocity.y;
                body.velocity += movingPlatformVelocity;
                // groundManager.SetFudgingVelocity(movingPlatformVelocity);
            }
        }
    }

    // private IEnumerator DebugThePhysc()
    // {
    //     while (true)
    //     {
    //         yield return new WaitForFixedUpdate();
    //         if (Input.GetKey(KeyCode.S)) { Debug.Break();}
    //         Debug.Log($"({sentVelocity.x},\t{sentVelocity.y},\t{sentVelocity.z})\t({Time.frameCount}\n({body.velocity.x},\t{body.velocity.y},\t{body.velocity.z})");
    //     }
    // }

    private void ProcessTimers()
    {
        if (wallJumpDebounceTimer > 0f)
        {
            wallJumpDebounceTimer -= Time.deltaTime;
        }
    }

    private void ProcessStates()
    {
        // Player States
        switch (playerState)
        {
            case PlayerStates.IDLE:
            {
                velocity.x = velocity.z = 0f;
                ProcessGroundedState();
            }
            break;

            case PlayerStates.RUNNING:
            {
                ProcessGroundedState();
            }
            break;

            case PlayerStates.JUMP:
            {
                DoJump();
                ProcessMidairState();
                ChangeState(PlayerStates.FALLING);
            }
            break;

            case PlayerStates.CHARGE_DIVE:
            case PlayerStates.HELI_SPIN:
            case PlayerStates.FALLING:
            {
                ProcessMidairState();
            }
            break;

            case PlayerStates.MELEE:
            {
                targetDirection = Vector3.zero;
                GotoVelocityViaAccel(acceleration, velocity);
            }
            break;

            case PlayerStates.KNOCKBACKED:
            {

            }
            break;

            case PlayerStates.WALL_CLIMB:
            {
                ProcessWallJumpState();
            }
            break;

            case PlayerStates.LEDGE_GRAB:
            {
                velocity = Vector3.zero;

                // Check if jumped out
                if (inputJump)
                {
                    // TODO: make it so that the falling state also inherits velocity from a moving platform! PLEASE GIVE ME THE VELOCITY INHERITENCE!!!!!!!!!!
                    ChangeState(PlayerStates.FALLING);      // So that there's no two jumps

                    // Do climb out
                    velocity = Quaternion.Euler(0f, facingTargetAngle, 0f) * Vector3.forward * ledgeClimboutVelo.x;
                    velocity.y = ledgeClimboutVelo.y;
                    ledgeGrabHandler.ReportClimbedOut();

                    // Animator
                    animatorManager.DoLedgeGrab(false);
                    animatorManager.DoJump();
                }
            }
            break;
        }
    }

    private void ProcessGroundedState()
    {
        // Turn player's direction with targetDirection Input
        if (targetDirection.magnitude > 0.1f && !disableCharTurning)
            SetFacingTargetAngle(Mathf.Atan2(targetDirection.x, targetDirection.z) * Mathf.Rad2Deg, false);

        GotoVelocityViaAccel(acceleration, velocity);


        // To switch between idle and running
        ChangeStateToGrounded();

        // Post-velocity change: check for stairs
        CheckForStairs();

        // Do falling if finds myself to be midair
        if (!onGround)
        {
            ChangeState(PlayerStates.FALLING);
        }

        // Jump calc
        if (inputJump)
        {
            bool stillValidJump = fallOffTimer <= fallOffJumpTimeLeeway;
            if ((onGround && groundAngle < transSlopeLimit + 1f) || stillValidJump)
            {
                if (!stillValidJump && groundAngle >= transSlopeLimit + 1f)
                {
                    // DoWallJump(groundNormalVec);
                    // animatorManager.DoJump();
                }
                else
                {
                    // DoJump();
                    ChangeState(PlayerStates.JUMP);
                }
            }
        }
    }

    private void ProcessMidairState()
    {
        if (IsState(PlayerStates.CHARGE_DIVE))
        {
            // Turn player based off velocity
            Vector3 flatVelo = velocity;
            flatVelo.y = 0f;
            SetFacingTargetAngle(Mathf.Atan2(flatVelo.x, flatVelo.z) * Mathf.Rad2Deg, false);
        }
        else
        {
            // Turn player's direction with targetDirection Input
            if (targetDirection.magnitude > 0.1f && !disableCharTurning)
                SetFacingTargetAngle(Mathf.Atan2(targetDirection.x, targetDirection.z) * Mathf.Rad2Deg, false);
        }

        GotoVelocityViaAccel(midairAcceleration, /*body.*/velocity); // TODO: see how can get external forces to appear in the gameplay (rn: it's making it so when you jump the first frame's platform velocity still gets applied. Then you move twice the speed of the platform)

        // See if should change to grounded state
        if (onGround && velocity.y <= 0f)
        {
            // Change to a grounded state
            ChangeStateToGrounded();
        }
        else if (IsState(PlayerStates.FALLING) && inputJump)    // NOTE: so that when jumping out of water, midairEvent doesn't get triggered too
        {
            transformationManager.DoMidairEvent(ref velocity);
        }    
    }

    private void GotoVelocityViaAccel(float accel, Vector3 inputVelocity)
    {
        // Move towards acceleration speed
        Vector3 flatVelocity = inputVelocity;    flatVelocity.y = 0;
        flatVelocity = Vector3.MoveTowards(
            flatVelocity,
            targetDirection * maxSpeed,
            accel * 10f * Time.deltaTime
        );

        velocity.x = flatVelocity.x;
        velocity.z = flatVelocity.z;


        // Change direction for climbing slopes
        Vector3 slopeAdjusted = flatVelocity;
        if (groundAngle < transSlopeLimit + 1f && velocity.y <= 0)
        {
            slopeAdjusted = Quaternion.FromToRotation(Vector3.up, groundNormalVec) * flatVelocity;
        }

        // Apply the sloped movement
        addedVelocityCorrectDownwardsWalk = slopeAdjusted - flatVelocity;
        Debug.DrawRay(transform.position, addedVelocityCorrectDownwardsWalk, Color.red);
        Debug.DrawRay(transform.position, slopeAdjusted, Color.green);


        // For the animation data
        Vector3 aniVelo = velocity;     aniVelo.y = 0f;
        idleRunBlend = aniVelo.magnitude / maxSpeed;
    }

    private void ProcessWallJumpState()
    {
        if (Physics.Raycast(bottomCollider.position + new Vector3(0f, 0.1f, 0f), Vector3.down, 0.25f + 0.1f, raycastLayers))
        {
            // Exit wall Jump!
            ChangeStateToGrounded();
        }
        else
        {
            wallClimbTimer += Time.deltaTime;

            // Press Jump to jump away!
            if (inputJump && !wallClimbFallDisabled)    // NOTE: wallClimbFallDisabled refers to no wall jump
            {
                DoWallJump(-wallJumpFacingDir, true);
                ChangeState(PlayerStates.FALLING);
            }
            else
            {
                // Find normal to climb up wall
                RaycastHit hitData;
                bool passed = Physics.Raycast(capsuleCollider.transform.position + capsuleCollider.center, wallJumpFacingDir, out hitData, 3.5f, raycastLayers);

                if (!passed)
                {
                    // Redo and see if it's barely hanging on (only check top point)
                    passed = Physics.Raycast(
                        capsuleCollider.transform.position + capsuleCollider.center + new Vector3(0f, capsuleCollider.height / 2f, 0f),
                        wallJumpFacingDir,
                        out hitData,
                        capsuleCollider.radius + 0.5f
                    );
                }

                if (passed)
                {
                    // Start processing wall climb/slide
                    float wallAngle = Vector3.Angle(Vector3.up, hitData.normal);
                    if (wallAngle >= wallJumpNormalAngles.x &&
                        wallAngle <= wallJumpNormalAngles.y)
                    {
                        // Reset wallJumpFacingDir
                        wallJumpFacingDir = -new Vector3(hitData.normal.x, 0f, hitData.normal.z).normalized;
                        SetFacingTargetAngle(Mathf.Atan2(wallJumpFacingDir.x, wallJumpFacingDir.z) * Mathf.Rad2Deg, true);

                        // Find climb speed
                        if (wallClimbTimer < wallClimbTime)
                        {
                            // Climb up wall
                            wallClimbCurrentSpeed = wallClimbSpeed;
                            animatorManager.UpdateWallClimbBlend(true);
                        }
                        else
                        {
                            wallClimbCurrentSpeed = Mathf.Min(wallClimbCurrentSpeed, wallClimbSpeed);

                            if (wallClimbFallDisabled && wallClimbCurrentSpeed < 0f)
                            {
                                disableWallJumpUntilGround = true;
                                ChangeState(PlayerStates.FALLING);
                            }
                            else
                            {
                                // Check to see if want fast or slow fall
                                float fallGravity = wallClimbGravityMin;

                                Vector3 flatNormal = hitData.normal;    flatNormal.y = 0;
                                float inputAngle = Vector3.Angle(targetDirection, hitData.normal);
                                if (targetDirection.magnitude > 0f && inputAngle < 45f)
                                {
                                    fallGravity = wallClimbGravityMax;
                                }

                                // Fall down wall
                                wallClimbCurrentSpeed += Physics.gravity.y * Time.deltaTime;
                                wallClimbCurrentSpeed = Mathf.Max(wallClimbCurrentSpeed, -fallGravity);
                                animatorManager.UpdateWallClimbBlend(false);
                            }
                        }

                        // Find vector to climb up
                        Vector3 climbUp = Quaternion.FromToRotation(Vector3.up, hitData.normal) * (wallJumpFacingDir * wallClimbCurrentSpeed);
                        velocity = climbUp;

                        // FIXME: This wall-pressing solution worked... until we had the oncollisionstay() give inpulse return velocity back to this. This would force an impulse with some lag to shoot the player large distances. So figure out how to have the player pushed up against the wall but not so pushy yknow.
                        // const float collisionSkin = 0.4f;
                        // addedVelocityWallClimbPress = (collisionSkin - hitData.distance) * hitData.normal / Time.deltaTime;    // hitData.distance may be a little too big if the raycast angle was not hitData.normal*-1
                        // Debug.LogWarning(addedVelocityWallClimbPress);
                    }
                    else
                    {
                        ChangeState(PlayerStates.FALLING);
                    }
                }
                else
                {
                    ChangeState(PlayerStates.FALLING);
                }
            }
        }
    }

    void Update()
    {
        // Process target facing angle
        float angle = Mathf.SmoothDampAngle(
            models.eulerAngles.y,
            facingTargetAngle,
            ref turnSmoothModelVelo,
            turnSmoothTime
        );
        models.eulerAngles = new Vector3(0f, angle, 0f);            // TODO: this line for some reason is getting buggy, causing the facing rotation of the monster while shooting arrows to go wild


        // Update Anim JumpFall Blend
        bool animOnGround = onGround;
        if (!animOnGround)
        {
            RaycastHit hitin;
            float padding = 0.05f;
            animOnGround = Physics.Raycast(bottomCollider.position + Vector3.up * padding, Vector3.down, out hitin, 0.25f + padding * 2f, raycastLayers);
            Debug.DrawLine(bottomCollider.position, bottomCollider.position + Vector3.down * 0.25f, Color.yellow);
            // if (!animOnGround) Debug.Break();
        }
        animatorManager.SetIsGrounded(animOnGround);
        animatorManager.UpdateIdleRunBlend(idleRunBlend);
        animatorManager.UpdateJumpFallBlend(velocity.y);
    }

    public void ChangeState(PlayerStates newState)
    {
        playerState = newState;

        // HACK: To unset the wall climb state in the animator
        if (newState != PlayerStates.WALL_CLIMB)
        {
            animatorManager.DoWallClimb(false);
        }
    }

    public bool IsState(PlayerStates state)
    {
        return playerState == state;
    }

    public void DoWallJump(Vector3 facingDirection, bool doNewFacingDir = false)
    {
        velocity = new Vector3(facingDirection.x, 0f, facingDirection.z).normalized * wallJumpJumpVec.x;
        velocity.y = wallJumpJumpVec.y;
        wallJumpDebounceTimer = 0.2f;   // Set a debounce timer!

        if (doNewFacingDir)
        {
            if (wallJumpUpwards)
            {
                // Special slime way, you jump straight up and you can ledge grab but no grab again!
                disableWallJumpUntilGround = true;
            }
            else
            {
                SetFacingTargetAngle(
                    Mathf.Atan2(facingDirection.x, facingDirection.z) * Mathf.Rad2Deg,
                    true
                );
            }

            StartCoroutine(TempDisableCharTurning(0.25f));      // You can't grab onto the same wall again!
        }
    }

    private void DoJump()
    {
        velocity = previousBodyVelocity;
        velocity.y = jumpHeight + Mathf.Max(velocity.y, 0f);
        animatorManager.DoJump();
    }

    //
    // Make stairs thing! And this can be pretty complicated too eh
    // NOTE: this kinda seems like it's gonna just turn into its own componenet I feel like.
    //
    private void CheckForStairs()
    {
        const float stepHeight = 0.35f;
        const float raycastHeightPadding = 0.1f;

        //
        // Shoot raycast from where real capsule ends (plus a little padding)
        // and a little more from stepHeight to try and find the next step
        //
        Vector3 newCharacterPositionApprox = body.position + velocity * Time.deltaTime;
        Ray groundDetectionRay = new Ray(
            body.position + capsuleCollider.center - new Vector3(0f, capsuleCollider.height / 2f, 0f) + new Vector3(0f, raycastHeightPadding, 0f),
            Vector3.down
        );

        float distanceToBottom = transform.position.y - bottomCollider.position.y;
        RaycastHit hitInfo;
        bool hit =
            Physics.Raycast(groundDetectionRay, out hitInfo, raycastHeightPadding + distanceToBottom + stepHeight, raycastLayers)
            && hitInfo.collider.tag.Contains("ground");
        if (!hit)
        {
            return;
        }

        //
        // Move to the found step!
        //
        body.MovePosition(
            hitInfo.point + new Vector3(0f, 1f, 0f)     // TODO: HARDCODE
        );
    }

    private void CheckIfOnGround()
    {
        //
        // Set up ground normal props
        //
        groundNormalVec = Vector3.up;
        groundAngle = 90f;

        const float raycastHeightPadding = 0.1f;
        Ray groundDetectionRay = new Ray(
            body.position + capsuleCollider.center - new Vector3(0f, capsuleCollider.height / 2f, 0f) + new Vector3(0f, raycastHeightPadding, 0f),
            Vector3.down
        );
        float distanceToBottom = transform.position.y - bottomCollider.position.y;
        RaycastHit hit;
        if (Physics.Raycast(groundDetectionRay, out hit, distanceToBottom + raycastHeightPadding, raycastLayers)
            && hit.collider.tag.Contains("ground"))
        {
            groundNormalVec = hit.normal;
            groundAngle = Vector3.Angle(Vector3.up, hit.normal);
            onGround = true;
        }
        else
        {
            onGround = false;
        }

        // Debug.Log($"{onGround}\t{groundAngle}\t{velocity.y}");
        // Debug.DrawLine(groundDetectionRay.origin, groundDetectionRay.origin + groundDetectionRay.direction * (distanceToBottom + raycastHeightPadding), Color.magenta);
        if (onGround && groundAngle < transSlopeLimit + 1f && velocity.y <= 0f)
        {
            velocity.y = 0f;
            disableWallJumpUntilGround = false;
        }
    }

    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag.Contains("ground"))
        {
            // Check if wall jump??!?!?
            Vector3 normalizedImp = other.GetContact(0).normal.normalized;
            float facingAngleDiff = Mathf.Abs(Mathf.DeltaAngle(this.facingTargetAngle, Mathf.Atan2(-normalizedImp.x, -normalizedImp.z) * Mathf.Rad2Deg));
            float wallAngle = Vector3.Angle(normalizedImp, Vector3.up);

            // Could be a wall jump (kansei dorifuto)
            if (facingAngleDiff <= 45f &&
                wallAngle >= wallJumpNormalAngles.x &&
                    wallAngle <= wallJumpNormalAngles.y &&
                transformationManager.OnWallJumpEnterEvent(other))
            {
                // Trigger callback, which has ability to cancel a wall jump
                if (playerState == PlayerStates.FALLING)
                {
                    if (wallJumpDebounceTimer <= 0f && !disableWallJumpUntilGround)
                    {
                        if (velocity.y < 0f)    // To doshatto running up from standing... must be falling
                        {
                            Vector3 flatVelo = velocity;    flatVelo.y = 0f;
                            if (targetDirection.magnitude > 0.25f ||      // This needs to be checked last so that monster's knockback isn't short-circuited. -Timo
                                flatVelo.magnitude > maxSpeed / 2f)
                            {
                                wallJumpFacingDir = -new Vector3(normalizedImp.x, 0f, normalizedImp.z).normalized;
                                RaycastHit hit;
                                if (Physics.Raycast(capsuleCollider.transform.position + capsuleCollider.center, wallJumpFacingDir, out hit, 3.5f, raycastLayers) &&
                                    hit.transform.tag.Contains("ground"))
                                {
                                    // It's a wall jump enter!
                                    ChangeState(PlayerStates.WALL_CLIMB);
                                    animatorManager.DoWallClimb(true);
                                    SetVelocity(Vector3.zero);
                                    wallClimbTimer = 0f;
                                }
                            }
                        }
                    }
                }
            }

            // Only apply external correctional velocity if a wall or ceiling
            float groundAngle = Vector3.Angle(Vector3.up, other.GetContact(0).normal);
            bool isWall = groundAngle >= transSlopeLimit + 1f;
            if (isWall)
            {
                // velocity += other.impulse;      //FIXME: See what this could cause problems for.... like now climbing slopes is weird, and you can't really slide down steep slopes anymore.
                // FIXME: Maybe this would be better (just apply the x and z directino impulses)
                velocity += new Vector3(other.impulse.x, 0f, other.impulse.z);      // Well... this became the definitive edition!!!!! (NEWEST)
                // FIXME: I think this should be the definitive solution: to check if it's not a floor
                // velocity += other.impulse;
            }

            if (body.velocity != sentVelocity)
            {
                Debug.Log("Snatch detected! " + other.impulse + "\t" + other.GetContact(0).normal);

                // If snatched on a wall, shift it a tiny bit in the direction of the normal
                if (isWall)
                {
                    body.MovePosition(body.position + other.GetContact(0).normal * 0.01f);
                }
            }
        }
    }

    public IEnumerator TempDisableInputCoroutine(float seconds)
    {
        SetDisableInput(true);
        yield return new WaitForSecondsRealtime(seconds);
        SetDisableInput(false);
    }

    private IEnumerator TempDisableCharTurning(float seconds)
    {
        disableCharTurning = true;
        yield return new WaitForSecondsRealtime(seconds);
        disableCharTurning = false;
    }

    private IEnumerator ResetVelocityOfRigibodyEveryFrame()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            previousBodyVelocity = body.velocity;
            body.velocity = Vector3.zero;
        }
    }

    public void SetDisableInput(bool flag)
    {
        tempDisableMovement = flag;
    }

    public void SetFacingTargetAngle(float angle, bool turnImmediately)
    {
        this.facingTargetAngle = angle;
        if (turnImmediately)
        {
            models.eulerAngles = new Vector3(0f, angle, 0f);
        }
    }

    public float GetFacingTargetAngle()         { return facingTargetAngle; }
    public Vector3 GetCamRelInputDirection()    { return targetDirection; }
    public Vector3 GetVelocity()                { return velocity; }
    public Vector3 GetTargetDirection()         { return targetDirection; }
    public CapsuleCollider GetCollider()        { return capsuleCollider; }
    public void SetVelocity(Vector3 velo)       { velocity = velo; }
    public void SetDisableProcess(bool flag)    { disableProcess = flag; }
    public void SetDisableTurning(bool flag)    { disableCharTurning = flag; }
    public void ChangeStateToGrounded()
    {
        Vector3 flatVelo = velocity;
        flatVelo.y = 0f;
        ChangeState(flatVelo.sqrMagnitude > 0f ? PlayerStates.RUNNING : PlayerStates.IDLE);
    }
}
