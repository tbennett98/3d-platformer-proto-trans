﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedgeGrab : MonoBehaviour
{
    private CapsuleCollider controller;
    private ThirdPersonMovementRigidbody tpmr;
    private Rigidbody body;

    public float eyeLevelYOffset = 0f;
    public float searchMaxHeight = 1f;
    public float searchOvershoot = 0.001f;
    public float maxDistAwayFromWall;
    public float grabPointYOffset;

    private bool disableLedgeGrabbing;

    private MovementReporter attachedReporter = null;

    void Start()
    {
        controller = GetComponent<CapsuleCollider>();
        tpmr = GetComponent<ThirdPersonMovementRigidbody>();
        body = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (disableLedgeGrabbing) return;

        Vector3 velo = tpmr.GetVelocity();
        if ((tpmr.IsState(PlayerStates.FALLING) && velo.y < 0f) ||
            tpmr.IsState(PlayerStates.SCALING_WALL) ||
            tpmr.IsState(PlayerStates.WALL_CLIMB))
        {
            CheckLedgeGrab();
        }

        // For ledgegrabbing on moving platforms
        if (attachedReporter != null)
        {
            attachedReporter.AddObserver(gameObject);
        }
    }

    public bool CheckLedgeGrab()
    {
        if (tpmr.IsState(PlayerStates.LEDGE_GRAB)) return true;    // Already ledge grabbing

        // Shoot raycast to see if close enough
        Ray midRay = new Ray(
            body.position + controller.center + new Vector3(0f, controller.height / 2f + eyeLevelYOffset, 0f),
            Quaternion.Euler(0f, tpmr.models.eulerAngles.y, 0f) * Vector3.forward
        );
        Debug.DrawRay(midRay.origin, midRay.direction, Color.blue);

        RaycastHit hitInfo;
        bool hit = Physics.Raycast(midRay, out hitInfo, maxDistAwayFromWall + controller.radius, tpmr.raycastLayers);
        bool midBodyTestGood = false;
        bool usedFailsafeTest = false;

        if (hit && hitInfo.transform.tag.Contains("ground"))
        {
            midBodyTestGood = true;
        }

        if (!midBodyTestGood)
        {
            // Retry with center of controller
            midRay.origin = body.position + controller.center;
            hit = Physics.Raycast(midRay, out hitInfo, maxDistAwayFromWall + controller.radius, tpmr.raycastLayers);

            if (hit && hitInfo.transform.tag.Contains("ground"))
            {
                midBodyTestGood = true;
                usedFailsafeTest = true;
            }
        }

        if (midBodyTestGood)
        {
            // We're looking for a wall
            if (Mathf.Abs(hitInfo.normal.y) < 0.1f)
            {
                // Okay, so this is a wall, and we need to see if there is ground at the top
                Vector3 flatNormal = hitInfo.normal;    flatNormal.y = 0;   flatNormal.Normalize();
                Ray groundDownRay = new Ray(
                    hitInfo.point + new Vector3(-flatNormal.x * searchOvershoot, searchMaxHeight, -flatNormal.z * searchOvershoot),
                    Vector3.down
                );
                Debug.DrawRay(groundDownRay.origin, groundDownRay.direction, Color.magenta);
                
                RaycastHit hitInfo2;
                bool hit2 = Physics.Raycast(
                    groundDownRay,
                    out hitInfo2,
                    searchMaxHeight + 0.1f + (usedFailsafeTest ? controller.height / 2f + eyeLevelYOffset : 0f),
                    tpmr.raycastLayers
                );
                if (hit2 && hitInfo2.transform.tag.Contains("ground") && hitInfo2.normal.y >= 0.70710678118f)       // hitInfo2.normal.y >= 0.70710678118f is for checking to see if the normal is < 45 from up. The constant is sin(45 deg) and we assume the vector has a magnitude of 1
                {
                    // Detected a ledge!!! (possibly)
                    
                    //
                    // Double check that this ledge is not just a seam in the wall
                    // NOTE: This ledge grab checker is written for ledges that either have a flat surface or face towards the player (so keep that in mind please)
                    //
                    Ray checkIfCeilingExistsAbove = new Ray(
                        body.position + controller.center + new Vector3(0f, controller.height / 2f, 0f),
                        Vector3.up
                    );
                    float raycastDistance = (hitInfo2.point - checkIfCeilingExistsAbove.origin).y + searchOvershoot;
                    RaycastHit hitInfo3;
                    bool hit3 = Physics.Raycast(checkIfCeilingExistsAbove, out hitInfo3, raycastDistance, tpmr.raycastLayers);
                    bool failed = hit3 && hitInfo3.transform.tag.Contains("ground");        // There's a ceiling. Fail.

                    if (!failed)
                    {
                        Ray checkIfLedgeIsActuallyWall = new Ray(
                            new Vector3(hitInfo2.point.x + hitInfo.normal.x / 2f, hitInfo2.point.y + searchOvershoot + 0.1f, hitInfo2.point.z + hitInfo.normal.z / 2f),
                            new Vector3(-hitInfo.normal.x, 0f, -hitInfo.normal.z).normalized
                        );
                        RaycastHit hitInfo4;
                        bool hit4 = Physics.Raycast(checkIfLedgeIsActuallyWall, out hitInfo4, 1f, tpmr.raycastLayers);
                        bool failed2 = hit4 && hitInfo4.transform.tag.Contains("ground");       // The "ledge" actually is a wall
                        if (!failed2)
                        {
                            Debug.DrawLine(checkIfCeilingExistsAbove.origin, checkIfCeilingExistsAbove.origin + checkIfCeilingExistsAbove.direction * raycastDistance, Color.red, 1f);
                            Debug.DrawLine(checkIfLedgeIsActuallyWall.origin, checkIfLedgeIsActuallyWall.origin + checkIfLedgeIsActuallyWall.direction * 1f, Color.red, 1f);

                            // We can trust that this is indeed a true ledge. YAY!
                            Debug.Log("Ledge detected!");
                            Vector3 grabPoint = hitInfo2.point;
                            grabPoint += flatNormal * (controller.radius + searchOvershoot + 0.1f);
                            grabPoint.y += -controller.height / 2f + grabPointYOffset;

                            // Move to there!
                            body.velocity = Vector3.zero;
                            tpmr.SetVelocity(body.velocity);
                            body.MovePosition(grabPoint);
                            tpmr.SetFacingTargetAngle(Mathf.Atan2(-flatNormal.x, -flatNormal.z) * Mathf.Rad2Deg, false);
                            tpmr.ChangeState(PlayerStates.LEDGE_GRAB);

                            // Add to mvt observers if so
                            attachedReporter = hitInfo.transform.gameObject.GetComponent<MovementReporter>();

                            // Animator
                            tpmr.animatorManager.DoLedgeGrab(true);
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public void ReportClimbedOut()
    {
        // Get out of mvt observers if so
        if (attachedReporter != null)
        {
            attachedReporter = null;
        }

        // Ledgegrab debounce
        StartCoroutine(WaitBeforeNextLedgeGrab(0.25f));
    }

    private IEnumerator WaitBeforeNextLedgeGrab(float seconds)
    {
        disableLedgeGrabbing = true;
        yield return new WaitForSeconds(seconds);
        disableLedgeGrabbing = false;
    }
}
