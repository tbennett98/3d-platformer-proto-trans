﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FearMeterUI : MonoBehaviour
{
    public FearBrain fearBrain;

    [Header("UI Plugins/Animation")]
    public Image barFront;
    public Image maxFearOverlay;
    public float maxBarFrontWidth;
    public AnimationCurve overlayFlashingOpacity;
    private float animTime;
    private float normalOverlayAlpha;

    void Start()
    {
        normalOverlayAlpha = maxFearOverlay.color.a;
    }

    void Update()
    {
        barFront.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, fearBrain.currentAdrenaline / fearBrain.maxAdrenaline * maxBarFrontWidth);

        animTime += Time.deltaTime;

        Color uiColor = maxFearOverlay.color;
        uiColor.a = fearBrain.IsMaxAdrenaline() ? overlayFlashingOpacity.Evaluate(animTime) : normalOverlayAlpha;
        maxFearOverlay.color = uiColor;
    }
}
