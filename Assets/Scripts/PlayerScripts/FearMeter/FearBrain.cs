﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FearBrain : MonoBehaviour
{
    [Header("Attributes")]
    public float maxAdrenaline = 100f;
    public float stayInMaxAdrTime = 5f;
    private float stayInMaxTimer;
    public float currentAdrenaline = 0f;
    public float adrenalineBuffer = 25f;      // This is as a buffer for when you get hit past 0f
    public float hitPointMultiplier = 25f;

    [Header("Adrenaline Depletion")]
    public float depletionRate = 2f;
    public bool isInEnemyBounds = false;
    private float isInEnemyBoundsTimer;

    [Header("Beginning Adrenaline Rush")]
    public float timeOutsideEnemyBoundsReq = 30f;
    private float timeOutsideTimer;
    private bool willDoAdrenalineRush;

    [Header("Regenerating Life from max adrenaline")]
    public float timeToRegenerate = 4.5f;
    private float timeToRegenerateTimer;

    // Plugins
    private PlayerLife playerLife;

    void Start()
    {
        playerLife = GetComponent<PlayerLife>();
        willDoAdrenalineRush = true;
    }

    void Update()
    {
        if (willDoAdrenalineRush && isInEnemyBounds)
        {
            // Set the adrenaline to half the bar!
            currentAdrenaline = maxAdrenaline / 2f;
            willDoAdrenalineRush = false;
        }

        if (isInEnemyBounds)
        {
            // Reset timer if go back into enemy bounds
            timeOutsideTimer = 0f;
        }
        else if (!willDoAdrenalineRush)
        {
            // Increase timer when outside bounds
            timeOutsideTimer += Time.deltaTime;

            if (timeOutsideTimer > timeOutsideEnemyBoundsReq)
            {
                willDoAdrenalineRush = true;
            }
        }

        // Max adrenaline 'sticking', where the bar doesn't immediately deplete
        if (stayInMaxTimer > 0f)
        {
            stayInMaxTimer -= Time.deltaTime;
            timeToRegenerateTimer += Time.deltaTime;
        }
        else
        {
            timeToRegenerateTimer = 0f;

            // Slowly remove adrenaline
            currentAdrenaline =
                Mathf.MoveTowards(
                    currentAdrenaline,
                    0f,
                    Time.deltaTime * depletionRate
                );
        }

        if (timeToRegenerateTimer > timeToRegenerate)
        {
            // Regenerate a health point!
            playerLife.AddLife(1);
            timeToRegenerateTimer = 0f;
        }

        if (isInEnemyBounds)
        {
            if (isInEnemyBoundsTimer > 0.2f)
            {
                // Turn off enemyinbounds (since collision reports don't line up with update())
                isInEnemyBounds = false;
            }

            isInEnemyBoundsTimer += Time.deltaTime;
        }
    }

    public int ReceiveAttack(int amt)
    {
        if (currentAdrenaline <= 0f)
        {
            // Sorry, adrenaline can't protect you here
            return amt;
        }

        currentAdrenaline -= amt * hitPointMultiplier;
        float adrenalineBufferTotal = adrenalineBuffer + currentAdrenaline;     // In case the attack passed the buffer and laid an actual attack
        currentAdrenaline = Mathf.Max(0f, currentAdrenaline);

        stayInMaxTimer = 0f;

        if (adrenalineBufferTotal < 0f)
        {
            // Let player lose hitpoints
            return Mathf.RoundToInt(-adrenalineBufferTotal / hitPointMultiplier);
        }

        // Was able to successfully use adrenaline to block off the attack hitpoints!!!
        return 0;
    }

    public void AttackSuccess(int amt)
    {
        currentAdrenaline += amt * hitPointMultiplier;
        currentAdrenaline = Mathf.Min(maxAdrenaline, currentAdrenaline);
        if (IsMaxAdrenaline())
        {
            stayInMaxTimer = stayInMaxAdrTime;
        }
    }

    void SignalInEnemyBounds(bool flag)
    {
        isInEnemyBounds = flag;
        isInEnemyBoundsTimer = 0f;
    }

    public bool IsMaxAdrenaline()
    {
        return maxAdrenaline <= currentAdrenaline;
    }
}
