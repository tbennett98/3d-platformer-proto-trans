﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockCursor : MonoBehaviour
{
    public GameState gameState;

    void Start()
    {
        SetLockCursor(true);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !gameState.IsPlayerMovementDisabled() && !Input.GetKey(KeyCode.F9))            // TODO: This is only for testing! It's not normal to have to hold F9 to click stuff without getting your cursor locked..........
        {
            SetLockCursor(true);
        }
        
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.F5))   // TODO: I guess f5 could have an unlock function... but this is only for debug really
        {
            SetLockCursor(false);
        }
    }

    private void SetLockCursor(bool flag)
    {
        Cursor.lockState = flag ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !flag;
    }
}
