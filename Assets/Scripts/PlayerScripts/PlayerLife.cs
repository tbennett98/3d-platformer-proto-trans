﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLife : MonoBehaviour
{
    [Header("Plugins")]
    public Text uiText;

    [Header("Props")]
    public float life = 10f;

    void Start()
    {
        UpdateMe();
    }

    // NOTE: this could be tied together with some kind of particle effect... hence having the two functions!
    public void AddLife(float amount)
    {
        life += amount;
        UpdateMe();
    }

    public void RemoveLife(float amount)
    {
        life -= amount;
        UpdateMe();
    }

    private void UpdateMe()
    {
        uiText.text = $"Life: {this.life}";

        if (life <= 0)
        {
            Debug.LogError("GAME OVER... you died!");
        }
    }
}
