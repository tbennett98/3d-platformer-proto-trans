﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Add functionality to switch to different weapons depending on the prefab but this is pretty much it!
public class PlaceWeapon : MonoBehaviour
{
    [Header("Weapon Positioning")]
    public Transform handArmature;
    public GameObject weaponModel;
    private GameObject weaponModelCreated;

    void Start()
    {
        // Create and rig weapon
        GameObject createdWeapon = Instantiate(weaponModel.gameObject);
        createdWeapon.transform.parent = handArmature;
        createdWeapon.transform.localPosition = weaponModel.transform.position;
        createdWeapon.transform.localRotation = weaponModel.transform.rotation;
    }
}
