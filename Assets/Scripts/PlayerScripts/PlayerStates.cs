﻿public enum PlayerStates
{
    // Basic movement
    IDLE, RUNNING, JUMP, FALLING,

    // More advanced mvt
    MELEE, KNOCKBACKED, IN_WATER, WALL_CLIMB, LEDGE_GRAB,

    // Human specific
    HELI_SPIN,

    // Monster specific
    CHARGE_DIVE, CHARGE_BOING, SCALING_WALL,

    // Slime specific
    SLIMEBALL,

    // Lava specific
    GRAPPLING
}
