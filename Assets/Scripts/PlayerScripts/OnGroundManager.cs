﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnGroundManager : MonoBehaviour
{
    public string onGroundTagContains = "ground";
    public bool onGround;
    private Rigidbody body;
    private SphereCollider sphereCollider;
    private MovementReporterObserver movementReporterObserver;

    private Vector3 externalVelocitiesFudging;

    void Start()
    {
        body = GetComponent<Rigidbody>();
        sphereCollider = GetComponent<SphereCollider>();
        movementReporterObserver = GetComponent<MovementReporterObserver>();
    }

    void FixedUpdate()
    {
        // Fudge the collider & do physics check
        externalVelocitiesFudging.y = Mathf.Min(externalVelocitiesFudging.y, 0f);       // NOTE: Though this fudging method compensates, it will not compensate perfectly, however, I believe this is good enough to leave
        externalVelocitiesFudging *= 2 * Time.deltaTime;
        Collider[] colliders = Physics.OverlapCapsule(
            body.position + sphereCollider.center,
            body.position + sphereCollider.center + externalVelocitiesFudging,
            sphereCollider.radius
        );
        externalVelocitiesFudging = Vector3.zero;

        // See if can find a collision
        bool foundCollision = false;
        foreach (Collider other in colliders)
        {
            if (!other.isTrigger &&
            other.gameObject.tag.ToLower().Contains(onGroundTagContains.ToLower()))
            {
                foundCollision = true;
                break;
            }
        }
        onGround = foundCollision;

        // Report the colliders to mvt observer
        if (onGround && movementReporterObserver != null)
        {
            movementReporterObserver.ReportColliders(colliders);
        }
    }

    public void SetFudgingVelocity(Vector3 velocity)
    {
        externalVelocitiesFudging += velocity;
    }
}
