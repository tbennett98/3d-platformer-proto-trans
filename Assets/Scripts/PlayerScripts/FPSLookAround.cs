﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSLookAround : MonoBehaviour
{
    public GameState gameState;

    public Transform containerTrans;
    public ThirdPersonMovementRigidbody tpmr;
    public Cinemachine.CinemachineVirtualCamera virtualCamera;
    public bool isFPSEnabled = false;

    public float sensitivityHor = 1;
    public float sensitivityVer = 1;
    public float sensitivityPushPull = 1;

    public Vector2 pushPullBounds = new Vector2(40, 10);
    private float defaultFOV;

    public Vector2 lookUpDownBounds = new Vector2(-85f, 85f);
    private float upDownRotation;


    void Start()
    {
        defaultFOV = virtualCamera.m_Lens.FieldOfView;
    }

    void Update()
    {
        if (Input.GetButtonDown("Toggle FPS") && (!gameState.IsPlayerMovementDisabled() || isFPSEnabled))       // NOTE: this should be allowed to go into fps mode when player movement is allowed, however, during fps mode, player movement is disabled, so an extra flag to allow fps mode to disable should be allowed.
        {
            isFPSEnabled = !isFPSEnabled;
            if (isFPSEnabled)
            {
                // Reset lookat for new fps view!
                transform.eulerAngles = new Vector3(0, tpmr.cam.eulerAngles.y, 0);
                upDownRotation = 0f;
                virtualCamera.m_Lens.FieldOfView = defaultFOV;
            }
            virtualCamera.gameObject.SetActive(isFPSEnabled);
        }

        if (isFPSEnabled)
        {
            // Look around
            Vector3 lookDelta = new Vector3();
            lookDelta.y = (Input.GetAxisRaw("Mouse X") + Input.GetAxisRaw("Horizontal")) * sensitivityHor;
            upDownRotation += Input.GetAxisRaw("Mouse Y") * sensitivityVer * -1 * Time.deltaTime * 100f;

            Vector3 newEulerAngles = transform.eulerAngles + lookDelta * Time.deltaTime * 100f;
            upDownRotation = Mathf.Clamp(upDownRotation, lookUpDownBounds.x, lookUpDownBounds.y);
            newEulerAngles.x = upDownRotation;
            transform.eulerAngles = newEulerAngles;

            // Push pull
            float newFOV =
                virtualCamera.m_Lens.FieldOfView
                + Input.GetAxisRaw("Vertical") * sensitivityPushPull * Time.deltaTime * -100f;
            virtualCamera.m_Lens.FieldOfView = Mathf.Clamp(newFOV, pushPullBounds.y, pushPullBounds.x);

        }
    }
}
