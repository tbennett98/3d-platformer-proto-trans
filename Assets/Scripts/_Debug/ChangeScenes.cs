﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class ChangeScenes : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            int nextSceneInd =
                (SceneManager.GetActiveScene().buildIndex + 1)
                    % SceneManager.sceneCountInBuildSettings;
            if (nextSceneInd == 0) nextSceneInd++;      // So that game doesn't reload (scene 0 is loadgame scene) TODO: Make a better system, like typing in the scene name or something eh

            SceneManager.LoadScene(nextSceneInd);
        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            Debug.Break();
        }

        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F3))
        {
            UnityEditor.EditorApplication.isPlaying = false;
        }
        #endif
    }
}
