﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathPlane : MonoBehaviour
{
    public float deadPlaneY = -40f;

    void Update()
    {
        // Reset if y is below 40
        if (transform.position.y < deadPlaneY)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
