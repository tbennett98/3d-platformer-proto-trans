﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextboxQueue
{
    [System.Serializable]
    public class TextboxData
    {
        public string mainText;
        public bool isQuestion;
        public string[] questionTexts;

        public TextboxData(string mainText = "")
        {
            this.mainText = mainText;
            this.isQuestion = false;
        }
    }

    private Queue<TextboxData> textQueue;

    public TextboxQueue(TextboxData[] texts)
    {
        textQueue = new Queue<TextboxData>();

        foreach (TextboxData text in texts)
        {
            textQueue.Enqueue(text);
        }
    }

    public void Enqueue(TextboxData text)
    {
        textQueue.Enqueue(text);
    }

    public TextboxData Dequeue()
    {
        return textQueue.Dequeue();
    }

    public bool HasElements()
    {
        return textQueue.Count > 0;
    }
}
