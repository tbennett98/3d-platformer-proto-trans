﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TextboxLoader : MonoBehaviour
{
    public GameObject textboxPrefab;
    public bool mustPressAction = true;

    public TextboxQueue.TextboxData[] textboxDatas;

    private InteractableEntity interactableEntity;

    [Header("Send message depending on the selectedOption")]
    public UnityEvent[] selectedAnswerOutcomes;

    [Header("Send message when textbox is finished")]
    public bool undoBusyFlagWhenFinished = true;
    public UnityEvent triggerWhenTextboxFinishes;

    void Start()
    {
        interactableEntity = GetComponent<InteractableEntity>();
    }

    void OnInteracted()
    {
        DoTextbox();
    }

    public void AnswerQuestion(int optionIndex)
    {
        selectedAnswerOutcomes[optionIndex].Invoke();
    }

    public void OnTextboxFinished()
    {
        if (undoBusyFlagWhenFinished && interactableEntity != null)
        {
            interactableEntity.UndoBusyFlag();
        }

        // Invoke the textbox finished event
        triggerWhenTextboxFinishes.Invoke();
    }

    // NOTE:
    // Here is code to have hard-coded text boxes be loaded
    // in via a trigger and a button press, however it would be good to
    // also implement a JSON loader for textbox trees or something.

    public void DoTextbox()
    {
        // Don't display the same textbox multiple times
        TextboxHandler textboxHandler = FindObjectOfType<TextboxHandler>();
        if (textboxHandler == null)
        {
            TextboxHandler tbh = GameObject.Instantiate(textboxPrefab).GetComponent<TextboxHandler>();
            tbh.SetTextboxCreatedByLoader(this);
            tbh.SetTextboxQueue(
                new TextboxQueue(textboxDatas)
            );
        }
        else
        {
            textboxHandler.AppendToTextboxQueue(new TextboxQueue(textboxDatas));
        }
    }
}
