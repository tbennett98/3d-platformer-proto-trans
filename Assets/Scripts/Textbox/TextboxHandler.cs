﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextboxHandler : MonoBehaviour
{
    public const float LETTERS_PER_SECOND = 60f;

    public GameState gameState;

    public Text uiText;
    public GameObject uiOptionSelections;
    public Button uiFirstButton;
    private TextboxQueue.TextboxData currentText;
    private float displayChars;

    private bool inputProceed;
    private TextboxLoader createdByLoader;
    private TextboxQueue queue;

    // HACK: These functions are oh-so hacky but I don't care       -Timo
    void OnEnable()
    {
        FindObjectOfType<ThirdPersonMovementRigidbody>().SetDisableInput(true);
        gameState.DisablePlayerMovement(this, true);
    }

    void OnDisable()
    {
        // TODO: for some reason this gets a nullpointerexception sometimes....
        FindObjectOfType<ThirdPersonMovementRigidbody>().SetDisableInput(false);        // TODO: Player will jump when exiting the last textbox
        gameState.DisablePlayerMovement(this, false);
    }

    void Update()
    {
        // Keep going up in the text count
        if (currentText.mainText.Length > displayChars)
        {
            displayChars += LETTERS_PER_SECOND * Time.deltaTime;
            uiText.text = currentText.mainText.Substring(0, Mathf.Clamp((int)displayChars, 0, currentText.mainText.Length));
        }
        else
        {
            // Show the options if it's a question
            if (currentText.isQuestion &&
                !uiFirstButton.gameObject.activeInHierarchy)
            {
                // Load in the text and extra buttons for the options
                int numOptions = currentText.questionTexts.Length;
                for (int i = 0; i < numOptions; i++)
                {
                    Button currentUIButton = uiFirstButton;
                    if (i > 0)
                    {
                        // Add extra button
                        currentUIButton = GameObject.Instantiate(uiFirstButton.gameObject, uiOptionSelections.transform).GetComponent<Button>();
                        RectTransform rectTransform = currentUIButton.transform as RectTransform;
                        rectTransform.anchoredPosition = new Vector2(0f, -30 * i);

                    }

                    // Set listener events and text
                    currentUIButton.onClick.RemoveAllListeners();
                    int answerIndex = i;    // NOTE: this copies the variable i's value into another variable so that the delegate below has an unchanging variable (since i++ will cause the lambda one line below to use the new i's value). Unfortunately, no const folding is supported in c# so you'll have to do this semi-hacky way of doing things.     -03/14/2021 Timo
                    currentUIButton.onClick.AddListener(
                        delegate {
                            uiOptionSelections.SetActive(false);
                            AnswerQuestion(answerIndex);
                            ProceedText();
                        }
                    );
                    currentUIButton.GetComponentInChildren<Text>().text = currentText.questionTexts[i];
                }

                uiOptionSelections.SetActive(true);
            }

            // Continue if pressed the continue button, and if all text is displayed
            if ((Input.GetButtonDown("Jump") || Input.GetButtonDown("Fire")) && !currentText.isQuestion)
            {
                ProceedText();
            }
        }
    }

    public void SetTextboxCreatedByLoader(TextboxLoader loader)
    {
        this.createdByLoader = loader;
    }

    public void SetTextboxQueue(TextboxQueue queue)
    {
        // NOTE: this method should be called on
        // instantiation of this gameobject/prefab eh
        this.queue = queue;
        ProceedText();
    }

    public void AppendToTextboxQueue(TextboxQueue queue)
    {
        while (queue.HasElements())
        {
            this.queue.Enqueue(queue.Dequeue());
        }
    }

    public void AnswerQuestion(int optionIndex)
    {
        // It's up to the textbox loader to do the logic for which option was selected
        createdByLoader.AnswerQuestion(optionIndex);
    }

    private void ProceedText()
    {
        displayChars = 0f;
        if (queue == null)
        {
            Debug.LogError("Queue is missing for textbox", gameObject);
            currentText = new TextboxQueue.TextboxData("Queue is missing.... ERROR");
            return;
        }

        if (queue.HasElements())
        {
            currentText = queue.Dequeue();
            uiText.text = string.Empty;
        }
        else
        {
            // Quit
            createdByLoader.OnTextboxFinished();
            Destroy(gameObject);
        }
    }
}
