﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootIK : MonoBehaviour
{
    public string idleAnimName = "Armature|Idle";
    public float distanceToGround = 0.95f;
    public float footYOffset = 0.1f;
    public float ikSettledownSpeed = 10f;
    public LayerMask detectLayers;

    private Rigidbody body;
    private Animator anim;
    private Vector3 defaultPosition, localPosition;
    private float toFullIKWeight;

    private void Start()
    {
        body = GetComponentInParent<Rigidbody>();
        anim = GetComponent<Animator>();
        defaultPosition = transform.localPosition;
        toFullIKWeight = 1f;
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (layerIndex == 0)
        {
            if (anim && anim.GetCurrentAnimatorStateInfo(0).IsName(idleAnimName))
            {
                if (toFullIKWeight < 1f)
                {
                    toFullIKWeight = Mathf.Min(toFullIKWeight + ikSettledownSpeed * Time.deltaTime, 1f);
                }

                float footMvL = DoFoot(AvatarIKGoal.LeftFoot);
                float footMvR = DoFoot(AvatarIKGoal.RightFoot);
                float lesserVertMvt = Mathf.Min(footMvL, footMvR);

                // Body mvt needs to be done after knees to ensure correct knee distance calculations
                transform.localPosition += Vector3.Lerp(Vector3.zero, new Vector3(0f, lesserVertMvt, 0f), toFullIKWeight);
            }
            else
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, defaultPosition, 0.25f);
                toFullIKWeight = 0f;
            }
        }

        // body.MovePosition(body.position + localPosition);
    }

    private float DoFoot(AvatarIKGoal ikFoot)
    {
        float retFloat = 0f;
        RaycastHit hit;
        Vector3 ikFootPos = anim.GetIKPosition(ikFoot);
        Ray ray = new Ray(ikFootPos + Vector3.up, Vector3.down);
        Debug.DrawLine(ray.origin, ray.origin + ray.direction * (distanceToGround + 1f), Color.magenta);
        if (Physics.Raycast(ray, out hit, distanceToGround + 1f, detectLayers))
        {
            if (hit.transform.tag.Contains("ground"))
            {
                anim.SetIKPositionWeight(ikFoot, 1f);
                anim.SetIKRotationWeight(ikFoot, toFullIKWeight);

                Vector3 footPos = hit.point;
                if (hit.rigidbody != null)
                {
                    // NOTE: this adjusts the foot position to the interpolated position
                    //
                    // NOTE: this has some maintenance cost, for example, when the platform were to rotate in the x or z euler angles
                    // MAYBE: try https://forum.unity.com/threads/best-way-to-interpolate-between-current-ik-position-to-target-using-onanimatorik.394653/ and see if lerping the foot position would work
                    footPos.y += hit.transform.position.y - hit.rigidbody.position.y;
                }

                footPos.y += footYOffset;
                retFloat = footPos.y - ikFootPos.y;

                // NOTE: to make this system more robust, let's make it so that the root slowly lerps to its required position, and then depending on t for the lerp, we'll rotate into the direction as well
                anim.SetIKPosition(ikFoot, footPos);
                anim.SetIKRotation(ikFoot, Quaternion.Euler(0f, anim.GetIKRotation(ikFoot).eulerAngles.y - transform.eulerAngles.y, 0f) * Quaternion.FromToRotation(Vector3.up, hit.normal) * transform.rotation);
            }
        }

        return retFloat;
    }
}
